#pragma once
#include<iostream>
#include<assert.h>
using std::cout;
using std::cin;
using std::endl;
class Date
{
public:
	bool isLeapYear(int year)
	{
		//四年一润百年不润或四百年一润
		return (year % 4 == 0 && year % 100 != 0) || (year % 400 == 0);
	}
	//获取某月天数
	int GetMonthDay(int year, int month);
	//构造函数
	Date(int year = 1, int month = 1, int day = 1);
	//打印
	void Print() const
	{
		cout << _year << "-" << _month << "-" << _day << endl;
	}
	// <运算符重载
	bool operator<(const Date& d) const;
	// ==运算符重载
	bool operator==(const Date& d) const;
	// <=运算符重载
	bool operator<=(const Date& d) const
	{
		return *this < d || *this == d;
	}
	// >运算符重载
	bool operator>(const Date& d) const
	{
		return !(*this <= d);
		//return (d < *this);
	}
	// >=运算符重载
	bool operator>=(const Date& d) const
	{
		return !(*this < d);
	}
	// !=运算符重载
	bool operator!=(const Date& d) const
	{
		return !(*this == d);
	}

	//日期 + 天数
	Date operator+(int day) const;
	//日期 += 天数
	Date& operator+=(int day);
	//日期 -= 天数
	Date& operator-=(int day);
	//日期 - 天数
	Date operator-(int day) const;

	//前置++
	Date& operator++(); //无参的为前置
	//后置++
	Date operator++(int i); //有参数的为后置
	//前置--
	Date& operator--(); //无参的为前置
	//后置--
	Date operator--(int i); //有参数的为后置

//日期 - 日期
	int operator-(const Date& d) const;
private:
	int _year;
	int _month;
	int _day;
};