#pragma once
#include<iostream>
#include<assert.h>
using namespace std;
namespace bit
{
    class string
    {
        /*friend ostream& operator<<(ostream& _cout, const bit::string& s);
        friend istream& operator>>(istream& _cin, bit::string& s);*/
    public:
        typedef char* iterator;
    public:
        //构造函数
        string(const char* str = "")
            :_size(strlen(str))
            ,_capacity(_size)//
        {
            _str = new char[_capacity + 1];//+1给\0预留空间
            strcpy(_str, str);//把常量字符串的内容拷贝进去
        }
        //拷贝构造函数--》传统写法
     /*
        string(const string& s)//深拷贝
            :_size(strlen(s._str))
            , _capacity(_size)
        {
            _str = new char[_capacity + 1];//开辟一段与原数组等大的空间
            strcpy(_str, s._str);//拷贝过去
        }
     */
        //现代写法
        string(const string& s)
            :_str(nullptr)
            , _size(0)
            , _capacity(_size)
        {
            string tmp(s._str);//利用构造函数，创建一个与s等容的string对象
            swap(tmp);//利用swap函数交换_str, _size, _capacity
        }
        //复制运算符重载函数 s1 = s3
        string& operator=(const string& s)
        {
            if (this != &s)//确保不是自己给自己赋值
            {
                string tmp(s._str);
                swap(tmp);
            }
            return *this;
        }
        //析构函数
        ~string()
        {
            if (_str)//如果不为空再析构
            {
                delete[] _str;
                _str = nullptr;
                _size = _capacity = 0;
            }
        }




            //////////////////////////////////////////////////////////////

            // iterator

        iterator begin()
        {
            return _str;
        }

        iterator end()
        {
            return _str + _size;
        }



            /////////////////////////////////////////////////////////////

            // modify

        void push_back(char c)
        {
            //检测是否需要扩容
            if (_size == _capacity)
            {
                reserve(_capacity == 0 ? 4 : _capacity * 2);
            }
            _str[_size] = c;
            _size++;
            _str[_size] = '\0';
        }

        string& operator+=(char c)
        {
            push_back(c);
            return *this;
        }

        void append(const char* str)
        {
            size_t len = _size + strlen(str);
            if (len > _capacity)
            {
                //扩容
                reserve(len);
            }
            strcpy(_str + _size, str);
            _size = len;
        }

        string& operator+=(const char* str)
        {
            append(str);
            return *this;
        }

        void clear()
        {
            _str[0] = '\0';
            _size = 0;
        }
            
        void swap(string& s)
        {
            std::swap(_str, s._str);
            std::swap(_size, s._size);
            std::swap(_capacity, s._capacity);
        }

        const char* c_str()const
        {
            return _str;
        }

        /////////////////////////////////////////////////////////////

        // capacity

        size_t size()const
        {
            return _size;
        }

        size_t capacity()const
        {
            return _capacity;
        }

        bool empty()const
        {
            return strcmp(_str, "") == 0;
        }

        void resize(size_t n, char c = '\0')
        {
            if (n < _size)//若n<_size，更新_size到n
            {
                _size = n;
                _str[_size] = '\0';
            }
            else
            {
                if (n > _capacity)//更新容量到n
                {
                    reserve(n);
                }
                for (size_t i = _size; i < n; i++)
                {
                    _str[i] = c;
                }
                _size = n;
                _str[_size] = '\0';
            }
        }

        void reserve(size_t n)
        {
            //只有在n超过容量的时候，才会出现扩容：
            if (n > _capacity)
            {
                char* tmp = new char[n + 1];//多开一个给\0
                strcpy(tmp, _str);
                delete[] _str;
                _str = tmp;
                _capacity = n;
            }

        }



        /////////////////////////////////////////////////////////////

        // access

        char& operator[](size_t index)
        {
            assert(index < _size);
            return _str[index];
        }

        const char& operator[](size_t index)const
        {
            assert(index < _size);
            return _str[index];
        }



        /////////////////////////////////////////////////////////////

        //relational operators

        bool operator<(const string& s)
        {
            return strcmp(this->c_str(), s.c_str()) < 0;
        }

        bool operator<=(const string& s)
        {
            return *this < s || *this == s;
        }

        bool operator>(const string& s)
        {
            return !(*this <= s);
        }

        bool operator>=(const string& s)
        {
            return !(*this < s);
        }

        bool operator==(const string& s)
        {
            return strcmp(this->c_str(), s.c_str()) == 0;
        }

        bool operator!=(const string& s)
        {
            return !(*this == s);
        }



        // 返回c在string中第一次出现的位置

        size_t find(char c, size_t pos = 0) const
        {
            for (; pos < _size; pos++)
            {
                if (_str[pos] == c)
                    return pos;
            }
            return npos;
        }

        // 返回子串s在string中第一次出现的位置

        size_t find(const char* s, size_t pos = 0) const
        {
            const char* p = strstr(_str + pos, s);
            if (p == nullptr)
            {
                return npos;
            }
            else
            {
                return p - _str;//指针相减即位置
            }
        }

        // 在pos位置上插入字符c/字符串str，并返回该字符的位置
        // abcdefgh
        // abc defgh
        //pos = 3  x
        // abcxdefgh
        string& insert(size_t pos, char c)
        {
            assert(pos <= _size);
            if (_size == _capacity)
            {
                //扩容
                reserve(_capacity == 0 ? 4 : _capacity * 2);
            }

            size_t end = _size + 1;
            while (end > pos)
            {
                _str[end] = _str[end - 1];
                end--;
            }
            _str[pos] = c;
            _size += 1;
            return *this;
        }

        //abcdef  pos=3  xzy
        //abcdef+++
        //abcdef

        string& insert(size_t pos, const char* str)
        {
            assert(pos <= _size);
            if (strlen(str) == 0)
            {
                return *this;//若传进来的字符长度位0，说明即可直接返回
            }
            size_t len = _size + strlen(str);
            if (len > _capacity)
            {
                //扩容
                reserve(len);
            }
            size_t end = _size;
            while (end > pos)
            {
                _str[len] = _str[end];
                len--;
                end--;
            }
            _str[len] = _str[end];
            strncpy(_str + pos, str, strlen(str));
            _size += strlen(str);
            return *this;
        }



        // 删除pos位置上的元素，并返回该元素的下一个位置
        //abcdef pos
        //
        string& erase(size_t pos, size_t len)
        {
            assert(pos <= _size);
            if (len == npos || pos + len >= _size)
            {
                _str[pos] = '\0';
                _size = pos;
            }
            else
            {
                size_t begin = pos + len;
                while (begin <= _size)
                {
                    _str[begin - len] = _str[begin];
                    begin++;
                }
                _size -= len;
            }
            return *this;
        }
    private:
        friend ostream& operator<<(ostream& _cout, const bit::string& s);
        friend istream& operator>>(istream& _cin, bit::string& s);
    private:

        char* _str;
        size_t _size;
        size_t _capacity;
        
        const static size_t npos;
    };
    const size_t string::npos = -1;




}


ostream& operator<<(ostream& out, const string& s)
{
    for (auto ch : s)
    {
        out << ch;
    }
    return out;
}


istream& operator>>(istream& in, string& s)
{
    /*
        法一：
        s.clear();
        char ch;
        ch = in.get();//使用get()函数才能获取空格或者换行字符
        while (ch != ' ' && ch != '\n')
        {
            s += ch;
            ch = in.get();
        }
        return in;
    */
    //法二：
    //要先把原字符串的所有数据给清空才可以输入新的数据，否则会累加到原数据后面，出错
    s.clear();
    char ch;
    ch = in.get();//使用get()函数才能获取空格或者换行字符
    char buff[128] = { '\0' };
    size_t i = 0;
    while (ch != ' ' && ch != '\n')
    {
        buff[i++] = ch;
        if (i == 127)
        {
            s += buff;
            memset(buff, '\0', 128);
            i = 0;
        }
        ch = in.get();
    }
    s += buff;
    return in;
}