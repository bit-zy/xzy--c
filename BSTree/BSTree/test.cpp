#define _CRT_SECURE_NO_WARNINGS 1
#include"BSTree_key.h"
#include"BSTree_key_value.h"
namespace key
{
	void TestBSTree1()
	{
		BSTree<int> t;
		int a[] = { 8, 3, 1, 10, 6, 4, 7, 14, 13 };
		for (auto e : a)
		{
			t.Insert(e);
		}
		t.InOrder();

		t.Insert(16);
		t.Insert(9);
		t.InOrder();
	}
	void TestBSTree2()
	{
		BSTree<int> t;
		int a[] = { 8, 3, 1, 10, 6, 4, 7, 14, 13 };
		for (auto e : a)
		{
			t.Insert(e);
		}
		t.InOrder();
		//删除3和8
		t.Erase(3);
		t.Erase(8);
		t.InOrder();
		//删除14和7
		t.Erase(14);
		t.Erase(7);
		t.InOrder();
		//删除所有值
		for (auto e : a)
		{
			t.Erase(e);
		}
		t.InOrder();
	}

	void TestBSTree3()
	{
		BSTree<int> t;
		int a[] = { 8, 3, 1, 10, 6, 4, 7, 14, 13 };
		for (auto e : a)
		{
			t.Insert(e);
		}
		t.InOrder();

		BSTree<int> copy = t;
		copy.InOrder();
	}
	void TestBSTree4()
	{
		BSTree<int> t;
		int a[] = { 8, 3, 1, 10, 6, 4, 7, 14, 13 };
		for (auto e : a)
		{
			t.InsertR(e);
		}
		t.InOrder();

		BSTree<int> copy = t;
		copy.InOrder();
	}
	void TestBSTree5()
	{
		BSTree<int> t;
		int a[] = { 8, 3, 1, 10, 6, 4, 7, 14, 13 };
		for (auto e : a)
		{
			t.InsertR(e);
		}
		t.InOrder();
		//删除3和8
		t.EraseR(3);
		t.EraseR(8);
		t.InOrder();
		//删除14和7
		t.EraseR(14);
		t.EraseR(7);
		t.InOrder();
		//删除所有值
		for (auto e : a)
		{
			t.EraseR(e);
		}
		t.InOrder();
	}
}
namespace key_value
{
	void TestBSTree1()
	{
		BSTree<string, string> ECdict;
		ECdict.InsertR("root", "根");
		ECdict.InsertR("string", "字符串");
		ECdict.InsertR("left", "左边");
		ECdict.InsertR("insert", "插入");
		ECdict.InsertR("ShuanQ", "拴Q");
		string str;
		while (cin >> str)
		{
			//BSTreeNode<string, string>* ret = ECdict.FindR(str);
			auto ret = ECdict.FindR(str);
			if (ret != nullptr)
			{
				cout << "对应的中文：" << ret->_value << endl;
			}
			else
			{
				cout << "无此单词，请重写输入" << endl;
			}
		}
	}

	void TestBSTree2()
	{
		string arr[] = { "苹果", "西瓜", "苹果", "西瓜", "苹果", "苹果", "西瓜","苹果", "香蕉", "苹果", "香蕉" };
		//统计水果出现的次数
		BSTree<string, int> countTree;
		int count = 0;
		for (const auto& str : arr)
		{
			auto ret = countTree.FindR(str);
			if (ret == nullptr)
			{
				countTree.InsertR(str, 1);
			}
			else
			{
				ret->_value++;//修改value
			}
		}
		countTree.InOrder();
	}

}

int main()
{
	//key::TestBSTree1();
	//key::TestBSTree2();
	//key::TestBSTree3();
	//key::TestBSTree4();
	//key::TestBSTree5();
	//key_value::TestBSTree1();
	key_value::TestBSTree2();
}



