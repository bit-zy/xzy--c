#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;
//class Person {
//public:
//	virtual void BuyTicket() { cout << "买票-全价" << endl; }
//};
//class Student : public Person {
//public:
//	virtual void BuyTicket() { cout << "买票-半价" << endl; }
//	/*注意：在重写基类虚函数时，派生类的虚函数在不加virtual关键字时，虽然也可以构成重写(因
//	为继承后基类的虚函数被继承下来了在派生类依旧保持虚函数属性),但是该种写法不是很规范，不建议
//	这样使用*/
//	/*void BuyTicket() { cout << "买票-半价" << endl; }*/
//};
//void Func(Person& p)
//{
//	//通过父类的引用调用虚函数
//	p.BuyTicket();
//}
//void Func(Person* p)
//{
//	//通过父类的指针调用虚函数
//	p->BuyTicket();
//}
//int main()
//{
//	Person ps;
//	Student st;
//	//传对象
//	Func(ps);//买票-全价
//	Func(st);//买票-半价
//	//传地址
//	Func(&ps);//买票-全价
//	Func(&st);//买票-半价
//	return 0;
//}

////A是B的父类
//class A {};
//class B : public A {};
////父类
//class Person {
//public:
//	//父类虚函数返回值为父类的指针
//	virtual A* f()
//	{
//		cout << "virtual A* Person::f()" << endl;
//		return nullptr;
//	}
//};
////子类
//class Student : public Person {
//public:
//	virtual B* f()//返回值B是A的子类，此时发生协变（协变的类型必须是父子关系）
//	{
//		cout << "virtual B* Student::f()" << endl;
//		return nullptr;
//	}
//};
//int main()
//{
//	Person p;//父类的指针
//	Student s;
//	Person* ptr = &p;
//	ptr->f();//virtual A* Person::f()
//	
//	ptr = &s;
//	ptr->f();//virtual B* Student::f()
//}


//class A
//{
//public:
//	virtual void func(int val = 1)
//	{
//		cout << "A->" << val << endl;
//	}
//	virtual void test()
//	{
//		func();
//	}
//};
//class B : public A
//{
//public:
//	void func(int val = 0) 
//	{ 
//		cout << "B->" << val << endl;
//	}
//};
//int main(int argc, char* argv[])
//{
//	B* p = new B;
//	p->test();//子类继承了test函数
//	return 0;
//}


//class Person {
//public:
//	virtual ~Person()
//	{ 
//		cout << "~Person()" << endl; 
//	}
//};
//class Student : public Person {
//public:
//	//Person析构函数加了virtual，关系就变了
//	//重定义（隐藏）关系 -> 重写（覆盖）关系
//	//对于普通对象是没有影响的
//	~Student() 
//	{
//		cout << "~Student()" << endl; 
//	}
//};
//// 只有派生类Student的析构函数重写了Person的析构函数，下面的delete对象调用析构函\
//数，才能构成多态，才能保证ptr指向的对象正确的调用析构函数。
//int main()
//{
//	/*Person p;
//	Student s;*/
//
//	Person* ptr = new Person;
//	delete ptr;// 编译器转换成 ptr->destructor() + operator delete(ptr)
//	ptr = new Student;
//	delete ptr;// 编译器转换成 ptr->destructor() + operator delete(ptr)
//	
//	return 0;
//}



//class Person {
//public:
//	virtual ~Person()
//	{
//		cout << "~Person()" << endl;
//	}
//};
//class Student : public Person {
//public:
//	~Student()
//	{
//		cout << "~Student()" << endl;
//	}
//};
//int main()
//{
//	Person* ptr = new Person;
//	delete ptr;// 编译器转换成 ptr->destructor() + operator delete(ptr)
//	ptr = new Student;
//	delete ptr;// 编译器转换成 ptr->destructor() + operator delete(ptr)
//	return 0;
//}



//// 这里常考一道笔试题：sizeof(Base)是多少？
//class Base
//{
//public:
//	virtual void Func1()
//	{
//		cout << "Func1()" << endl;
//	}
//private:
//	int _b = 1;
//};
//int main()
//{
//	cout << sizeof(Base) << endl;
//	return 0;
//}


//class Car
//{
//public:
//	virtual void Drive() final {}
//};
//class Benz :public Car
//{
//public:
//	virtual void Drive() { cout << "Benz-舒适" << endl; }
//};

////最终类，不能被继承
//class Car final
//{
//public:
//};
//class Benz :public Car
//{
//public:
//};

//class Car {
//public:
//	/*virtual*/ void Drive() {}
//};
////override是写在子类中的，要求严格检测是否完成重写，如果没有就报错
//class Benz :public Car {
//public:
//	virtual void Drive() override { cout << "Benz-舒适" << endl; }
//};

//抽象类
class Car
{
public:
	//纯虚函数
	virtual void Drive() = 0;
};
class BMW :public Car
{
public:
	//重写纯虚函数
	virtual void Drive()
	{
		cout << "BMW-操控" << endl;
	}
};
class Benz :public Car
{
public:
	//重写纯虚函数
	virtual void Drive()
	{
		cout << "Benz-舒适" << endl;
	}
};
int main()
{
	//Car c;//抽象类不能实例化对象
	//重写纯虚函数，派生类就可以实例化出对象
	BMW b1;
	Benz b2;  
	//不同对象使用基类指针完成多态的行为
	Car* pBenz = new Benz;
	Car* pBMW = new BMW;
	pBenz->Drive();//Benz-舒适
	pBMW->Drive();//BMW-操控
}