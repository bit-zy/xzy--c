#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;
//class A
//{
//
//};
//A::~A()
//{
//	delete this;
//	this = nullptr;
//}
//int main()
//{
//	return 0;
//}

//class A
//{
//public:
//	A()
//	{
//		cout << "构造函数" << endl;
//	}
//};
//int main()
//{
//	A c1, * c2;
//	A* c3 = new A;
//	A& c4 = c1;
//	return 0;
//}


#include<iostream>
using namespace std;
bool IsPrime(int n)
{
    for (int i = 2; i < n; i++)
    {
        if (n % i == 0)
        {
            return false;//不是素数
        }
    }
    return true;
}
int main()
{
    int n = 0;
    cin >> n;
    int k = n / 2;
    int p = 0;
    while (k > 0)
    {
        if (IsPrime(k))
        {
            p = n - k;
            if (IsPrime(p))
            {
                break;
            }
        }
        k--;
    }
    cout << k << " " << p << endl;
    return 0;
}