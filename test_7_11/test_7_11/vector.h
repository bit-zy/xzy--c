#pragma once
#pragma once
#include<assert.h>
#include<iostream>
using namespace std;

namespace cpp
{
	template<class T>
	class vector
	{
	public:
		typedef T* iterator;
		typedef const T* const_iterator;
		/*构造函数*/
			//无参拷贝构造
		vector()
			:_start(nullptr)
			, _finish(nullptr)
			, _endofstoage(nullptr)
		{}


		/*容器访问相关函数*/
			//迭代器
				//begin
		iterator begin()
		{
			return _start;
		}
		//end
		iterator end()
		{
			return _finish;
		}
		//const版本迭代器
		const_iterator begin() const
		{
			return _start;
		}
		//end
		const_iterator end() const
		{
			return _finish;
		}

		//operator[]运算符重载
		T& operator[](size_t pos)
		{
			assert(pos < size());//检测pos的合法性
			return _start[pos];
		}
		//const版本的[]运算符重载
		const T& operator[](size_t pos) const
		{
			assert(pos < size());//检测pos的合法性
			return _start[pos];
		}

		/*空间增长问题*/
			//size
		size_t size() const //最好加上const，普通对象和const对象均可调用
		{
			return _finish - _start; //指针相减就能得到size的个数
		}
		//capacity
		size_t capacity() const
		{
			return _endofstoage - _start;
		}

		//reserve扩容
		void reserve(size_t n)
		{
			size_t sz = size();//提前算出size()的大小，方便后续更新_finish
			if (n > capacity())
			{
				T* tmp = new T[n];
				if (_start)//判断旧空间是否有数据
				{
					memcpy(tmp, _start, sizeof(T) * size());
					delete[] _start;//释放旧空间
				}
				_start = tmp;//指向新空间
			}
			//更新_finish和_endofstoage
			_finish = _start + sz;
			_endofstoage = _start + n;
		}
		//resize
			//void resize(size_t n, T val = T())
		void resize(size_t n, const T& val = T()) //利用T()调用默认构造函数的值进行初始化，这样写说明C++的内置类型也有自己的构造函数
		{
			//如果 n > capacity()容量，就需要扩容
			if (n > capacity())
			{
				reserve(n);
			}
			//如果 n > size()，就需要把有效数据_finish到_start + n之间的数据置为缺省值val
			if (n > size())
			{
				while (_finish < _start + n)
				{
					*_finish = val;
					_finish++;
				}
			}
			//如果 n < size()，更新有效数据到_start + n
			else
			{
				_finish = _start + n;
			}
		}


		/*增加*/
			//push_back
		void push_back(const T& x)
		{
			//检测是否需要扩容
			if (_finish == _endofstoage)
			{
				size_t newcapcacity = capacity() == 0 ? 4 : capacity() * 2;
				reserve(newcapcacity);
			}
			*_finish = x;
			_finish++;
		}
		//insert
		iterator insert(iterator pos, const T& x)
		{
			//检测参数合法性
			assert(pos >= _start && pos <= _finish);
			//检测是否需要扩容
			/*扩容以后pos就失效了，需要更新一下*/
			if (_finish == _endofstoage)
			{
				size_t n = pos - _start;//计算pos和start的相对距离
				size_t newcapcacity = capacity() == 0 ? 4 : capacity() * 2;
				reserve(newcapcacity);
				pos = _start + n;//防止迭代器失效，要让pos始终指向与_start间距n的位置
			}
			//挪动数据
			iterator end = _finish - 1;
			while (end >= pos)
			{
				*(end + 1) = *(end);
				end--;
			}
			//把值插进去
			*pos = x;
			_finish++;
			return pos;
		}


		/*删除*/
			//pop_back
		void pop_back()
		{
			if (_finish > _start)
			{
				_finish--;
			}
		}
	private:
		iterator _start;	  //指向容器的头
		iterator _finish;	  //指向有效数据的尾
		iterator _endofstoage;//指向容器的尾
	};
}