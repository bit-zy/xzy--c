#define _CRT_SECURE_NO_WARNINGS 1
//#include<iostream>
//#include<vector>
//using namespace std;
////获取公约数的函数
//int getDivisor(int& x, int& y)
//{
//    int ret = 0;
//    while (ret = x % y)
//    {
//        x = y;
//        y = ret;
//    }
//    return y;
//}
//int main()
//{
//    int n = 0;
//    int a = 0;
//    while (cin >> n >> a)//多组输入
//    {
//        int num = 0;
//        vector<int> v(n);
//        int i = 0;
//        while (n--)//把怪物的防御力依次放入数组v里头
//        {
//            cin >> num;
//            v[i++] = num;
//        }
//        int sum = a;//记录最终能力值
//        for (int i = 0; i < v.size(); i++)
//        {
//            if (a >= v[i])
//                sum += v[i];
//            else
//            {
//                int ret = getDivisor(sum, v[i]);
//                sum += ret;
//            }
//        }
//        cout << sum << endl;
//    }
//    return 0;
//}


//#include<iostream>
//#include<vector>
//using namespace std;
//获取公约数的函数
//int getDivisor(int& x, int& y)
//{
//    int ret = 0;
//    while (ret = x % y)
//    {
//        x = y;
//        y = ret;
//    }
//    return y;
//}
//int main()
//{
//    int n = 0;
//    int a = 0;
//    while (cin >> n >> a)//多组输入
//    {
//        int num = 0;
//        vector<int> v(n);
//        int i = 0;
//        while (n--)//把怪物的防御力依次放入数组v里头
//        {
//            cin >> num;
//            v[i++] = num;
//        }
//        int sum = a;//记录最终能力值
//        for (int i = 0; i < v.size(); i++)
//        {
//            if (a >= v[i])
//                a += v[i];
//            else
//            {
//                int y = v[i];
//                int x = a;
//                int ret = getDivisor(x, y);
//                a += ret;
//            }
//        }
//        cout << a << endl;
//    }
//    return 0;
//}\

#include<iostream>
#include<unordered_map>
#include<string>
using namespace std;
int main()
{
    string str;
    cin >> str;
    unordered_map<char, bool> mp;
    for (auto c : str)
    {
        mp[c] = mp.find(c) == mp.end();
    }
    for (auto e : str)
    {
        if (mp[e])
            cout << e << endl;
    }
    return 0;
}