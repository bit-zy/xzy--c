#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<list>
using namespace std;
void test1()
{
	list<int> lt1;
	list<int> lt2(5, 3);//构造5个值为3的元素
	for (auto e : lt2)
		cout << e << " ";
	list<int> lt3(lt2);//用lt2拷贝构造lt3
	list<int> lt4(lt2.begin(), lt2.end());
	// 以数组为迭代器区间构造lt5
	int array[] = { 1,2,3,4 };
	list<int> lt5(array, array + sizeof(array) / sizeof(int));
}
void test2()
{
	list<int> lt(5, 3);
	list<int>::iterator it = lt.begin();
	while (it != lt.end())
	{
		cout << *it << " "; //3 3 3 3 3
		it++;
	}
}
void test3()
{
	list<int> lt;
	for (int i = 1; i <= 4; i++)
	{
		lt.push_back(i);//1 2 3 4
	}
	//list<int>::reverse_iterator rit = lt.rbegin();
	////或者用auto自动识别类型：auto rit = lt.rbegin();
	//while (rit != lt.rend())
	//{
	//	cout << *rit << " "; //4 3 2 1
	//	rit++;
	//}
	for (auto e : lt)
	{
		cout << e << " ";//1 2 3 4
	}
}
void test4()
{
	list<int> lt;
	for (int i = 1; i <= 4; i++)
	{
		lt.push_back(i);//1 2 3 4
	}
	cout << lt.front() << endl;//1
	cout << lt.back() << endl; //4
}
void test5()
{
	list<int> lt;
	for (int i = 1; i <= 4; i++)
	{
		lt.push_back(i);//1 2 3 4
	}
	cout << lt.empty() << endl;//0
	cout << lt.size() << endl;//4
}
void test6()
{
	list<int> lt;
	for (int i = 1; i <= 4; i++)
	{
		lt.push_back(i);//1 2 3 4
	}
	lt.push_front(0);
	cout << lt.front() << endl;//0
	lt.pop_front();
	cout << lt.front() << endl;//1
	lt.push_back(9);
	cout << lt.back() << endl;//9
	lt.pop_back();
	cout << lt.back() << endl;//4
}
void test7()
{
	list<int> lt;
	for (int i = 1; i <= 4; i++)
	{
		lt.push_back(i);//1 2 3 4
	}
	list<int>::iterator pos = find(lt.begin(), lt.end(), 3);
	//1、在3的位置插入值7
	lt.insert(pos, 7);
	for (auto e : lt)
		cout << e << " ";//1 2 7 3 4
	cout << endl;

	//2、在3的位置插入5个-1
	pos = find(lt.begin(), lt.end(), 3);
	lt.insert(pos, 5, -1);
	for (auto e : lt)
		cout << e << " ";//1 2 7 -1 -1 -1 -1 -1 3 4
	cout << endl;

	//3、在7的位置插入迭代器区间
	pos = find(lt.begin(), lt.end(), 7);
	list<int> lt2(3, 0);
	lt.insert(pos, lt2.begin(), lt2.end());
	for (auto e : lt)
		cout << e << " ";//1 2 0 0 0 7 -1 -1 -1 -1 -1 3 4
}
void test8()
{
	list<int> lt;
	for (int i = 1; i <= 7; i++)
	{
		lt.push_back(i);//1 2 3 4 5 6 7
	}
	list<int>::iterator pos = find(lt.begin(), lt.end(), 2);
	//1、删除2位置的元素
	lt.erase(pos);
	for (auto e : lt)
		cout << e << " ";//1 3 4 5 6 7
	cout << endl;
	//2、删除值为4后的所有元素
	pos = find(lt.begin(), lt.end(), 4);
	lt.erase(pos, lt.end());
	for (auto e : lt)
		cout << e << " ";//1 3
}
void test9()
{
	list<int> lt1(5, -1);
	list<int> lt2(3, 7);
	lt2.swap(lt1);
	for (auto e : lt1)
		cout << e << " "; //7 7 7 
	cout << endl;
	for (auto d : lt2)
		cout << d << " "; //-1 -1 -1 -1 -1
}
void test10()
{
	list<int> lt(5, -1);
	for (auto e : lt)
		cout << e << " ";//-1 -1 -1 -1 -1
	cout << endl;
	lt.clear();
	for (auto e : lt)
		cout << e << " ";//空
}
void test11()
{
	list<int> lt;
	for (int i = 3; i >= -3; i--)
	{
		lt.push_back(i);//3 2 1 0 -1 -2 -3
	}
	lt.sort();
	for (auto e : lt)
		cout << e << " ";//-3 -2 -1 0 1 2 3
}
void test12()
{
	list<int> lt;
	lt.push_back(5);
	lt.push_back(1);
	lt.push_back(3);
	lt.push_back(2);
	lt.push_back(2);
	lt.push_back(5);
	lt.push_back(-1);
	lt.push_back(7);
	lt.sort();
	lt.unique();
	for (auto e : lt)
		cout << e << " ";

}
int main()
{
	test12();
}