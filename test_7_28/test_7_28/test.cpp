#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;
//class Base
//{
//public:
//	virtual void Func1()
//	{
//		cout << "Func1()" << endl;
//	}
//private:
//	int _b = 1;
//};
//
//
//int main()
//{
//	Base b;
//	cout << sizeof(b) << endl;//8/16
//	return 0;
//}


// 针对上面的代码我们做出以下改造
// 1.我们增加一个派生类Derive去继承Base
// 2.Derive中重写Func1
// 3.Base再增加一个虚函数Func2和一个普通函数Func3
//class Base
//{
//public:
//	virtual void Func1()
//	{
//		cout << "Base::Func1()" << endl;
//	}
//	virtual void Func2()
//	{
//		cout << "Base::Func2()" << endl;
//	}
//	void Func3()
//	{
//		cout << "Base::Func3()" << endl;
//	}
//private:
//	int _b = 1;
//};
//class Derive : public Base
//{
//public:
//	virtual void Func1()
//	{
//		cout << "Derive::Func1()" << endl;
//	}
//private:
//	int _d = 2;
//};
//int main()
//{
//	Base b;
//	Derive d;
//	Base* p = &b;//指向父类对象
//	p->Func1();//调用父类虚函数
//
//	p = &d;//指向子类的对象
//	p->Func1();//调用子类的虚函数
//	return 0;
//}

//class Person {
//public:
//	virtual void BuyTicket() { cout << "Person : 买票-全价" << endl; }
//};
//class Student : public Person {
//public:
//	virtual void BuyTicket() { cout << "Student : 买票-半价" << endl; }
//};
//void Func(Person* p)
//{
//	p->BuyTicket();
//}
//int main()
//{
//	Person mike;
//	Func(&mike);//Person : 买票-全价
//	mike.BuyTicket();//Person : 买票-全价
//	return 0;
//}

//void Func(Person* p)
//{
//	p->BuyTicket();
//}
//int main()
//{
//	Person Mike;
//	//指向父类对象调用父类虚函数
//	Func(&Mike);//Person : 买票-全价
//	Student Johnson;
//	//指向子类对象调用子类虚函数
//	Func(&Johnson);//Student : 买票-半价
//	return 0;
//}

#include<stdio.h>
class Base {
public:
	virtual void func1() { cout << "Base::func1" << endl; }
	virtual void func2() { cout << "Base::func2" << endl; }
private:
	int a = 0;
};
class Derive :public Base {
public:
	virtual void func1() { cout << "Derive::func1" << endl; }
	virtual void func3() { cout << "Derive::func3" << endl; }
	virtual void func4() { cout << "Derive::func4" << endl; }
private:
	int b = 0;
};

typedef void(*VFPTR) ();
void PrintVTable(VFPTR vTable[])
{
	// 依次取虚表中的虚函数指针打印并调用。调用就可以看出存的是哪个函数
	cout << " 虚表地址>" << vTable << endl;
	for (int i = 0; vTable[i] != nullptr; ++i)
	{
		printf(" 第%d个虚函数地址 :%p,->", i, vTable[i]);
		VFPTR f = vTable[i];
		f();
	}
	cout << endl;  
}

//int c = 2;
//int main()
//{
//	/*Base b;
//	Derive d;
//	PrintVTable((VFPTR*)(*(int*)&b));
//	PrintVTable((VFPTR*)(*(int*)&d));*/
//
//	/*Base b1;
//	Base b2;
//	Base b3;*/
//	Base b4;
//	/*PrintVTable((VFPTR*)(*((int*)&b1)));
//	PrintVTable((VFPTR*)(*((int*)&b2)));
//	PrintVTable((VFPTR*)(*((int*)&b3)));*/
//	//PrintVTable((VFPTR*)(*((int*)&b4)));
//
//	int a = 0;
//	static int b = 1;
//	const char* str = "hello world";
//	int* p = new int[10];
//	printf("栈：%p\n", &a);
//	printf("静态区/数据段：%p\n", &b);
//	printf("静态区/数据段：%p\n", &c);
//	printf("常量区/代码段：%p\n", str);
//	printf("堆：%p\n", p);
//	printf("虚表：%p\n", (*((int*)&b4)));
//	printf("函数地址：%p\n", &Derive::func3);
//	printf("函数地址：%p\n", &Derive::func2);
//	printf("函数地址：%p\n", &Derive::func1);
//}

int main()
{
	string num1("123");
	string num2("456");
	string k = num1 + num2;
	cout << k << endl;
	return 0;
}