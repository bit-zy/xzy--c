#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<string>
#include<map>
using namespace std;

void test_map1()
{
	//����
	map<string, string> dict;
	dict.insert(pair<string, string>("sort", "����"));
	pair<string, string> kv("byte", "�ֽ�");
	dict.insert(kv);
	dict.insert(make_pair("left", "���"));
	dict.insert({ "right", "�ұ�" });
	//����
	map<string, string>::iterator it = dict.begin();
	while (it != dict.end())
	{
		cout << (*it).first << ":" << (*it).second << endl;
		it++;
	}
}
void test()
{
	map<string, string> dict;
	auto ret1 = dict.insert(make_pair("left", "���"));
	auto ret2 = dict.insert(make_pair("left", "ʣ��"));
	dict["operator"] = "����";//���� + �޸�
	dict["left"] = "��ߡ�ʣ��";//�޸�
	dict["erase"];//����
	cout << dict["left"] << endl;//��ߡ�ʣ��
}
void test_map2()
{
	//����
	map<string, string> dict;
	dict.insert(make_pair("left", "��"));
	dict.insert(make_pair("right", "��"));
	dict.insert(make_pair("front", "ǰ"));
	dict.insert(make_pair("back", "��"));
	//����
	map<string, string>::iterator it = dict.begin();
	while (it != dict.end())
	{
		//cout << (*it).first << ":" << (*it).second << endl;
		cout << it->first << ":" << it->second << endl;
		it++;
	}
	cout << endl;
	//��Χfor
	for (const auto& e : dict)
		cout << e.first << ":" << e.second << endl;
}

void test_map3()
{
	string arr[] = { "ƻ��", "����", "ƻ��", "����", "ƻ��", "ƻ��", "����", "ƻ��", "�㽶", "ƻ��", "�㽶" };
	/*map<string, int> countMap;
	for (auto& str : arr)
	{
		map<string, int>::iterator it = countMap.find(str);
		if (it != countMap.end())
		{
			it->second++;
		}
		else
		{
			countMap.insert(make_pair(str, 1));
		}
	}*/

//�Ż�1��
	/*map<string, int> countMap;
	for (auto& str : arr)
	{
		//pair<map<string, int>::iterator, bool> ret = countMap.insert(make_pair(str, 1));
		auto ret = countMap.insert(make_pair(str, 1));
		if (ret.second == false)
		{
			ret.first->second++;
		}
	}*/

//�Ż�2��
	map<string, int> countMap;
	for (auto& str : arr)
	{
		countMap[str]++;
	}

	for (const auto& kv : countMap)
		cout << kv.first << ":" << kv.second << endl;
}
void test_map4()
{
	map<string, string> dict;
	dict.insert(make_pair("left", "��"));
	dict.insert(make_pair("right", "��"));
	dict.insert(make_pair("front", "ǰ"));
	dict.insert(make_pair("back", "��"));
	string str;
	cin >> str;//����left
	map<string, string>::iterator pos = dict.find(str);
	if (pos != dict.end())
	{
		cout << pos->first << " : " << pos->second << endl;//left : ��
	}
	else
	{
		cout << "û�ҵ�" << endl;
	}
}

void test_map5()
{
	map<int, string> m;
	m.insert(make_pair(1, "one"));
	m.insert(make_pair(2, "two"));
	m.insert(make_pair(3, "three"));
	m.insert(make_pair(4, "four"));
	m.insert(make_pair(5, "five"));
	m.insert(make_pair(6, "six"));
	//ָ��keyֵɾ��
	cout << m.erase(3) << endl;
	map<int, string>::iterator pos = m.find(5);
	if (pos != m.end())
	{
		m.erase(pos, m.end());//ɾ��5��6������
	}
	for (const auto& kv : m)
	{
		cout << kv.first << ":" << kv.second << endl;
	}
}
void test_multimap()
{
	multimap<string, string> dict;
	dict.insert(make_pair("left", "���"));
	dict.insert(make_pair("left", "ʣ��"));
	dict.insert(make_pair("left", "���"));
}
int main() 
{
	map<int, string> m1;
	map<int, string> m2(m1);
	map<int, string> m3(m2.begin(), m2.end());
	map<int, string, greater<int>> m4;
	//test_map1();
	//test_map2();
	//test_map3();
	//test_map4();
	//test_map5();
	//test();
	test_multimap();
	return 0;
}
