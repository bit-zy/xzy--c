#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include"SmartPtr.h"
using namespace std;
//int div()
//{
//	int a, b;
//	cin >> a >> b;
//	if (b == 0)
//		throw invalid_argument("��0����");
//	return a / b;
//}
//void Func()
//{
	//int* p1 = new int[10];
	//int* p2 = new int[10];
	//int* p3 = new int[10];
	//int* p4 = new int[10];
//	try
//	{
//		cout << div() << endl;
//	}
//	catch (...)
//	{
//		delete[] p1;
//		delete[] p2;
//		delete[] p3;
//		delete[] p4;
//		throw;
//	}
//	delete[] p1;
//	delete[] p2;
//	delete[] p3;
//	delete[] p4;
//}
//int main()
//{
//	try
//	{
//		Func();
//	}
//	catch (exception& e)
//	{
//		cout << e.what() << endl;
//	}
//	return 0;
//}
//double div()
//{
//	double a, b;
//	cin >> a >> b;
//	if (b == 0)
//		throw invalid_argument("��0����");
//	return a / b;
//}
//void Func()
//{
//	cpp::SmartPtr<int> sp1(new int);
//	cpp::SmartPtr<int> sp2(new int);
//	cpp::SmartPtr<int> sp3(new int);
//	cpp::SmartPtr<int> sp4(new int);
//	cout << div() << endl;
//}
//int main()
//{
//	try
//	{
//		Func();
//	}
//	catch (exception& e)
//	{
//		cout << e.what() << endl;
//	}
//	return 0;
//}

//double div()
//{
//	double a, b;
//	cin >> a >> b;
//	if (b == 0)
//		throw invalid_argument("��0����");
//	return a / b;
//}
//void Func()
//{
//	cpp::SmartPtr<int> sp1(new int);
//	cpp::SmartPtr<pair<string, int>> sp2(new pair<string, int>("sort", 1));
//	*sp1 = 0;
//	sp2->second = 10;
//	sp2->first = "ten";
//}
//int main()
//{
//	try
//	{
//		Func();
//	}
//	catch (exception& e)
//	{
//		cout << e.what() << endl;
//	}
//	return 0;
//}

//int main()
//{
//	cpp::SmartPtr<int> sp1(new int);
//	cpp::SmartPtr<int> sp2(sp1);//��������
//
//	cpp::SmartPtr<int> sp3(new int);
//	cpp::SmartPtr<int> sp4(new int);
//	sp3 = sp4;//������ֵ
//	return 0;
//}
//int main()
//{
//	cpp::auto_ptr<int> ap1(new int(1));
//	cpp::auto_ptr<int> ap2(ap1);
//	cpp::auto_ptr<int> ap3 = ap2;
//	return 0;
//}

//#include<memory>
//int main()
//{
//	std::unique_ptr<int> up1(new int);
//	std::unique_ptr<int> up2(up1);//err�����ܿ���
//	return 0;
//}

//int main()
//{
//	cpp::shared_ptr<int> sp1(new int);
//	cpp::shared_ptr<int> sp2(sp1);
//	cpp::shared_ptr<int> sp3(new int);
//	sp1 = sp3;
//	sp2 = sp3;
//	std::shared_ptr<int> sp3 = sp2;
//	return 0;
//}
int main()
{
	cpp::shared_ptr<int> sp1(new int);
	cpp::shared_ptr<int> sp2(sp1);
	cpp::shared_ptr<int> sp3(new int);
	sp1 = sp3;
	sp2 = sp3;
	return 0;
}