#define _CRT_SECURE_NO_WARNINGS 1
#include<vector>
#include"BloomFilter.h"
void TestBloomFilter()
{
	//����10��ֵ������m = 4.3n��������
	//cpp::BloomFilter<43, string, BKDRHash, APHash, DJBHash> bf;
	cpp::BloomFilter<43> bf;
	string a[] = { "ƻ��", "�㽶", "����", "11111111", "eeeeffff", "��ݮ", "��Ϣ", "����", "����", "set" };
	for (auto& e : a)
	{
		bf.Set(e);
	}
	for (auto& e : a)
	{
		cout << bf.Test(e) << endl;
	}
	cout << endl;
	cout << bf.Test("â��") << endl;
	cout << bf.Test("string") << endl;
	cout << bf.Test("ffffeeee") << endl;
}

void TestBloomFilter2()
{
	srand(time(0));
	const size_t N = 100;
	cpp::BloomFilter<5 * N> bf;

	vector<string> v1;
	string url = "https://www.cnblogs.com/-clq/archive/2012/05/31/2528153.html";

	for (size_t i = 0; i < N; ++i)
	{
		v1.push_back(url + to_string(1234 + i));
	}

	for (auto& str : v1)
	{
		bf.Set(str);
	}

	/*for (auto& str : v1)
	{
	cout << bf.Test(str) << endl;
	}
	cout << endl << endl;*/

	vector<string> v2;
	for (size_t i = 0; i < N; ++i)
	{
		string url = "https://www.cnblogs.com/-clq/archive/2012/05/31/2528153.html";
		url += to_string(999999 + i);
		v2.push_back(url);
	}

	size_t n2 = 0;
	for (auto& str : v2)
	{
		if (bf.Test(str))
		{
			++n2;
		}
	}
	cout << "�����ַ���������:" << (double)n2 / (double)N << endl;

	vector<string> v3;
	for (size_t i = 0; i < N; ++i)
	{
		string url = "zhihu.com";
		url += to_string(rand());
		v3.push_back(url);
	}

	size_t n3 = 0;
	for (auto& str : v3)
	{
		if (bf.Test(str))
		{
			++n3;
		}
	}
	cout << "�������ַ���������:" << (double)n3 / (double)N << endl;
}
int main()
{
	//TestBloomFilter();
	TestBloomFilter2();
	return 0;
}

