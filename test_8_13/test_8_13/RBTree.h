#pragma once
#include<iostream>
#include<assert.h>
using namespace std;
enum Colour
{
	Red,
	Black,
};
//节点类
template <class K, class V>
struct RBTreeNode
{
	//三叉链结构
	RBTreeNode<K, V>* _left;
	RBTreeNode<K, V>* _right;
	RBTreeNode<K, V>* _parent;
	//存储的键值对
	pair<K, V> _kv;
	//节点的颜色
	Colour _col;
	//构造函数
	RBTreeNode(const pair<K, V>& kv)
		:_left(nullptr)
		, _right(nullptr)
		, _parent(nullptr)
		, _kv(kv)
		, _col(RED)
	{}
};
//红黑树的类
template <class K, class V>
class RBTree
{
	typedef RBTreeNode<K, V> Node;
public:
	bool Insert(const pair<K, V>& kv)
	{
		//1、一开始为空树，直接new新节点
		if (_root == nullptr)
		{
			_root = new Node(kv);
			_root->_col = Red;//新插入的节点处理成红色
			return true;
		}
		//2、寻找插入的合适位置
		Node* cur = _root;
		Node* parent = nullptr;
		while (cur)
		{
			if (cur->_kv.first < kv.first)
			{
				parent = cur;
				cur = cur->_right;//插入的值 > 节点的值，更新到右子树查找
			}
			else if (cur->_kv.first > kv.first)
			{
				parent = cur;
				cur = cur->_left;//插入的值 < 节点的值，更新到左子树查找
			}
			else
			{
				return false;//插入的值 = 节点的值，数据冗余插入失败，返回false
			}
		}
		//3、找到了插入的位置，进行父亲与插入节点的链接
		cur = new Node(kv);
		cur->_col = Red;//插入的节点处理成红色
		if (parent->_kv.first < kv.first)
		{
			parent->_right = cur;//插入的值 > 父亲的值，链接在父亲的右边
		}
		else
		{
			parent->_left = cur;//插入的值 < 父亲的值，链接在父亲的左边
		}
		//因为是三叉连，插入后记得双向链接（孩子链接父亲）
		cur->_parent = parent;

		//4、检测新节点插入后，红黑树的性质是否造到破坏
		while (parent && parent->_col == Red)//存在连续的红色节点
		{
			Node* grandfather = parent->_parent;
			assert(grandfather);
			//先确保叔叔的位置
			if (grandfather->_left == parent)
			{
				Node* uncle = grandfather->_right;
				//情况一：cur为红，p为红，g为黑，u存在且为红
				if (uncle && uncle->_col == Red)
				{
					//变色
					parent->_col = uncle->_col = Black;
					grandfather->_col = Red;
					//继续往上处理
					cur = grandfather;
					parent = cur->_parent;
				}
				else
				{

				}
			}
			else
			{
				Node* uncke = grandfather->_left;
				//情况一：cur为红，p为红，g为黑，u存在且为红
				if (uncle && uncle->_col == Red)
				{
					//变色
					parent->_col = uncle->_col = Black;
					grandfather->_col = Red;
					//继续往上处理
					cur = grandfather;
					parent = cur->_parent;
				}
			}
		}


		_root->_col = Black;//暴力处理把根变成黑色
		return true;
	}
private:
	Node* _root;
};