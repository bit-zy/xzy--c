#pragma once
#include<iostream>
#include<deque>
using namespace std;
namespace cpp
{
	template<class T, class Container = deque<T>>
	class stack
	{
	public:
		//入栈尾插
		void push(const T& x)
		{
			_con.push_back(x);
		}
		//出栈尾删
		void pop()
		{
			_con.pop_back();
		}
		//取栈顶数据（取队尾数据）
		const T& top()
		{
			return _con.back();//返回队尾数据
		}
		//获取有效数据个数
		size_t size()
		{
			return _con.size();
		}
		//判空
		bool empty()
		{
			return _con.empty();
		}
	private:
		Container _con;
	};
}
