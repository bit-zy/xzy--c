#pragma once
#include<iostream>
#include<string>
#include<vector>
using namespace std;

//利用仿函数将数据类型转换为整型
template<class K>
struct DefaultHash
{
	size_t operator()(const K& key)
	{
		return (size_t)key;
	}
};
//模板的特化
template<>
struct DefaultHash<string>
{
	size_t operator()(const string& key)
	{
		//BKDR哈希算法
		size_t hash = 0;
		for (auto ch : key)
		{
			hash = hash * 131 + ch;//把所有字符的ascii码值累计加起来
		}
		return hash;
	}
};

//开散列哈希桶的实现
namespace Bucket
{
	//哈希节点的类
	template<class T>
	struct HashNode
	{
		T _data;
		HashNode<T>* _next;
		//构造函数
		HashNode(const T& data)
			:_data(data)
			, _next(nullptr)
		{}
	};

	//哈希表的声明
	template<class K, class T, class KeyOfT, class HashFunc>
	class HashTable;
	//正向迭代器
	template<class K, class T, class KeyOfT, class HashFunc>
	class __HTIterator
	{
		typedef HashNode<T> Node; //哈希节点的类型
		typedef __HTIterator<K, T, KeyOfT, HashFunc> Self; //正向迭代器的类型
	public:
		Node* _node;//节点指针
		HashTable<K, T, KeyOfT, HashFunc>* _pht;//哈希表的地址
		//构造函数
		__HTIterator(){}//默认的
		__HTIterator(Node* node, HashTable<K, T, KeyOfT, HashFunc>* pht)
			:_node(node)
			, _pht(pht)
		{}

		//++运算符重载
		Self& operator++()
		{
			if (_node->_next)
			{
				_node = _node->_next;
			}
			else//当前桶已经走完，需要到下一个不为空的桶
			{
				KeyOfT kot;//取出key数据
				HashFunc hf;//转换成整型
				size_t hashi = hf(kot(_node->_data)) % _pht->_tables.size();
				++hashi;
				for (; hashi < _pht->_tables.size(); ++hashi)
				{
					if (_pht->_tables[hashi])//更新节点指针到非空的桶
					{
						_node = _pht->_tables[hashi];
						break;
					}
				}
				//没有找到不为空的桶，用nullptr去做end标识
				if (hashi == _pht->_tables.size())
				{
					_node = nullptr;
				}
			}
			return *this;
		}
		//*运算符重载
		T& operator*()
		{
			return _node->_data;//返回哈希节点中数据的引用
		}
		//->运算符重载
		T* operator->()
		{
			return &_node->_data;//返回哈希节点中数据的地址
		}

		//!=运算符重载
		bool operator!=(const Self& s) const
		{
			return _node != s._node;
		}
		//==运算符重载
		bool operator==(const Self& s) const
		{
			return _node == s._node;
		}

	};

	//unordered_map -> HashTable<K, pair<K, V>, MapKeyOfT> _ht;
	//unordered_set -> HashTable<K, K, SetKeyOfT> _ht;
	template<class K, class T, class KeyOfT, class HashFunc>
	class HashTable
	{
		//把迭代器设为HashTable的友元
		template<class K, class T, class KeyOfT, class HashFunc>
		friend class __HTIterator;

		typedef HashNode<T> Node;//哈希结点类型
	public:
		typedef __HTIterator<K, T, KeyOfT, HashFunc> iterator;//正向迭代器的类型
		
		//begin
		iterator begin()
		{
			for (size_t i = 0; i < _tables.size(); ++i)
			{
				Node* cur = _tables[i];
				//找到第一个不为空的桶的节点位置
				if (cur)
				{
					return iterator(cur, this);
				}
			}
			return end();
		}
		//end()
		iterator end()
		{
			return iterator(nullptr, this);
		}

		//析构函数
		~HashTable()
		{
			for (size_t i = 0; i < _tables.size(); i++)
			{
				Node* cur = _tables[i];
				while (cur)
				{
					Node* next = cur->_next;
					delete cur;
					cur = next;
				}
				_tables[i] = nullptr;//释放后置空
			}
		}
		//素数表
		size_t GetNextPrime(size_t prime)
		{
			const int PRIMECOUNT = 28;
			static const size_t primeList[PRIMECOUNT] =
			{
				53ul, 97ul, 193ul, 389ul, 769ul,
				1543ul, 3079ul, 6151ul, 12289ul, 24593ul,
				49157ul, 98317ul, 196613ul, 393241ul, 786433ul,
				1572869ul, 3145739ul, 6291469ul, 12582917ul, 25165843ul,
				50331653ul, 100663319ul, 201326611ul, 402653189ul, 805306457ul,
				1610612741ul, 3221225473ul, 4294967291ul
			};

			// 获取比prime大那一个素数
			size_t i = 0;
			for (; i < PRIMECOUNT; ++i)
			{
				if (primeList[i] > prime)
					return primeList[i];
			}

			return primeList[i];
		}

		//插入
		pair<iterator, bool> Insert(const T& data)
		{
			KeyOfT kot;
			//1、去除冗余
			iterator pos = Find(kot(data));
			if (pos != end())
			{
				return make_pair(pos, false);
			}
			//构建仿函数，把数据类型转为整型，便于后续建立映射关系
			HashFunc hf;
			
			//2、负载因子 == 1就扩容
			if (_tables.size() == _n)
			{
				//size_t newSize = _tables.size() == 0 ? 10 : _tables.size() * 2;
				size_t newSize = GetNextPrime(_tables.size());
				if (newSize != _tables.size())//确保合法性，不可能超过素数表的最大值
				{
					vector<Node*> newTable;
					newTable.resize(newSize, nullptr);
					for (size_t i = 0; i < _tables.size(); ++i)//遍历旧表
					{
						Node* cur = _tables[i];
						while (cur)
						{
							Node* next = cur->_next;
							size_t hashi = hf(kot(cur->_data)) % newSize;//确认映射到新表的位置
							//头插
							cur->_next = newTable[hashi];
							newTable[hashi] = cur;
							cur = next;
						}
						_tables[i] = nullptr;
					}
					newTable.swap(_tables);
				}
			}
			//3、头插
			//找到对应的映射位置
			size_t hashi = hf(kot(data));//调用仿函数取出key，并套用仿函数转成整型
			hashi %= _tables.size();
			//头插到对应的桶即可
			Node* newnode = new Node(data);
			newnode->_next = _tables[hashi];
			_tables[hashi] = newnode;
			++_n;
			return make_pair(iterator(newnode, this), false);
		}

		//查找
		iterator Find(const K& key)
		{
			//防止后续除0错误
			if (_tables.size() == 0)
			{
				return iterator(nullptr, this);
			}
			//构建仿函数，把数据类型转为整型，便于后续建立映射关系
			HashFunc hf;
			KeyOfT kot;
			//找到对应的映射下标位置
			size_t hashi = hf(key);
			//size_t hashi = HashFunc()(key);//匿名对象
			hashi %= _tables.size();
			Node* cur = _tables[hashi];
			//在此位置的链表中进行遍历查找
			while (cur)
			{
				if (kot(cur->_data) == key)
				{
					//找到了
					return iterator(cur, this);
				}
				cur = cur->_next;
			}
			//遍历结束，没有找到，返回nullptr
			return iterator(nullptr, this);
		}
		//删除
		bool Erase(const K& key)
		{
			//防止后续除0错误
			if (_tables.size() == 0)
			{
				return false;
			}
			//构建仿函数，把数据类型转为整型，便于后续建立映射关系
			HashFunc hf;
			KeyOfT kot;
			//找到key所对应的哈希桶的位置
			size_t hashi = hf(key);
			hashi %= _tables.size();
			Node* cur = _tables[hashi];
			Node* prev = nullptr;
			//删除
			while (cur)
			{
				if (kot(cur->_data) == key)
				{
					if (prev == nullptr)//头删
					{
						_tables[hashi] = cur->_next;
					}
					else//非头删
					{
						prev->_next = cur->_next;
					}
					delete cur;
					return true;
				}
				prev = cur;
				cur = cur->_next;
			}
			return false;
		}

	private:
		//指针数组
		vector<Node*> _tables;
		size_t _n = 0;//记录有效数据的个数
	};
}