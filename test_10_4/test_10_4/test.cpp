#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<string>
using namespace std;
class Solution {
public:
    void reverse(string& s, int begin, int end)
    {
        while (begin < end)
        {
            char tmp = s[begin];
            s[begin] = s[end];
            s[end] = tmp;
            begin++;
            end--;
        }
    }
    string reverseStr(string s, int k) {
        for (int i = 0; i < s.size(); i += k)
        {
            if (i + k < s.size())
            {
                reverse(s, i, i + k - 1);
            }
            else
            {
                reverse(s, i, s.size() - 1);
            }
        }
        return s;
    }
};
int main()
{
    Solution st;
    string s("abcdefg");
    cout << st.reverseStr(s, 2) << endl;
    return 0;
}