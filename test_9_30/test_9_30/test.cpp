#define _CRT_SECURE_NO_WARNINGS 1
//#include<iostream>
//using namespace std;
//bool IsPerNum(int num)
//{
//    int sum = 0;
//    for (int i = 1; i <= num / 2; i++)
//    {
//        if (num % i == 0)
//            sum += i;
//    }
//    if (sum == num)
//        return true;
//    else
//        return false;
//}
//int main()
//{
//    int n = 0;
//    int count = 0;
//    while (n--)
//    {
//        if (IsPerNum(n))
//        {
//            count++;
//        }
//    }
//    cout << count << endl;
//    return 0;
//}


//int foo(char x[14])//(char* x[14])
//{
//	cout << sizeof(x) << endl;
//	char y[10] = { 0 };
//	cout << sizeof(y) << endl;
//
//	return sizeof(x) + 10;
//}
//int main()
//{
//	char x[10] = { 0 };
//	cout << foo(x) << endl;
//	return 0;
//}



//class Base
//{
//public:
//	/*Base()
//	{
//		echo();
//	}*/
//	inline virtual void echo()
//	{
//		printf("Base");
//	}
//};
//class Derived : public Base
//{
//public:
//	/*Derived()
//	{
//		echo();
//	}*/
//	virtual void echo()
//	{
//		printf("Derived");
//	}
//};
//int main()
//{
//	Base* base = new Derived();
//	base->echo();
//	return 0;
//}
//#include<vector>
//int main()
//{
//	vector<int>v;
//	v.push_back(100);
//	v.push_back(300);
//	v.push_back(300);
//	v.push_back(300);
//	v.push_back(300);
//	v.push_back(500);
//	vector<int>::iterator itor;
//	for (itor = v.begin(); itor != v.end(); itor++)
//	{
//		if (*itor == 300)
//		{
//			itor = v.erase(itor);
//		}
//	}
//	for (itor = v.begin(); itor != v.end(); itor++)
//	{
//		cout << *itor << " ";
//	}
//	return 0;
//}


//class A
//{
//public:
//	A()
//	{
//		p();
//	}
//	virtual void p()
//	{
//		printf("A");
//	}
//	virtual ~A()
//	{
//		p();
//	}
//};
//class B : public A
//{
//public:
//	B()
//	{
//		p();
//	}
//	void p()
//	{
//		printf("B");
//	}
//	~B()
//	{
//		p();
//	}
//};
//int main()
//{
//	A* a = new B();
//	delete a;
//	return 0;
//}

//class Person {
//public:
//	 ~Person()
//	{
//		cout << "~Person()" << endl;
//	}
//};
//class Student : public Person {
//public:
//	~Student()
//	{
//		cout << "~Student()" << endl;
//	}
//};
//int main()
//{
//	Person* ptr = new Person;
//	delete ptr;//~Person()
//	Student* ptr = new Student;
//	delete ptr;//~Person()
//	return 0;
//}

//#include<iostream>
//#include<vector>
//using namespace std;
//void s(int n)
//{
//
//}
//int main()
//{
//    int n = 0;
//    cin >> n;
//    //每一行有2n-1个元素
//    vector<vector<int>> vv;
//    vv.resize(2 * n - 1, 0);
//
//    return 0;
//}


#include<iostream>
#include<vector>
using namespace std;
int main()
{
    int n = 0;
    cin >> n;
    //每一行有2n-1个元素
    int k = 2 * n - 1;
    //初始化为n行，k列
    vector<vector<int>> vv(n, vector<int>(k, 0));
    vv[0][n - 1] = 1;
    for (int i = 1; i < n; i++)
    {
        for (int j = 0; j < k; j++)
        {
            if (i + j == n - 1)
            {
                vv[i][j] = 1;
            }
            else
            {
                if (j + 1 >= k)
                {
                    vv[i][j] = vv[i - 1][j - 1] + vv[i - 1][j];
                }
                else if (j - 1 < 0)
                {
                    vv[i][j] = vv[i - 1][j] + vv[i - 1][j + 1];
                }
                else
                {
                    vv[i][j] = vv[i - 1][j - 1] + vv[i - 1][j] + vv[i - 1][j + 1];
                }
            }
        }
    }
    int i = n - 1;
    for (int j = 0; j < k; j++)
    {
        if (vv[i][j] != 0 && vv[i][j] % 2 == 0)
        {
            cout << j << endl;
            return 0;
        }
    }
    cout << -1 << endl;
    return 0;
}