﻿#pragma once
#include<iostream>
#include<queue>
#include<vector>
#include<assert.h>
using namespace std;
enum Colour
{
	Red,
	Black,
};
//节点类
template <class K, class V>
struct RBTreeNode
{
	//三叉链结构
	RBTreeNode<K, V>* _left;
	RBTreeNode<K, V>* _right;
	RBTreeNode<K, V>* _parent;
	//存储的键值对
	pair<K, V> _kv;
	//节点的颜色
	Colour _col;
	//构造函数
	RBTreeNode(const pair<K, V>& kv)
		:_left(nullptr)
		, _right(nullptr)
		, _parent(nullptr)
		, _kv(kv)
		, _col(Red)
	{}
};
//红黑树的类
template <class K, class V>
class RBTree
{
	typedef RBTreeNode<K, V> Node;
public:
	bool Insert(const pair<K, V>& kv)
	{
	//1、一开始为空树，直接new新节点
		if (_root == nullptr)
		{
			_root = new Node(kv);
			_root->_col = Black;//新插入的节点处理成黑色
			return true;
		}
	//2、寻找插入的合适位置
		Node* cur = _root;
		Node* parent = nullptr;
		while (cur)
		{
			if (cur->_kv.first < kv.first)
			{
				parent = cur;
				cur = cur->_right;//插入的值 > 节点的值，更新到右子树查找
			}
			else if (cur->_kv.first > kv.first)
			{
				parent = cur;
				cur = cur->_left;//插入的值 < 节点的值，更新到左子树查找
			}
			else
			{
				return false;//插入的值 = 节点的值，数据冗余插入失败，返回false
			}
		}
	//3、找到了插入的位置，进行父亲与插入节点的链接
		cur = new Node(kv);
		cur->_col = Red;//插入的节点处理成红色
		if (parent->_kv.first < kv.first)
		{
			parent->_right = cur;//插入的值 > 父亲的值，链接在父亲的右边
		}
		else
		{
			parent->_left = cur;//插入的值 < 父亲的值，链接在父亲的左边
		}
		cur->_parent = parent;//三叉链，要双向链接

	//4、检测新节点插入后，红黑树的性质是否造到破坏
		while (parent && parent->_col == Red)//存在连续的红色节点
		{
			Node* grandfather = parent->_parent;
			assert(grandfather);
			//先确保叔叔的位置
			if (grandfather->_left == parent)
			{
				Node* uncle = grandfather->_right;
				//情况一：cur为红，p为红，g为黑，u存在且为红
				if (uncle && uncle->_col == Red)
				{
					//变色
					parent->_col = uncle->_col = Black;
					grandfather->_col = Red;
					//继续往上处理
					cur = grandfather;
					parent = cur->_parent;
				}
				//情况二+情况三：叔叔不存在，或者叔叔存在且为黑
				else
				{
					if (cur == parent->_left)//p为g的左，cur为p的左，则进行右单旋 + p变黑，g变红
					{
						//		  g
						//     p
						// cur
						RotateR(grandfather);
						parent->_col = Black;
						grandfather->_col = Red;
					}
					else//p是g的左，cur是p的右，则进行左右双旋 + cur变黑， g变红
					{
						//		  g
						//	 p
						//	     cur
						RotateLR(grandfather); 
						cur->_col = Black;
						grandfather->_col = Red;
					}
					break;
				}
			}
			else//grandfather->_right == parent
			{
				Node* uncle = grandfather->_left;
				//情况一：cur为红，p为红，g为黑，u存在且为红
				if (uncle && uncle->_col == Red)
				{
					//变色
					parent->_col = uncle->_col = Black;
					grandfather->_col = Red;
					//继续往上处理
					cur = grandfather;
					parent = cur->_parent;
				}
				//情况二+情况三：叔叔不存在，或者叔叔存在且为黑
				else
				{
					if (cur == parent->_right)//p为g的右，cur为p的右，则进行左单旋 + p变黑，g变红
					{
						//	g
						//	   p
						//	     cur
						RotateL(grandfather);
						parent->_col = Black;
						grandfather->_col = Red;
					}
					else//p是g的右，cur是p的左，则进行右左双旋 + cur变黑， g变红
					{
						//   g
						//	      p
						//	cur
						RotateRL(grandfather); 
						cur->_col = Black;
						grandfather->_col = Red;
					}
					break;
				}
			}
		}
		_root->_col = Black;//暴力处理把根变成黑色
		return true;
	}
//验证是否为一颗搜索二叉树
	void InOrder()
	{
		_InOrder(_root);//调用中序遍历子树
		cout << endl;
	}
//验证是否为红黑树
	bool IsBalanceTree()
	{
		Node* pRoot = _root;
		// 空树也是红黑树
		if (pRoot == nullptr)
			return true;
		// 检测根节点是否满足情况
		if (pRoot->_col != Black)
		{
			cout << "违反红黑树性质二：根节点必须为黑色" << endl;
			return false;
		}
		// 获取任意一条路径中黑色节点的个数-->拿最左路径作为比较基准值
		size_t blackCount = 0;
		Node* pCur = pRoot;
		while (pCur)
		{
			if (pCur->_col == Black )
				blackCount++;
			pCur = pCur->_left;
		}
		// 检测是否满足红黑树的性质，k用来记录路径中黑色节点的个数
		size_t k = 0;
		return _IsValidRBTree(pRoot, k, blackCount);
	}

//求一棵树的高度
	void Height()
	{
		cout << "最长路径：" << _maxHeight(_root) << endl;
		cout << "最短路径：" << _minHeight(_root) << endl;
	}
private:
	//1、左单旋
	void RotateL(Node* parent)
	{
		Node* subR = parent->_right;
		Node* subRL = subR->_left;
		Node* ppNode = parent->_parent;//提前保持parent的父亲
		//1、建立parent和subRL之间的关系
		parent->_right = subRL;
		if (subRL)//防止subRL为空
		{
			subRL->_parent = parent;
		}
		//2、建立subR和parent之间的关系
		subR->_left = parent;
		parent->_parent = subR;
		//3、建立ppNode和subR之间的关系
		if (parent == _root)
		{
			_root = subR;
			_root->_parent = nullptr;
		}
		else
		{
			if (parent == ppNode->_left)
			{
				ppNode->_left = subR;
			}
			else
			{
				ppNode->_right = subR;
			}
			subR->_parent = ppNode;//三叉链双向链接关系
		}
	}
	//2、右单旋
	void RotateR(Node* parent)
	{
		Node* subL = parent->_left;
		Node* subLR = subL->_right;
		Node* ppNode = parent->_parent;
		//1、建立parent和subLR之间的关系
		parent->_left = subLR;
		if (subLR)
		{
			subLR->_parent = parent;
		}
		//2、建立subL和parent之间的关系
		subL->_right = parent;
		parent->_parent = subL;
		//3、建立ppNode和subL的关系
		if (parent == _root)
		{
			_root = subL;
			_root->_parent = nullptr;
		}
		else
		{
			if (parent == ppNode->_left)
			{
				ppNode->_left = subL;
			}
			else
			{
				ppNode->_right = subL;
			}
			subL->_parent = ppNode;//三叉链双向关系
		}
	}
	//3、左右双旋
	void RotateLR(Node* parent)
	{
		RotateL(parent->_left);
		RotateR(parent);
	}

	//4、右左双旋
	void RotateRL(Node* parent)
	{
		RotateR(parent->_right);
		RotateL(parent);
	}
//中序遍历的子树
	void _InOrder(Node* root)
	{
		if (root == nullptr)
			return;

		_InOrder(root->_left);
		cout << root->_kv.first << " ";
		_InOrder(root->_right);
	}
//求一棵树的最长路径的子树
	int _maxHeight(Node* root)
	{
		if (root == nullptr)
			return 0;
		int lh = _maxHeight(root->_left);
		int rh = _maxHeight(root->_right);
		return lh > rh ? lh + 1 : rh + 1;
	}
//求一棵树的最短路径的子树
	int _minHeight(Node* root)
	{
		if (root == nullptr)
			return 0;
		int lh = _minHeight(root->_left);
		int rh = _minHeight(root->_right);
		return lh < rh ? lh + 1 : rh + 1;
	}
//求是否满足红黑树性质的子树
	bool _IsValidRBTree(Node* pRoot, size_t k, const size_t blackCount)
	{
		//走到null之后，判断k和black是否相等
		if (pRoot == nullptr)
		{
			if (k != blackCount)
			{
				cout << "违反性质四：每条路径中黑色节点的个数必须相同" << endl;
				return false;
			}
			return true;
		}
		// 统计黑色节点的个数
		if (pRoot->_col == Black)
			k++;
		// 检测当前节点与其双亲是否都为红色
		Node* pParent = pRoot->_parent;
		if (pParent && pParent->_col == Red && pRoot->_col == Red)
		{
			cout << "违反性质三：没有连在一起的红色节点，而这里出现了" << endl;
			return false;
		}
		return _IsValidRBTree(pRoot->_left, k, blackCount) && _IsValidRBTree(pRoot->_right, k, blackCount);
	}
public:
		//层序遍历（非必须）
	vector<vector<int>> levelOrder() {
		vector<vector<int>> vv;
		if (_root == nullptr)
			return vv;
		queue<Node*> q;
		int levelSize = 1;
		q.push(_root);
		while (!q.empty())
		{
			// levelSize控制一层一层出
			vector<int> levelV;
			while (levelSize--)
			{
				Node* front = q.front();
				q.pop();
				levelV.push_back(front->_kv.first);
				if (front->_left)
					q.push(front->_left);

				if (front->_right)
					q.push(front->_right);
			}
			vv.push_back(levelV);
			for (auto e : levelV)
			{
				cout << e << " ";
			}
			cout << endl;
			// 上一层出完，下一层就都进队列
			levelSize = q.size();
		}
		return vv;
	}

private:
	Node* _root = nullptr;
};