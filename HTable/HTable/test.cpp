#define _CRT_SECURE_NO_WARNINGS 1
#include<map>
#include<string>
#include<set>
#include "HashTable.h"
#include "Unordered_Map.h"
#include "Unordered_Set.h"

void test_set()
{
	struct Date
	{
		Date(int year = 1, int month = 1, int day = 1)
			:_year(year)
			, _month(month)
			, _day(day)
		{}
		bool operator==(const Date& d) const
		{
			return _year == d._year 
				&& _month == d._month
				&& _day == d._day;
		}
		int _year;
		int _month;
		int _day;
	};
	struct DateHash
	{
		size_t operator()(const Date& d)
		{
			size_t hash = 0;
			hash += d._year;
			hash *= 131;
			hash += d._month;
			hash *= 1313;
			hash += d._day;
			return hash;
		}
	};
	cpp::unordered_set<Date, DateHash> sd;
	sd.insert(Date(2022, 3, 4));
	sd.insert(Date(2022, 4, 3));




	cpp::unordered_set<int> s;
	//set<int> s;
	s.insert(2);
	s.insert(3);
	s.insert(1);
	s.insert(2);
	s.insert(5);
	s.insert(12);

	cpp::unordered_set<int>::iterator it = s.begin();
	//auto it = s.begin();
	while (it != s.end())
	{
		cout << *it << " ";
		++it;
	}
	cout << endl;
	
	for (auto e : s)
	{
		cout << e << " ";
	}
	cout << endl;
	
}
//void test_op()
//{
//	int n = 10000000;
//	vector<int> v;
//	v.reserve(n);
//	srand(time(0));
//	for (int i = 0; i < n; ++i)
//	{
//		//v.push_back(i);
//		//v.push_back(rand()+i);  // �ظ���
//		v.push_back(rand());  // �ظ���
//	}
//
//	size_t begin1 = clock();
//	set<int> s;
//	for (auto e : v)
//	{
//		s.insert(e);
//	}
//	size_t end1 = clock();
//
//	size_t begin2 = clock();
//	cpp::unordered_set<int> us;
//	for (auto e : v)
//	{
//		us.insert(e);
//	}
//	size_t end2 = clock();
//
//	cout << s.size() << endl;
//
//	cout << "set insert:" << end1 - begin1 << endl;
//	cout << "unordered_set insert:" << end2 - begin2 << endl;
//
//
//	size_t begin3 = clock();
//	for (auto e : v)
//	{
//		s.find(e);
//	}
//	size_t end3 = clock();
//
//	/*size_t begin4 = clock();
//	for (auto e : v)
//	{
//		us.find(e);
//	}
//	size_t end4 = clock();
//	cout << "set find:" << end3 - begin3 << endl;
//	cout << "unordered_set find:" << end4 - begin4 << endl;*/
//
//
//	size_t begin5 = clock();
//	for (auto e : v)
//	{
//		s.erase(e);
//	}
//	size_t end5 = clock();
//
//	/*size_t begin6 = clock();
//	for (auto e : v)
//	{
//		us.erase(e);
//	}
//	size_t end6 = clock();
//	cout << "set erase:" << end5 - begin5 << endl;
//	cout << "unordered_set erase:" << end6 - begin6 << endl;*/
//}
void test_map()
{
	cpp::unordered_map<string, string> dict;
	dict.insert(make_pair("sort", "����"));
	dict.insert(make_pair("left", "���"));
	dict.insert(make_pair("left", "ʣ��"));
	dict["string"];
	dict["left"] = "ʣ��";
	dict["string"] = "�ַ���";
	cpp::unordered_map<string, string>::iterator it = dict.begin();
	while (it != dict.end())
	{
		cout << it->first << " " << it->second << endl;
		++it;
	}
	for (auto& kv : dict)
	{
		cout << kv.first << " " << kv.second << endl;
	}
}

int main()
{
	//test_set();
	//test_op();
	test_map();

	return 0;
}


