#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;
//class A
//{
//public:
//	A()
//	{
//		cout << "构造函数" << endl;
//	}
//	~A()
//	{
//		cout << "析构函数" << endl;
//	}
//};
//int main()
//{
//	A* p = new A[5];
//	delete p;
//	return 0;
//}


//class A
//{
//public:
//	A(int i = 0)
//	{
//		cout << 1;
//	}
//	A(const A& x)
//	{
//		cout << 2;
//	}
//	A& operator=(const A& x)
//	{
//		cout << 3;
//		return *this;
//	}
//	~A()
//	{
//		cout << 4;
//	}
//};
//int main()
//{
//	A p1(1), p2(2), p3(p1);
//	//  112444
//	return 0;
//}


//void func(int& x)
//{
//	cout << endl;
//}
//int main()
//{
//	int a = 3;
//	func(a);
//}

//class UnusualAdd {
//public:
//    int addAB(int A, int B) {
//        // write code here
//        int sum = 0;
//        int x = A;
//        int y = B;
//        if (A < 0 && B > 0)
//        {
//            x = A;
//            y = B;
//        }
//        if (B < 0 && A > 0)
//        {
//            x = B;
//            y = A;
//        }
//        if (A < 0 && B < 0)
//        {
//            while (--abs(A))
//            {
//                sum++;
//            }
//            while (abs(B)--)
//            {
//                sum++;
//            }
//        }
//    }
//};

#include<iostream>
using namespace std;
#include<vector>
class Solution {
public:
    vector<int> searchRange(vector<int>& nums, int target) {
        int leftBorder = getLeftBorder(nums, target);
        int rightBorder = getRightBorder(nums, target);
        // 情况一
        if (leftBorder == -2 || rightBorder == -2) return { -1, -1 };
        // 情况三
        if (rightBorder - leftBorder > 1) return { leftBorder + 1, rightBorder - 1 };
        // 情况二
        return { -1, -1 };
    }
private:
    int getRightBorder(vector<int>& nums, int target) {
        int left = 0;
        int right = nums.size() - 1;
        int rightBorder = -2; // 记录一下rightBorder没有被赋值的情况
        while (left <= right) {
            int middle = left + ((right - left) / 2);
            if (nums[middle] > target) {
                right = middle - 1;
            }
            else { // 寻找右边界，nums[middle] == target的时候更新left
                left = middle + 1;
                rightBorder = left;
            }
        }
        return rightBorder;
    }
    int getLeftBorder(vector<int>& nums, int target) {
        int left = 0;
        int right = nums.size() - 1;
        int leftBorder = -2; // 记录一下leftBorder没有被赋值的情况
        while (left <= right) {
            int middle = left + ((right - left) / 2);
            if (nums[middle] >= target) { // 寻找左边界，nums[middle] == target的时候更新right
                right = middle - 1;
                leftBorder = right;
            }
            else {
                left = middle + 1;
            }
        }
        return leftBorder;
    }
};
int main()
{
    vector<int> nums{ 5,7,7,8,8,10 };
    Solution s;
    s.searchRange(nums, 8);
    return 0;
}