#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;
//struct A
//{
//	void foo()
//	{
//		cout << "foo";
//	}
//	virtual void bar()
//	{
//		cout << "bar";
//	}
//	A()
//	{
//		bar();
//	}
//};
//struct B : A
//{
//	void foo()
//	{
//		cout << "b_foo";
//	}
//	void bar()
//	{
//		cout << "b_bar";
//	}
//};
//
//int main()
//{
//	A* p = new B;
//	p->foo();
//	p->bar();
//	return 0;
//}

//class A
//{
//public:
//	virtual void x()
//	{
//
//	}
//};
//class B
//{
//public:
//	void x()
//	{
//
//	}
//};
//int main()
//{
//	B b;
//	b.x();
//}

//class A
//{
//public:
//	virtual void f()
//	{
//		printf("A\n");
//	}
//};
//class B : public A
//{
//public:
//	virtual void f()
//	{
//		printf("B\n");
//	}
//};
//int main()
//{
//	A* a = new B;
//	a->f();
//	delete a;
//	return 0;
//}

//class A
//{
//public:
//	A()
//	{
//		printf("A ");
//	}
//	~A()
//	{
//		printf("deA ");
//	}
//};
//class B
//{
//public:
//	B()
//	{
//		printf("B ");
//	}
//	~B()
//	{
//		printf("deB ");
//	}
//};
//class C : public A, public B
//{
//public:
//	C()
//	{
//		printf("C ");
//	}
//	~C()
//	{
//		printf("deC ");
//	}
//};
//int main()
//{
//	A* a = new C();
//	delete a;
//	return 0;
//}

class A
{
public:
	void foo()
	{
		printf("1");
	}
	virtual void fun()
	{
		printf("2");
	}
};
class B : public A
{
public:
	void foo()
	{
		printf("3");
	}
	void fun()
	{
		printf("4");
	}
};
int main()
{
	A a;
	B b;
	A* p = &a;
	p->foo();
	p->fun();
	p = &b;
	p->foo();
	p->fun();
	A* ptr = (A*)&b;
	ptr->foo();
	ptr->fun();
	return 0;
}