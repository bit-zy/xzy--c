#define _CRT_SECURE_NO_WARNINGS 1
#include"BSTree_key_value.h"
namespace key_value
{
	void TestBSTree1()
	{
		BSTree<string, string> ECdict;
		ECdict.InsertR("root", "根");
		ECdict.InsertR("string", "字符串");
		ECdict.InsertR("left", "左边");
		ECdict.InsertR("insert", "插入");
		ECdict.InsertR("ShuanQ", "拴Q");
		string str;
		while (cin >> str)
		{
			//BSTreeNode<string, string>* ret = ECdict.FindR(str);
			auto ret = ECdict.FindR(str);
			if (ret != nullptr)
			{
				cout << "对应的中文：" << ret->_value << endl;
			}
			else
			{
				cout << "无此单词，请重写输入" << endl;
			}
		}
	}

	void TestBSTree2()
	{
		string arr[] = { "苹果", "西瓜", "苹果", "西瓜", "苹果", "苹果", "西瓜","苹果", "香蕉", "苹果", "香蕉" };
		//统计水果出现的次数
		BSTree<string, int> countTree;
		int count = 0;
		for (const auto& str : arr)
		{
			auto ret = countTree.FindR(str);
			if (ret == nullptr)
			{
				countTree.InsertR(str, 1);
			}
			else
			{
				ret->_value++;//修改value
			}
		}
		countTree.InOrder();
	}

}

int main()
{
	//key_value::TestBSTree1();
	key_value::TestBSTree2();
}