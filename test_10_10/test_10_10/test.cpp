#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;
#include<iostream>
using namespace std;
//int main()
//{
//    int n = 0;
//    while (cin >> n)
//    {
//        int t = n;
//        int sum = 0;
//        while (t)
//        {
//            sum += t / 3;
//            t = t / 3 + t % 3;
//            if (t < 3)
//                break;
//        }
//        if (t == 2)
//            sum += 1;
//        cout << sum << endl;
//    }
//    return 0;
//}

#include<vector>
#include<string>
#include<stack>
class Solution {
public:
    int evalRPN(vector<string>& tokens) {
        stack<int> st;
        for (int i = 0; i < tokens.size(); i++)
        {
            if (tokens[i] != '+' && tokens[i] != '-' && tokens[i] != '*' && tokens[i] != '/')
            {
                st.push(stoi([i]));
            }
            else
            {
                int num1 = st.top();
                st.pop();
                int num2 = st.top();
                st.pop();
                if (tokens[i] == '+')
                    st.push(num1 + num2);
                else if (tokens[i] == '-')
                    st.push(num2 - num1);
                else if (tokens[i] == '*')
                    st.push(num1 * num2);
                else if (tokens[i] == '/')
                    st.push(num2 / num1);
            }
        }
        return st.top();
    }
};