#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<Windows.h>
#include<string>
using namespace std;
namespace cpp
{
	// 服务器开发中通常使用的异常继承体系
	//基类
	class Exception
	{
	public:
		Exception(const string& errmsg, int id)
			:_errmsg(errmsg)
			, _id(id)
		{}
		virtual string what() const
		{
			return _errmsg;
		}
		int getid() const
		{
			return _id;
		}
	protected:
		string _errmsg;//描述错误信息
		int _id;//错误编码
	};
	//数据库组
	class SqlException : public Exception
	{
	public:
		SqlException(const string& errmsg, int id, const string& sql)
			:Exception(errmsg, id)
			, _sql(sql)
		{}
		virtual string what() const
		{
			string str = "SqlException:";
			str += _errmsg;
			str += "->";
			str += _sql;
			return str;
		}
	private:
		const string _sql;
	};
	//缓存组
	class CacheException : public Exception
	{
	public:
		CacheException(const string& errmsg, int id)
			:Exception(errmsg, id)
		{}
		virtual string what() const
		{
			string str = "CacheException:";
			str += _errmsg;
			return str;
		}
	};
	//协议组
	class HttpServerException : public Exception
	{
	public:
		HttpServerException(const string& errmsg, int id, const string& type)
			:Exception(errmsg, id)
			, _type(type)
		{}
		virtual string what() const
		{
			string str = "HttpServerException:";
			str += _type;
			str += ":";
			str += _errmsg;
			return str;
		}
	private:
		const string _type;
	};
	//抛数据库组的异常
	void SQLMgr()
	{
		if (rand() < RAND_MAX / 10)//10%的概率抛权限不足的异常
		{
			throw SqlException("权限不足", 100, "select * from name = '张三'");
		}
		else
		{
			cout << "Sql success" << endl;
		}
	}
	//抛缓存组的异常
	void CacheMgr()
	{
		if (rand() < RAND_MAX / 10)//%10的概率抛权限不足的异常
		{
			throw CacheException("权限不足", 100);
		}
		else if (rand() < RAND_MAX / 25)//%25的概率抛数据不存在的异常
		{
			throw CacheException("数据不存在", 101);
		}
		else
		{
			cout << "Cache success" << endl;
		}
		SQLMgr();
	}
	//发生消息
	void SeedMsg(const string& str)
	{
		if (rand() < RAND_MAX - 10000)//大概率抛网络错误的异常
		{
			throw HttpServerException("SeedMsg::网络错误", 2, "put");
		}
		else if (rand() < RAND_MAX / 10)//%10的概率抛权限不足的异常
		{
			throw HttpServerException("SeedMsg::权限不足，你已经不是对方好友", 1, "post");
		}
		else
		{
			cout << "消息发送成功！->" << str << endl;
		}
	}
	//抛协议组的异常
	void HttpServer()
	{
		if (rand() < RAND_MAX / 10)//%10的概率抛请求资源不存在的异常
		{
			throw HttpServerException("请求资源不存在", 404, "get");
		}
		else if (rand() < RAND_MAX / 10)//%10的概率抛权限不足的异常
		{
			throw HttpServerException("权限不足", 501, "post");
		}
		else
		{
			cout << "Http success" << endl;
		}
		CacheMgr();
	}
}
int main()
{
	srand(time(0));
	while (1)
	{
		::Sleep(1000);
		try
		{
			//cpp::HttpServer();
			//发送消息出现网络错误，要求重试10次
			//权限错误就直接报错，
			for (size_t i = 0; i < 10; i++)
			{
				try
				{
					cpp::SeedMsg("明天星期八!!!");
					break;//如果发送成功了，直接break退出，防止发送成功还进入循环
				}
				catch (const cpp::Exception& e)
				{
					if (e.getid() == 2)//异常编码的价值，可以针对某个错误进行特殊处理
					{
						cout << "网络错误，重试发消息第" << i << "次" << endl;
						continue;
					}
					else//其它错误
					{
						//break;不能break，否则当出现权限错误时，根本不会走到这一步
						throw e;//重新抛出异常
					}
				}
			}
			
		}
		catch (const cpp::Exception& e) // 这里捕获父类对象就可以
		{
			// 多态
			cout << e.what() << endl;
		}
		catch (const std::exception& e) // 这里捕获库里的基类对象，捕获类似出现new失败等库里抛的异常
		{
			// 多态
			cout << e.what() << endl;
		}
		catch (...)
		{
			cout << "Unkown Exception" << endl;
		}
	}
	return 0;
}

