#pragma once
#include<iostream>
using namespace std;
//namespace cpp
	//{
	//	template<class T>
	//	class SmartPtr
	//	{
	//	public:
	//		//RAII思想
	//		SmartPtr(T* ptr)
	//			:_ptr(ptr)
	//		{}
	//		~SmartPtr()
	//		{
	//			cout << "delete: " << _ptr << endl;//方便观察谁释放
	//			delete _ptr;
	//		}
	//		//像指针一样使用
	//		T& operator*()
	//		{
	//			return *_ptr;
	//		}
	//		T* operator->()
	//		{
	//			return _ptr;
	//		}
	//		T* Get()
	//		{
	//			return _ptr;
	//		}
	//	private:
	//		T* _ptr;
	//	};
	//}
//1、auto_ptr的模拟实现
//namespace cpp
//{
//	template<class T>
//	class auto_ptr
//	{
//	public:
//		//构造函数
//		auto_ptr(T* ptr)
//			:_ptr(ptr)
//		{}
//		//析构函数
//		~auto_ptr()
//		{
//			if (_ptr != nullptr)
//			{
//				cout << "delete: " << _ptr << endl;//方便观察谁释放
//				delete _ptr;
//				_ptr = nullptr;
//			}
//		}
//		//拷贝构造函数 sp2(sp1)
//		auto_ptr(auto_ptr<T>& ap)
//			:_ptr(ap._ptr)
//		{
//			ap._ptr = nullptr;
//		}
//		//拷贝赋值函数
//		auto_ptr& operator=(auto_ptr<T>& ap)
//		{
//			if (this != &ap)
//			{
//				delete _ptr;       //释放自己管理的资源
//				_ptr = ap._ptr;    //接管ap对象的资源
//				ap._ptr = nullptr; //管理权转移后ap被置空
//			}
//			return *this;
//		}
//		//*运算符重载
//		T& operator*()
//		{
//			return *_ptr;
//		}
//		//->运算符重载
//		T* operator->()
//		{
//			return _ptr;
//		}
//		T* get()
//		{
//			return _ptr;
//		}
//	private:
//		T* _ptr;
//	};
//}

//2、unique_ptr的模拟实现
//namespace cpp
//{
//	template<class T>
//	class unique_ptr
//	{
//	public:
//		//构造函数
//		unique_ptr(T* ptr)
//			:_ptr(ptr)
//		{}
//		//析构函数
//		~unique_ptr()
//		{
//			if (_ptr != nullptr)
//			{
//				cout << "delete: " << _ptr << endl;//方便观察谁释放
//				delete _ptr;
//				_ptr = nullptr;
//			}
//		}
//		//*运算符重载
//		T& operator*()
//		{
//			return *_ptr;
//		}
//		//->运算符重载
//		T* operator->()
//		{
//			return _ptr;
//		}
//		T* get()
//		{
//			return _ptr;
//		}
//		//法一：C++11
//		unique_ptr(const unique_ptr<T>& up) = delete;
//		unique_ptr<T>& operator=(const unique_ptr<T>& up) = delete;
//		//法二：C++98
//	/*private:
//		//1、只声明，不实现
//		//2、声明成私有
//		unique_ptr(unique_ptr<T>& up);//拷贝构造
//		unique_ptr& operator=(unique_ptr<T>& up);//拷贝赋值*/
//	private:
//		T* _ptr;
//	};
//}

//3、shared_ptr的模拟实现
namespace cpp
{
	template<class T>
	class shared_ptr
	{
	public:
		//释放函数
		void Release()
		{
			if (--(*_pCount) == 0 && _ptr)//每走一次析构，计数就--，直到计数为0时才释放管理的资源
			{
				cout << "delete: " << _ptr << endl;//方便观察谁释放
				//释放资源和计数
				delete _ptr;
				_ptr = nullptr;
				delete _pCount;
				_pCount = nullptr;
			}
		}
		//构造函数
		shared_ptr(T* ptr)
			:_ptr(ptr)
			, _pCount(new int(1))//构造一个资源就把对应的计数设为1
		{}
		//析构函数
		~shared_ptr()
		{
			Release();
		}
		//拷贝构造
		shared_ptr(const shared_ptr<T>& sp)
			:_ptr(sp._ptr)
			, _pCount(sp._pCount)
		{
			(*_pCount)++;//每拷贝一个对象就对计数++
		}
		//拷贝赋值sp1 = sp3
		shared_ptr<T>& operator=(const shared_ptr<T>& sp)
		{
			if (_ptr != sp._ptr)//管理同一块资源的对象之间不能进行赋值操作
			{
				Release();//需要先--sp1的计数，因为sp1托管其它资源了，减去自己原先的计数，并且计数减到0时释放sp1之前管理的资源，统一放到Release函数处理
				_ptr = sp._ptr;//把sp3的资源赋给sp1
				_pCount = sp._pCount;//把sp3的计数赋给sp1
				++(*_pCount);//此时sp3和sp1共同托管sp3的资源，相应的计数++
			}
			return *this;
		}
		//*运算符重载
		T& operator*()
		{
			return *_ptr;
		}
		//->运算符重载
		T* operator->()
		{
			return _ptr;
		}
		T* get()
		{
			return _ptr;
		}
	private:
		T* _ptr; //管理的资源
		int* _pCount;//管理的资源对应的引用计数
	};
}