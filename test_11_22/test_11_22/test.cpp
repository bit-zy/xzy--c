#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<fstream>
using namespace std;
//int main()
//{
//	ifstream ifs("test.cpp");
//	//法一：
//	while (ifs)
//	{
//		char ch = ifs.get();
//		cout << ch;
//	}
//	/*法二
//	char ch;
//	while (ifs >> ch)
//	{
//		cout << ch;这里读出的结果不换行，过滤掉了空格和换行
//	}*/
//	return 0;
//}

//int main()
//{
//	ifstream ifs("test.cpp");
//	ofstream ofs("copy.cpp");
//	char ch = ifs.get();
//	while (~ch)
//	{
//		ofs << ch;
//		ch = ifs.get();
//	}
//	return 0;
//}


////以二进制的形式对文件进行写入
//int main()
//{
//	ofstream ofile; //定义文件流对象
//	ofile.open("test.txt", ofstream::out | ofstream::binary); //以二进制写入的方式打开test.txt文件
//	char array[] = "hello world";
//	ofile.write(array, strlen(array)); //将array字符串写入文件
//	ofile.put('#'); //将字符'#'写入文件
//	ofile.close(); //关闭文件
//	//此时test.txt文件仅有"hello world#"
//	return 0;
//}


////以二进制的形式对文件进行读取
//int main()
//{
//	ifstream ifile;//定义文件流对象
//	ifile.open("test.txt", ofstream::in | ofstream::binary);//以二进制的读取的方式打开test.doc文件
//	ifile.seekg(0, ifile.end);//跳转到文件末尾
//	int len = ifile.tellg();//获取当前字符在文件当中的位置，既文件的字符总数
//	ifile.seekg(0, ifile.beg);;//重新回到文件开头
//	char array[100];
//	ifile.read(array, len);//将文件当中的数据全部读取到字符串array当中
//	cout << len << endl;
//	cout << array << endl;
//	ifile.close();//关闭文件
//	return 0;
//}

//以文本的形式对文件进行写入
//int main()
//{
//	ofstream ofile;//定义文件流对象
//	ofile.open("test.txt");//以写入的方式打开test.txt文件
//	char array[] = "hello world";
//	ofile.write(array, strlen(array));//将array字符串写入文件
//	ofile.put('#');//将字符'#'写入文件
//	ofile.close();//关闭文件
//	return 0;
//}

////以文本的形式对文件进行读取
//int main()
//{
//	ifstream ifile;//定义文件流对象
//	ifile.open("test.txt");//以读取的方式打开test.txt文件
//	ifile.seekg(0, ifile.end);//跳转到文件末尾
//	int len = ifile.tellg();//获取当前字符在文件中的位置，即文件的字符总数
//	ifile.seekg(0, ifile.beg);//重新回到文件开头
//	char array[100];
//	ifile.read(array, len);//将文件中的数据全部读取到字符串data中
//	cout << len << endl;
//	cout << array << endl;
//	ifile.close();
//	return 0;
//}

//struct ServerInfo
//{
//	const char _address[256] = "";
//	int _port;
//};
//struct ConfigManager
//{
//public:
//	ConfigManager(const char* filename)
//		:_filename(filename)
//	{}
//	//以二进制写
//	void WriteBin(const ServerInfo& info)
//	{
//		ofstream ofs(_filename, ios_base::out | ios_base::binary);
//		ofs.write((const char*)&info, sizeof(info));
//	}
//	//以二进制读
//	void ReadBin(ServerInfo& info)
//	{
//		ifstream ifs(_filename, ios_base::in | ios_base::binary);
//		ifs.read((char*)&info, sizeof(info));
//	}
//private:
//	string _filename; // 配置文件
//};
//int main()
//{
//	//以二进制写
//	ServerInfo winfo = { "https://legacy.cplusplus.com/reference/ios/", 80 };
//	ConfigManager cf_bin("test.bin");
//	cf_bin.WriteBin(winfo);
//	//以二进制读
//	ServerInfo rbinfo;
//	cf_bin.ReadBin(rbinfo);
//	cout << rbinfo._address << " " << rbinfo._port << " ";//https://legacy.cplusplus.com/reference/ios/ 80
//	return 0;
//}

#include<string>
#include<iostream>
using namespace std;
class Date
{
	//友元函数
	friend ostream& operator<<(ostream& out, const Date& d);//流插入 <<
	friend istream& operator>>(istream& in, Date& d);//流提取 >>
public:
	Date(int year = 1, int month = 1, int day = 1)
	{
		_year = year;
		_month = month;
		_day = day;
	}
private:
	int _year;
	int _month;
	int _day;
};
//流插入 <<
ostream& operator<<(ostream& out, const Date& d)
{
	out << d._year << "-" << d._month << "-" << d._day << endl;
	return out;
}
//流提取 >>
istream& operator>>(istream& in, Date& d)
{
	in >> d._year >> d._month >> d._day;
	return in;
}
struct ServerInfo
{
	string _address;
	int _port;
	Date _d;
};
struct ConfigManager
{
public:
	ConfigManager(const char* filename)
		:_filename(filename)
	{}
	//以文本方式写
	void WriteText(const ServerInfo& info)
	{
		ofstream ofs(_filename);
		ofs << info._address << endl;
		ofs << info._port << endl;
		ofs << info._d << endl;
	}
	//以文本方式读
	void ReadText(ServerInfo& info)
	{
		ifstream ifs(_filename);
		ifs >> info._address;
		ifs >> info._port;
		ifs >> info._d;
	}

private:
	string _filename; // 配置文件
};
int main()
{
	ServerInfo winfo = { "https://legacy.cplusplus.com/reference/ios/", 80, { 2022, 11, 22 } };
	//以文本方式写
	ConfigManager cf_txt("test.txt");
	cf_txt.WriteText(winfo);
	//以文本方式读
	ConfigManager cf_text("test.txt");
	ServerInfo rtinfo;
	cf_text.ReadText(rtinfo);
	cout << rtinfo._address << " " << rtinfo._port << " " << rtinfo._d << endl;
	return 0;
}


