#pragma once
#include"HashTable.h"
namespace cpp
{
	template<class K, class HashFunc = DefaultHash<K>>
	class unordered_set
	{
		//仿函数
		struct SetKeyOfT
		{
			const K& operator()(const K& key)
			{
				return key;
			}
		};
	public:
		//现在没有实例化，没办法到HashTable里面找iterator，所以typename就是告诉编译器这里是一个类型，实例化以后再去取
		typedef typename Bucket::HashTable<K, K, SetKeyOfT, HashFunc>::iterator iterator;
		//begin()
		iterator begin()
		{
			return _ht.begin();
		}
		//end()
		iterator end()
		{
			return _ht.end();
		}
		//insert插入
		pair<iterator, bool> insert(const K& key)
		{
			return _ht.Insert(key);
		}
		//find查找
		iterator find(const K& key)
		{
			return _ht.Find(key);
		}
		//erase删除
		bool erase(const K& key)
		{
			return _ht.Erase(key);
		}
	private:
		Bucket::HashTable<K, K, SetKeyOfT, HashFunc> _ht;
	};
}