#define _CRT_SECURE_NO_WARNINGS 1
#include<string>
#include<bitset>
#include<iostream>
using namespace std;
//int main()
//{
//	bitset<16> bs1;
//	bitset<16> bs2(0xfa2);//0000111110100010
//	//bitset<16> bs3(0xfa5);
//	bitset<16> bs3(string("01101001"));//0000000001101001
//	bitset<16> bs4("01101001");//0000000001101001
//	cout << bs1 << endl;
//	cout << bs2 << endl;
//	cout << bs3 << endl;
//	cout << bs4 << endl;
//	return 0;
//}


//int main()
//{
//	bitset<16> bs;
//	bs.set(4);
//	bs.set(6);
//	bs.set(2);
//	cout << bs.size() << endl;//16
//	cout << bs << endl;//0000000001010100
//	//获取指定位的状态
//	cout << bs.test(0) << endl;//0
//	cout << bs.test(2) << endl;//1
//	//反转所有位
//	bs.flip();
//	cout << bs << endl;//1111111110101011
//	//反转第1位
//	bs.flip(1);
//	cout << bs << endl;//1111111110101001
//	cout << bs.count() << endl;//12
//	//清空第3位
//	bs.reset(3);
//	cout << bs << endl;//1111111110100001
//	//清空所有位
//	bs.reset();
//	cout << bs.none() << endl;//1
//	cout << bs.any() << endl;//0
//	//设置所有位
//	bs.set();
//	cout << bs.all() << endl;//1
//	return 0;
//}

int main()
{
	//>>输入、<<输出运算符
	bitset<8> bs;
	cin >> bs;//10100
	cout << bs << endl;//00010100
	//复合赋值运算符
	bitset<8> bs1("101011");
	bitset<8> bs2("100100");
	cout << (bs1 >>= 2) << endl;//00001010
	cout << (bs2 |= bs1) << endl;//00101110
	//位运算符
	bitset<8> bs3("10010");
	bitset<8> bs4("11001");
	cout << (bs3 & bs4) << endl;//00010000
	cout << (bs3 ^ bs4) << endl;//00001011
	//operator[]运算符
	cout << bs3[4] << endl;//1
	cout << bs3[2] << endl;//0
}