#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<Windows.h>
using namespace std;
namespace cpp
{
	// 服务器开发中通常使用的异常继承体系
	//基类
	class Exception
	{
	public:
		Exception(const string& errmsg, int id)
			:_errmsg(errmsg)
			, _id(id)
		{}
		virtual string what() const
		{
			return _errmsg;
		}
		int getid() const
		{
			return _id;
		}
	protected:
		string _errmsg;//描述错误信息
		int _id;//错误编码
	};
	//数据库组
	class SqlException : public Exception
	{
	public:
		SqlException(const string& errmsg, int id, const string& sql)
			:Exception(errmsg, id)
			, _sql(sql)
		{}
		virtual string what() const
		{
			string str = "SqlException:";
			str += _errmsg;
			str += "->";
			str += _sql;
			return str;
		}
	private:
		const string _sql;
	};
	//缓存组
	class CacheException : public Exception
	{
	public:
		CacheException(const string& errmsg, int id)
			:Exception(errmsg, id)
		{}
		virtual string what() const
		{
			string str = "CacheException:";
			str += _errmsg;
			return str;
		}
	};
	//协议组
	class HttpServerException : public Exception
	{
	public:
		HttpServerException(const string& errmsg, int id, const string& type)
			:Exception(errmsg, id)
			, _type(type)
		{}
		virtual string what() const
		{
			string str = "HttpServerException:";
			str += _type;
			str += ":";
			str += _errmsg;
			return str;
		}
	private:
		const string _type;
	};
	//抛数据库组的异常
	void SQLMgr()
	{
		if (rand() < RAND_MAX / 10)//10%的概率抛权限不足的异常
		{
			throw SqlException("权限不足", 100, "select * from name = '张三'");
		}
		else
		{
			cout << "Sql success" << endl;
		}
	}
	//抛缓存组的异常
	void CacheMgr()
	{
		if (rand() < RAND_MAX / 10)//%10的概率抛权限不足的异常
		{
			throw CacheException("权限不足", 100);
		}
		else if (rand() < RAND_MAX / 25)//%25的概率抛数据不存在的异常
		{
			throw CacheException("数据不存在", 101);
		}
		else
		{
			cout << "Cache success" << endl;
		}
		SQLMgr();
	}
	//抛协议组的异常
	void HttpServer()
	{
		if (rand() < RAND_MAX / 10)//%10的概率抛请求资源不存在的异常
		{
			throw HttpServerException("请求资源不存在", 404, "get");
		}
		else if (rand() < RAND_MAX / 10)//%10的概率抛权限不足的异常
		{
			throw HttpServerException("权限不足", 501, "post");
		}
		else
		{
			cout << "Http success" << endl;
		}
		CacheMgr();
	}
}
int main()
{
	srand(time(0));
	while (1)
	{
		::Sleep(1000);
		try 
		{
			cpp::HttpServer();
		}
		catch (const cpp::Exception& e) // 这里捕获父类对象就可以
		{
			// 多态
			cout << e.what() << endl;
		}
		catch (const std::exception& e) // 这里捕获库里的基类对象，捕获类似出现new失败等库里抛的异常
		{
			// 多态
			cout << e.what() << endl;
		}
		catch (...)
		{
			cout << "Unkown Exception" << endl;
		}
	}
	return 0;
}