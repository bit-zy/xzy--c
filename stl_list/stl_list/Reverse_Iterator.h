#pragma once
//#include<iostream>
//using namespace std;
namespace cpp
{
	template<class Iterator, class Ref, class Ptr>
	struct Reverse_iterator
	{
		typedef Reverse_iterator<Iterator, Ref, Ptr> Self;
		Iterator _it;
		//构造函数
		Reverse_iterator(Iterator it)
			:_it(it)
		{}
		//*运算符重载
		Ref operator*()
		{
			iterator tmp = it;
			//返回上一个数据
			return *(--tmp);
		}
		//->运算符重载
		Ptr operator*()
		{
			//复用operator*，返回上一个数据
			return &(operator*());
		}
		//++运算符重载
		Self& operator++() 
		{
			--_it;
			return *this;
		}
		//--运算符重载
		Self& operator--() 
		{
			++_it;
			return *this;
		}
		//!=运算符重载
		bool operator!=(const Self& s)
		{
			return _it != s._it;//返回两个结点指针的位置是否不同即可
		}
		//==运算符重载
		bool operator==(const Self& s)
		{
			return _it == s._it;//返回俩结点指针是否相同
		}
	};
}
