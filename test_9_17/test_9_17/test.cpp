#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;
//int foo(int n)
//{
//	if (n < 2)
//	{
//		return n;
//	}
//	return 2 * foo(n - 1) + foo(n - 2);
//}
//int main()
//{
//	cout << foo(5) << endl;
//	return 0;
//}

//int main()
//{
//	int a = 1;
//	if (a <= 0)
//	{
//		printf("*****\n");
//	}
//	else
//	{
//		printf("%%%%\n");
//	}
//}


//#define SQR(A) A*A
//int main()
//{
//	int x = 6, y = 3, z = 2;
//	x /= SQR(y + z) / SQR(y + z);
//	cout << x << endl;
//	return 0;
//}

//struct A
//{
//	long a1;
//	short a2;
//	int a3;
//	int* a4;
//};
//int main()
//{
//	cout << sizeof(A) << endl;
//}

//int f(int n)
//{
//	if (n == 1)
//	{
//		return 1;
//	}
//	else
//	{
//		return (f(n - 1) + n * n * n);
//	}
//}
//int main()
//{
//	cout << f(3) << endl;
//	return 0;
//}

#include<vector>
//class Solution {
//public:
//    int StrToInt(string str) {
//        vector<int> v;
//        int num = 0;
//        int ret = 0;
//        int i = 0;
//        if (str[0] == '+')
//        {
//            ret = 1;
//            i++;
//        }
//        if (str[0] == '-')
//        {
//            ret = -1;
//            i++;
//        }
//        while (i < str.size())
//        {
//
//            if (!isdigit(str[i]))
//            {
//                return 0;
//            }
//            v.push_back(str[i] - '0');
//            i++;
//        }
//        int j = 0;
//        for (int i = v.size() - 1; i >= 0; i--)
//        {
//            int p = pow(10, j);
//            j++;
//            v[i] *= p;
//            num += v[i];
//        }
//        num *= ret;
//        return num;
//    }
//};
#include<string>
class Solution {
public:
    int StrToInt(string str) {
        vector<int> v;
        int num = 0;
        int ret = 0;
        int i = 0;
        if (str[0] == '+')
        {
            ret = 1;
            i++;
        }
        if (str[0] == '-')
        {
            ret = -1;
            i++;
        }
        while (i < str.size())
        {
            if (isspace(str[i]))
                i++;
            if (!isdigit(str[i]) && !isspace(str[i]))//如果不是数字且不是空格，直接返回0
            {
                return 0;
            }
            v.push_back(str[i] - '0');
            i++;
        }
        int j = 0;
        for (int i = v.size() - 1; i >= 0; i--)
        {
            int p = pow(10, j);
            j++;
            v[i] *= p;
            num += v[i];
        }
        if (ret)
        {
            num *= ret;
        }
        return num;
    }
};
int main()
{
    string str;
    getline(cin, str);
    Solution s;
    int n = s.StrToInt(str);
    cout << n << endl;

    return 0;
}