#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<string>
using namespace std;
//父类
class Person 
{
public:
	//父类构造函数
	Person(const char* name)
		:_name(name)
	{}
	//虚函数
	virtual void BuyTicket() { cout << _name << "Person: 买票-全价 100￥" << endl; }
protected:
	string _name;
};
//子类
class Student : public Person
{
public:
	//子类构造函数
	Student(const char* name)
		:Person(name)//调用父类的构造函数
	{}
	//虚函数 + 函数名/参数/返回 均相同 --> 重写/覆盖
	virtual void BuyTicket() { cout << _name << "Student: 买票-半价 50￥" << endl; }
};
//子类
class Soldier : public Person
{
public:
	//子类构造函数
	Soldier(const char* name)
		:Person(name)//调用父类的构造函数
	{}
	//虚函数 + 函数名/参数/返回 均相同 --> 重写/覆盖
	virtual void BuyTicket() { cout << _name << "Soldier: 优先买票-88价 88￥" << endl; }
};
/*通过基类的指针调用
void Pay(Person* ptr)
{
	ptr->BuyTicket();
	delete ptr;
}
*/
//通过基类的引用调用
void Pay(Person& ptr)
{
	ptr.BuyTicket();
}
int main()
{
	int option = 0;
	cout << "==============================================" << endl;
	do
	{
		cout << "请选择身份：";
		cout << "1、普通人 2、学生 3、军人" << endl;
		cin >> option;
		cout << "请输入您的姓名：";
		string name;
		cin >> name;
		switch (option)
		{
		case 1:
		{
			
			//Pay(new Person(name.c_str()));指针的调用方法
			Person p(name.c_str());
			Pay(p);//引用的调用方法
			break;
		}
		case 2:
		{
			Student st(name.c_str());
			Pay(st);
			break;
		}
		case 3:
		{
			Soldier so(name.c_str());
			Pay(so);
			break;
		}
		default:
			cout << "输入错误，请重新输入" << endl;
			break;
		}
		cout << "==============================================" << endl;
	} while (option != -1);
	return 0;
}