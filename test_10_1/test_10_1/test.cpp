#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;
//class A
//{
//public:
//	A()
//		:miVal(0)
//	{
//		test();
//	}
//	virtual void func()
//	{
//		cout << miVal << ' ';
//	}
//	void test()
//	{
//		func();
//	}
//public:
//	int miVal;
//};
//class B : public A
//{
//public:
//	B()
//	{
//		test();
//	}
//	virtual void func()
//	{
//		++miVal;
//		cout << miVal << ' ';
//	}
//};
//int main()
//{
//	A* p = new B;
//	p->test();
//	return 0;
//}


class A
{
public:
	~A()
	{
		cout << "~A()";
	}
};
class B
{
public:
	virtual ~B()
	{
		cout << "~B()";
	}
};
class C : public A, public B
{
public:
	~C()
	{
		cout << "~C()";
	}
};
int main()
{
	C* c = new C;
	B* b1 = dynamic_cast<B *>(c);
	A* a2 = dynamic_cast<A *>(b1);
	delete a2;
	return 0;
}