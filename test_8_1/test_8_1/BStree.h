#pragma once
#include<iostream>
using namespace std;
template<class K>
//struct BinarySearchTreeNode
struct BSTreeNode
{
	BSTreeNode<K>* _left; //左指针
	BSTreeNode<K>* _right;//右指针
	K _key;//节点值
	BSTreeNode(const K& key)//构造函数
		:_left(nullptr)
		, _right(nullptr)
		, _key(key)
	{}
};

template<class K>
class BSTree
{
	typedef BSTreeNode<K> Node;
private:
	void DestoryTree(Node* root)
	{
		if (root == nullptr)
			return;
		//通过递归删除所有结点
		DestoryTree(root->_left);
		DestoryTree(root->_right);
		delete root;
	}
	Node* CopyTree(Node* root)
	{
		if (root == nullptr)
			return nullptr;
		Node* copyNode = new Node(root->_key);
		//递归创建拷贝一棵树
		copyNode->_left = CopyTree(root->_left);
		copyNode->_right = CopyTree(root->_right);
		return copyNode;
	}
public:
	//强制编译器自己生成构造函数，忽视拷贝构造带来的影响
	BSTree() = default;//C++11才支持

	//拷贝构造函数--深拷贝
	BSTree(const BSTree<K>& t)
	{
		_root = CopyTree(t._root);
	}

	//赋值运算符重载函数 t1 = t2
	BSTree<K>& operator=(BSTree<K> t)//t就是t2的拷贝
	{
		//现代写法
		swap(_root, t._root);
		return *this;
	}

	//析构函数
	~BSTree()
	{
		DestoryTree(_root);
		_root = nullptr;
	}
	/*非递归版的插入、删除、查找*/
		//insert插入
	bool Insert(const K& key)
	{
		if (_root == nullptr)//若一开始树为空
		{
			_root = new Node(key);//直接申请值为key的结点作为二叉搜索树的根结点
			return true;
		}
		Node* parent = nullptr;
		Node* cur = _root;
		//1、找寻插入的合适位置
		while (cur)
		{
			if (cur->_key < key)//若key大于当前结点值
			{
				parent = cur;
				cur = cur->_right;//让cur走向左子树
			}
			else if (cur->_key > key)//若key小于当前结点值
			{
				parent = cur;
				cur = cur->_left;//让cur走向右子树
			}
			else
			{
				return false;//若key等于当前结点值，说明插入的值不合法，返回false
			}
		}
		//2、进行与父亲的链接
		cur = new Node(key);
		if (parent->_key < key)
		{
			parent->_right = cur;//比父亲的值大连接在右子树
		}
		else
		{
			parent->_left = cur;//比父亲的值小链接在左子树
		}
		return true;
	}

	//Erase删除
	bool Erase(const K& key)
	{
		Node* parent = nullptr;
		Node* cur = _root;
		while (cur)
		{
			//1、先找到要删除的结点
			if (cur->_key < key)
			{
				parent = cur;
				//让parent始终为cur的父亲
				cur = cur->_right;
			}
			else if (cur->_key > key)
			{
				parent = cur;
				//让parent始终为cur的父亲
				cur = cur->_left;
			}
			else
			{
				//找到了，分两类情况讨论：
				//1、待删值只有一个孩子 -- 左为空 or 右为空
				//2、待删值两个孩子都在 -- 替换法删除
				if (cur->_left == nullptr)
				{
					if (cur == _root)
					{
						//如果左孩子为空且删除的值为根结点，直接更新根结点为右孩子
						_root = cur->_right;
					}
					else
					{
						//左孩子为空
						if (cur == parent->_left)
						{
							//如果父亲的左孩子为待删值，将父亲的左孩子指向待删值指向的右孩子
							parent->_left = cur->_right;
						}
						else
						{
							//如果父亲的左孩子不是待删值，将父亲的右孩子指向待删值指向的右孩子
							parent->_right = cur->_right;
						}
					}
					//删除待删的结点
					delete cur;
				}
				else if (cur->_right == nullptr)
				{
					if (cur == _root)
					{
						//如果右孩子为空且删除的值为根结点，直接更新根结点为左孩子
						_root = cur->_left;
					}
					else
					{
						//右孩子为空
						if (cur == parent->_left)
						{
							//如果父亲的左孩子为待删值，将父亲的左孩子指向待删值指向的左孩子
							parent->_left = cur->_left;
						}
						else
						{
							//如果父亲的左孩子不是待删值，将父亲的右孩子指向待删值指向的左孩子
							parent->_right = cur->_left;
						}
					}
					//删除待删的结点
					delete cur;
				}
				else
				{
					//待删值的两个孩子都在，替换法删除。
					//找右子树的最小值或找左子树的最大值，下面为找右子树最小值
					Node* minParent = cur;//右子树的根可能就是minRight，所以这里minParent不能为nullptr，
					//因为此时不会进入while循环导致minParent就一直为nullptr，最后删除的时候堆野指针的非法访问
					Node* minRight = cur->_right;
					while (minRight->_left)
					{
						minParent = minRight;
						//让minParent始终为minRight的父亲
						minRight = minRight->_left;
					}
					swap(minRight->_key, cur->_key);//或者cur->_key = minRight->_key;
					//链接父亲minParent和要删除的结点的孩子
					if (minParent->_left == minRight)
					{
						//如果minParent的左孩子为待删值，让minParent的左孩子指向minRight的右
						minParent->_left = minRight->_right;
					}
					else
					{
						//如果minParent的右孩子为待删值，让minParent的右孩子指向minRight的右
						minParent->_right = minRight->_right;
					}
					//删除要删的结点
					delete minRight;
				}
				return true;
			}
		}
		//遍历一遍找不到要删除的值，直接返回false
		return false;
	}


	//const Node* Find(const K& key)
	bool Find(const K& key)
	{
		Node* cur = _root;
		while (cur)
		{
			if (cur->_key < key)
			{
				cur = cur->_right;//若key值大于当前结点的值，则应该在该结点的右子树当中进行查找。
			}
			else if (cur->_key > key)
			{
				cur = cur->_left;//若key值小于当前结点的值，则应该在该结点的左子树当中进行查找。
			}
			else
			{
				return true;//若key值等于当前结点的值，则查找成功，返回true。
			}
		}
		return false;//遍历一圈没找到返回false
	}

	//中序遍历 -- 递归
	void InOrder()
	{
		_InOrder(_root);
		cout << endl;
	}

	///////////////////////////////////////递归版的插入、删除、查找/////////////////////////////////////////////
		//查找
	bool FindR(const K& key)
	{
		return _FindR(_root, key);
	}
	//插入
	bool InsertR(const K& key)
	{
		return _InsertR(_root, key);
	}
	//删除
	bool EraseR(const K& key)
	{
		return _EraseR(_root, key);
	}

private:
	//查找的子树
	bool _FindR(Node* root, const K& key)
	{
		if (root == nullptr)
			return false;
		if (root->_key < key)
		{
			//如果比key小，转换到右子树去找
			return _FindR(root->_right, key);
		}
		else if (root->_key > key)
		{
			//如果比key大，转换到左子树去找
			return _FindR(root->_left, key);
		}
		else
		{
			//找到了
			return true;
		}
	}
	//插入的子树
	bool _InsertR(Node*& root, const K& key)//Node*&为指针的引用
	{
		if (root == nullptr)
		{
			root = new Node(key);
			return true;
		}

		if (root->_key < key)
			return _InsertR(root->_right, key);//如果比key小，转换到右子树去插入
		else if (root->_key > key)
			return _InsertR(root->_left, key);//如果比key大，转换到左子树去插入
		else
			return false;//如果相等，就返回false
	}
	//删除的子树
	bool _EraseR(Node*& root, const K& key)
	{
		if (root == nullptr)
		{
			//如果是空就返回false
			return false;
		}

		if (root->_key < key)
		{
			return _EraseR(root->_right, key);//如果比key小，转换到右子树去插入
		}
		else if (root->_key > key)
		{
			return _EraseR(root->_left, key);//如果比key大，转换到左子树去插入
		}
		else
		{
			Node* del = root;//提前保存root结点的位置
			//开始删除
			if (root->_left == nullptr)
			{
				//如果左为空
				root = root->_right;
			}
			else if (root->_right == nullptr)
			{
				root = root->_left;
			}
			else
			{
				Node* minRight = root->_right;
				while (minRight->_left)
				{
					minRight = minRight->_left;
				}
				swap(root->_key, minRight->_key);
				return _EraseR(root->_right, key);
			}
			delete del;
			return true;
		}
	}

	//中序遍历的子树
	void _InOrder(Node* root)
	{
		if (root == nullptr)
			return;
		_InOrder(root->_left);//递归到左子树
		cout << root->_key << " ";//访问根结点
		_InOrder(root->_right);//递归到右子树
	}
private:
	Node* _root = nullptr;
};

