#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
#include<list>
#include<string>
#include<set>
#include<map>
using namespace std;
//struct Point
//{
//	int _x;
//	int _y;
//};
//int main()
//{
//	int array1[] = { 1, 2, 3, 4, 5 };
//	int array2[5] = { 0 };
//	Point p = { 1, 2 };
//	return 0;
//}

//struct Point
//{
//	int _x;
//	int _y;
//};
//int main()
//{
//	int x1 = 1;//建议就用这个
//	//{}对内置类型初始化，"=" 可加可不加
//	int x2 = { 2 };
//	int x3{ 2 };
//	int array1[]{ 1, 2, 3, 4, 5 };
//	int array2[5]{ 0 };
//	Point p{ 1, 2 };
//	// C++11中列表初始化也可以适用于new表达式中
//	int* p1 = new int(1);
//	int* p2 = new int[3]{ 1, 3, 4 };
//	return 0;
//}
//
//class Date
//{
//public:
//	Date(int year, int month, int day)
//		:_year(year)
//		, _month(month)
//		, _day(day)
//	{
//		cout << "Date(int year, int month, int day)" << endl;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//int main()
//{
//	Date d1(2022, 10, 7); // old style
//	// C++11支持的列表初始化，这里会调用构造函数初始化
//	Date d2 = { 2022, 10, 9 };
//	Date d3{ 2022, 10, 8 };
//
//	Date* p1 = new Date(1, 2, 3);
//	Date* p2 = new Date[3]{ { 2022, 1, 1 }, { 2022, 1, 2 }, { 2022, 1, 3 } };
//	return 0;
//}

//class Date
//{
//public:
//	Date(int year, int month, int day)
//		:_year(year)
//		, _month(month)
//		, _day(day)
//	{
//		cout << "Date(int year, int month, int day)" << endl;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//int main()
//{
//	//vector
//	vector<int> v1 = { 1, 2 ,3 ,4, 5 };
//	vector<int> v2{ 1, 2, 3, 4, 5 };
//	vector<Date> v3 = { { 2022, 1, 1 }, { 2022, 1, 2 }, { 2022, 1, 3 } };
//	//list
//	list<int> lt1{ 1, 2, 3 };
//	//set
//	set<int> s1{ 3, 4, 5, 6, 3 };
//	//map
//	map<string, string> dict = { {"string", "字符串" }, {"sort", "排序" } };
//	return 0;
//}


//int main()
//{
//	// the type of il is an initializer_list
//	auto il = { 10, 20, 30 };
//	cout << typeid(il).name() << endl;//class std::initializer_list<int>
//	initializer_list<double> ilt = { 3.3, 5.5, 9.9 };
//	initializer_list<double>::iterator it = ilt.begin();
//	while (it != ilt.end())
//	{
//		cout << *it << " ";//3.3 5.5 9.9
//		it++;
//	}
//	cout << endl;
//	for (auto e : ilt)
//	{
//		cout << e << " ";//3.3 5.5 9.9
//	}
//	return 0;
//}

//namespace cpp
//{
//	template<class T>
//	class vector {
//	public:
//		typedef T* iterator;
		//vector(initializer_list<T> l)
		//{
		//	_start = new T[l.size()];
		//	_finish = _start + l.size();
		//	_endofstorage = _start + l.size();
		//	//迭代器遍历
		//	/*
		//	iterator vit = _start;
		//	typename initializer_list<T>::iterator lit = l.begin();
		//	while (lit != l.end())
		//	{
		//		*vit++ = *lit++;
		//	}
		//	*/
		//	//范围for遍历
		//	for (auto e : l)
		//	{
		//		*vit++ = e;
		//	}
		//}
//		vector<T>& operator=(initializer_list<T> l) {
//			vector<T> tmp(l);
//			std::swap(_start, tmp._start);
//			std::swap(_finish, tmp._finish);
//			std::swap(_endofstorage, tmp._endofstorage);
//			return *this;
//		}
//	private:
//		iterator _start;
//		iterator _finish;
//		iterator _endofstorage;
//	};
//}

#include"vector.h"
void test_vector()
{
	cpp::vector<int> v1 = { 1, 2, 3, 4 };
	for (auto e : v1)
	{
		cout << e << " ";
	}
}
int main()
{
	test_vector();
	return 0;
}

