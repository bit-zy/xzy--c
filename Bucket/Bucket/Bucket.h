#pragma once
#include<iostream>
#include<string>
#include<vector>
using namespace std;

//利用仿函数将数据类型转换为整型
template<class K>
struct DefaultHash
{
	size_t operator()(const K& key)
	{
		return (size_t)key;
	}
};
//模板的特化
template<>
struct DefaultHash<string>
{
	size_t operator()(const string& key)
	{
		//BKDR哈希算法
		size_t hash = 0;
		for (auto ch : key)
		{
			hash = hash * 131 + ch;//把所有字符的ascii码值累计加起来
		}
		return hash;
	}
};

//开散列哈希桶的实现
namespace Bucket
{
	template<class K, class V>
	struct HashNode
	{
		pair<K, V> _kv;
		HashNode<K, V>* _next;
		//构造函数
		HashNode(const pair<K, V>& kv)
			:_kv(kv)
			, _next(nullptr)
		{}
	};
	template<class K, class V, class HashFunc = DefaultHash<K>>
	class HashTable
	{
		typedef HashNode<K, V> Node;
	public:
		//析构函数
		~HashTable()
		{
			for (size_t i = 0; i < _tables.size(); i++)
			{
				Node* cur = _tables[i];
				while (cur)
				{
					Node* next = cur->_next;
					delete cur;
					cur = next;
				}
				_tables[i] = nullptr;//释放后置空
			}
		}
		//插入
		bool Insert(const pair<K, V>& kv)
		{
			//1、去除冗余
			if (Find(kv.first))
			{
				return false;
			}
			//构建仿函数，把数据类型转为整型，便于后续建立映射关系
			HashFunc hf;
			//2、负载因子 == 1就扩容
			if (_tables.size() == _n)
			{
				/*法一
				size_t newSize = _tables.size() == 0 ? 10 : _tables.size() * 2;
				HashTable<K, V> newHT;//
				newHT._tables.resize(newSize, nullptr);
				//遍历旧表，把旧表的数据重新映射到新表
				for (size_t i = 0; i < _tables.size(); i++)
				{
					Node* cur = _tables[i];
					while (cur)//如果cur不为空，就插入
					{
						newHT.Insert(cur->_kv);
						cur = cur->_next;
					}
				}
				newHT._tables.swap(_tables);*/

				//法二：
				size_t newSize = _tables.size() == 0 ? 10 : _tables.size() * 2;
				vector<Node*> newTable;
				newTable.resize(newSize, nullptr);
				for (size_t i = 0; i < _tables.size(); i++)//遍历旧表
				{
					Node* cur = _tables[i];
					while (cur)
					{
						Node* next = cur->_next;
						size_t hashi = hf(cur->_kv.first) % newSize;//确认映射到新表的位置
						//头插
						cur->_next = newTable[hashi];
						newTable[hashi] = cur;
						cur = next;
					}
					_tables[i] = nullptr;
				}
				newTable.swap(_tables);
			}
			//3、头插
			//找到对应的映射位置
			size_t hashi = hf(kv.first);
			hashi %= _tables.size();
			//头插到对应的桶即可
			Node* newnode = new Node(kv);
			newnode->_next = _tables[hashi];
			_tables[hashi] = newnode;
			++_n;
			return true;
		}
		//查找
		Node* Find(const K& key)
		{
			//防止后续除0错误
			if (_tables.size() == 0)
			{
				return nullptr;
			}
			//构建仿函数，把数据类型转为整型，便于后续建立映射关系
			HashFunc hf;
			//找到对应的映射下标位置
			size_t hashi = hf(key);
			//size_t hashi = HashFunc()(key);//匿名对象
			hashi %= _tables.size();
			Node* cur = _tables[hashi];
			//在此位置的链表中进行遍历查找
			while (cur)
			{
				if (cur->_kv.first == key)
				{
					//找到了
					return cur;
				}
				cur = cur->_next;
			}
			//遍历结束，没有找到，返回nullptr
			return nullptr;
		}
		//删除
		/*法一*/
		bool Erase(const K& key)
		{
			//防止后续除0错误
			if (_tables.size() == 0)
			{
				return false;
			}
			//构建仿函数，把数据类型转为整型，便于后续建立映射关系
			HashFunc hf;
			//找到key所对应的哈希桶的位置
			size_t hashi = hf(key);
			hashi %= _tables.size();
			Node* cur = _tables[hashi];
			Node* prev = nullptr;
			//删除
			while (cur)
			{
				if (cur->_kv.first == key)
				{
					if (prev == nullptr)//头删
					{
						_tables[hashi] = cur->_next;
					}
					else//非头删
					{
						prev->_next = cur->_next;
					}
					delete cur;
					return true;
				}
				prev = cur;
				cur = cur->_next;
			}
			return false;
		}
		/*法二替换法……*/

	private:
		//指针数组
		vector<Node*> _tables;
		size_t _n = 0;//记录有效数据的个数
	};
}