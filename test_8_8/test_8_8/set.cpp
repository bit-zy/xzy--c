#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<set>
using namespace std;
void test_set1()
{
	set<int> s;
	s.insert(4);
	s.insert(5);
	s.insert(2);
	s.insert(1);
	s.insert(1);
	s.insert(3);
	s.insert(3);
	set<int>::iterator it = s.begin();
	while (it != s.end())
	{
		//有序+去重
		//*it = 10; err不允许修改
		cout << *it << " ";//1 2 3 4 5
		it++;
	}
	cout << endl;
	//范围for
	for (auto e : s)
	{
		cout << e << " ";
	}
}

void test_set2()
{
	set<int> s;
	s.insert(4);
	s.insert(5);
	s.insert(2);
	s.insert(1);
	s.insert(1);
	s.insert(3);
	s.insert(3);
	set<int>::iterator pos = s.find(20);//O(logN)
	if (pos != s.end())
	{
		cout << "找到了" << endl;
	}
	pos = find(s.begin(), s.end(), 2);//O(N)
	if (pos != s.end())
	{
		cout << "找到了" << endl;
	}
}
void test_set3()
{
	set<int> s;
	s.insert(4);
	s.insert(5);
	s.insert(2);
	s.insert(1);
	s.insert(3);
	cout << s.erase(3) << endl;//1
	cout << s.erase(30) << endl;//0
	for (auto e : s)
	{
		cout << e << " "; //1 2 4 5
	}
	cout << endl;
	set<int>::iterator pos = s.find(3);
	if (pos != s.end())
		s.erase(pos);
	for (auto e : s)
	{
		cout << e << " "; //1 2 4 5
	}

	/*int x;
	while (cin >> x)
	{
		set<int>::iterator pos = s.find(x);
		if (pos != s.end())
		{
			s.erase(pos);
			cout << "删除" << x << "成功" << endl;
		}
		else
		{
			cout << x << "不在set中" << endl;
		}
		for (auto e : s)
		{
			cout << e << " ";
		}
		cout << endl;
	}*/
	cout << endl;
	s.insert(3);
	if (s.find(5) != s.end())
	{
		cout << "5在" << endl;//5在
	}
	if (s.count(5))//相较于find更加方便
	{
		cout << "5在" << endl;//5在
	}
}


void test_set4()
{
	set<int> s;
	s.insert(4);
	s.insert(5);
	s.insert(2);
	s.insert(1);
	s.insert(3);
	s.insert(7);
	s.insert(9);
	for (auto e : s)
		cout << e << " ";//1 2 3 4 5 7 9
	cout << endl;
	//lower_bound返回 >= val的位置迭代器
	set<int>::iterator lowIt = s.lower_bound(3);
	cout << *lowIt << endl;//3
	lowIt = s.lower_bound(6);
	cout << *lowIt << endl;//7

	//要求删除 >= x 的所有值
	int x;
	cin >> x;//6
	lowIt = s.lower_bound(x);
	s.erase(lowIt, s.end());
	for (auto e : s)
		cout << e << " ";//1 2 3 4 5
}

void test_set5()
{
	set<int> s;
	s.insert(4);
	s.insert(5);
	s.insert(2);
	s.insert(1);
	s.insert(3);
	s.insert(7);
	s.insert(9);
	for (auto e : s)
		cout << e << " ";//1 2 3 4 5 7 9
	cout << endl;
	//upper_bound返回 > val的位置迭代器
	set<int>::iterator upIt = s.upper_bound(5);//存在
	cout << *upIt << endl;//7
	upIt = s.upper_bound(6);//不存在
	cout << *upIt << endl;//7
	//删除 x <= [] <= y的区间
	int x, y;
	cin >> x >> y;//x = 3, y = 7
	auto leftIt = s.lower_bound(x);
	auto rightIt = s.upper_bound(y);
	s.erase(leftIt, rightIt);
	for (auto e : s)
		cout << e << " ";//1 2 9
	cout << endl;
}

void test_set6()
{
	multiset<int> s;
	s.insert(4);
	s.insert(5);
	s.insert(2);
	s.insert(1);
	s.insert(1);
	s.insert(3);
	s.insert(3);
	set<int>::iterator it = s.begin();
	while (it != s.end())
	{
		//排序
		cout << *it << " ";//1 1 2 3 3 4 5
		it++;
	}
	cout << endl;
	//范围for
	for (auto e : s)
	{
		cout << e << " ";//1 1 2 3 3 4 5
	}
	cout << s.count(1) << endl;
	cout << s.erase(1) << endl; //返回1有多少个
}

void test_set7()
{
	multiset<int> multiset;
	multiset.insert(4);
	multiset.insert(5);
	multiset.insert(2);
	multiset.insert(1);
	multiset.insert(1);
	multiset.insert(3);
	multiset.insert(3);
	multiset.insert(3);
	//1 1 2 3 3 3 4 5
	set<int> set(multiset.begin(), multiset.end());
	cout << multiset.count(1) << endl;//2 返回1的个数
	cout << set.count(1) << endl;//1 返回一个bool值，存在返回1
	cout << multiset.erase(1) << endl;//2 返回删除的1的个数
	cout << set.erase(1) << endl;//1 返回一个bool值，存在删除的值返回1
	auto pos1 = multiset.find(3); //返回中序的第一个3的迭代器
	while (pos1 != multiset.end())
	{
		cout << *pos1 << " ";//3 3 3 4 5
		pos1++;
	}
}
//int main()
//{
//	set<int> s1;
//	set<int> s2(s1);
//	string s("hello world");
//	set<char> s3(s.begin(), s.end());
//	//test_set1();
//	//test_set2();
//	//test_set3();
//	//test_set4();
//	//test_set5();
//	//test_set6();
//	test_set7();
//}

