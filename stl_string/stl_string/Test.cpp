#define _CRT_SECURE_NO_WARNINGS 1
#include"String.h"
void test_string1()
{
	cpp::string s1("hello world");
	cout << s1.c_str() << endl;
	//修改字符
	s1[0] = 'x'; //s1.operator[](0) = 'x';
	cout << s1.c_str() << endl;
	//读字符
	for (size_t i = 0; i < s1.size(); i++)
	{
		cout << s1[i] << " ";
	}
	cout << endl;
}
void test_string2()
{
	cpp::string s1("hello world");
	cpp::string s2(s1);
	cout << s1.c_str() << endl;
	cout << s2.c_str() << endl;
	//修改字符
	s1[0] = 'x'; //s1.operator[](0) = 'x';
	cout << s1.c_str() << endl;
	cout << s2.c_str() << endl;
}
void test_string3()
{
	cpp::string s1("hello world");
	cpp::string s2(s1);
	cpp::string s3("11111111111");
	s1 = s3;
	cout << s1.c_str() << endl;
	cout << s2.c_str() << endl;
	cout << s3.c_str() << endl;
}
void test_string4()
{
	cpp::string s1("hello world");
	cout << s1.c_str() << endl;
	cpp::string s2;
	cout << s2.c_str() << endl;
}
void test_string5()
{
	cpp::string s1("hello world");
	s1.push_back('1');
	s1.push_back('1');
	cout << s1.c_str() << endl; //hello world11
	s1 += 'x';
	cout << s1.c_str() << endl; //hello world11x
	s1 += " CSDN";
	cout << s1.c_str() << endl;
}
void test_string6()
{
	cpp::string s3("hello world");
	s3.reserve(15);
	//s3.resize(20, 'x');
	s3.resize(14, 'x');
	s3.resize(5);
}
void func(const cpp::string& s)//引用传参，避免深拷贝，加上const防止改变
{
	for (size_t i = 0; i < s.size(); i++)
	{
		//s[i] += 1;
		cout << s[i] << " ";
	}
	cout << endl;
	for (auto ch : s)
	{
		cout << ch << " ";
	}
}
void test_string7()
{
	cpp::string s1("hello world");
	//迭代器
	cpp::string::iterator it = s1.begin();
	while (it != s1.end())
	{
		cout << *it << " "; //h e l l o   w o r l d
		it++;
	}
	cout << endl;
	//范围for
	for (auto& ch : s1) //加上引用，相当于是每个字符的别名，便于修改
	{
		ch -= 1;
	}
	for (auto& ch : s1)
	{
		cout << ch << " "; //g d k k n  v n q k c
	}
}

void test_string8()
{
	cpp::string s("hello world");
	s.insert(6, '@');
	s += '@';
	cout << s.c_str() << endl; //hello @world@
	for (auto& ch : s)
	{
		cout << ch << " "; 
	}
	cout << "#" << endl; //h e l l o   @ w o r l d @ #
	
 	s += '\0';
	for (auto& ch : s)
	{
		cout << ch << " "; 
	}
	cout << "#" << endl; //h e l l o   @ w o r l d @  #
	s.insert(0, '@');
	cout << s.c_str() << endl;//@hello @world@
}
void test_string9()
{
	cpp::string s("hello world");
	s.insert(0, "xxx");
	cout << s.c_str() << endl;//xxxhello world
	s.erase(0, 3);
	cout << s.c_str() << endl;//hellow world
}
void test_string10()
{
	cpp::string s("hello world");
	cout << s << endl;
	cout << s.c_str() << endl;
	s += '\0';
	s += '\0';
	s += '\0';
	s += 'x';
	cout << s << endl;
	cout << s.c_str() << endl;
	cpp::string s1, s2;
	cin >> s1;
	cout << s1 << endl;
	cin >> s2;
	cout << s2 << endl;
}
void test_string11()
{
	cpp::string s("hello world");
	cin >> s;
	cout << s << endl;
}
void test_string12()
{
	cpp::string s("hello world");
	cpp::getline(cin, s);
	cout << s << endl;
}

void test_string13()
{
	cpp::string s1("hello world");
	cpp::string s2(s1);
	cout << s2 << endl;
}
void test_string14()
{
	cpp::string s1("hello world");
	cpp::string s3("111111111111111");
	s1 = s3;
	cout << s1 << endl;
}
void test_string15()
{
	int i;
	cin >> i;
	string s = to_string(i);//整型转字符串
	cout << s << endl;
	int val = stoi(s);//字符串转整型
}

int main()
{
	try
	{
		//C++new失败要抛异常捕获
		//test_string1();
		//test_string2();
		//test_string3();
		//test_string4();
		//test_string5();
		//test_string6();
		//test_string7();
		//test_string8();
		//test_string9();
		//test_string10();
		//test_string11();
		//test_string12();
		//test_string13();
		//test_string14();
		test_string15();
	}
	catch (const exception& e)
	{
		cout << e.what() << endl;
	}
	return 0;
}