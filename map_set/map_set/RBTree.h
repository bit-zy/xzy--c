﻿#pragma once
#include<iostream>
#include<queue>
#include<vector>
#include<assert.h>
using namespace std;
enum Colour
{
	Red,
	Black
};
//节点类
template <class T>
struct RBTreeNode
{
	//三叉链结构
	RBTreeNode<T>* _left;
	RBTreeNode<T>* _right;
	RBTreeNode<T>* _parent;
	T _data;//数据
	//节点的颜色
	Colour _col;
	//构造函数
	RBTreeNode(const T& data)
		:_left(nullptr)
		, _right(nullptr)
		, _parent(nullptr)
		, _data(data)
		, _col(Red)
	{}
};

//迭代器的类
template<class T, class Ref, class Ptr>
struct __RBTreeIterator
{
	typedef RBTreeNode<T> Node;
	typedef __RBTreeIterator<T, Ref, Ptr> Self;//把迭代器typedef，简化后续使用
	Node* _node;
	//构造函数
	__RBTreeIterator(Node* node)
		:_node(node)
	{}
	//*运算符重载
	Ref operator*()
	{
		return _node->_data;//返回_data数据本身
	}
	//->运算符重载
	Ptr operator->()
	{
		return &_node->_data;//返回_data数据的地址
	}
	//前置++
	Self& operator++()
	{
		if (_node->_right == nullptr)//右子树为空
		{
			//找祖先里面，孩子是父亲左的那个
			Node* cur = _node;
			Node* parent = cur->_parent;
			while (parent && parent->_right == cur)
			{
				cur = cur->_parent;
				parent = parent->_parent;
			}
			_node = parent;
		}
		else//右子树不为空
		{
			//右子树的最左节点
			Node* subLeft = _node->_right;
			while (subLeft->_left)
			{
				subLeft = subLeft->_left;
			}
			_node = subLeft;
		}
		return *this;
	}
	//后置++
	Self& operator++(int)
	{
		Self tmp(*this);
		++(*this);//复用前置++
		return tmp;
	}
	//前置--
	Self& operator--()
	{
		if (_node->_left == nullptr)//左子树为空
		{
			//找孩子是父亲右的那个
			Node* cur = _node;
			Node* parent = cur->_parent;
			while (parent && parent->_left == cur)
			{
				cur = cur->_parent;
				parent = parent->_parent;
			}
			_node = parent;
		}
		else//左子树不为空
		{
			//找左子树的最右节点
			Node* subRight = _node->_left;
			while (subRight->_right)
			{
				subRight = subRight->_right;
			}
			_node = subRight;
		}
		return *this;
	}
	//后置--
	Self& operator--(int)
	{
		Self tmp(*this);
		--(*this);
		return tmp;
	}
	//!=
	bool operator!=(const Self& s)
	{
		return _node != s._node;
	}
	//==
	bool operator==(const Self& s)
	{
		return _node == s._node;
	}
};

// 红黑树的类
// T决定红黑树中存储的什么数据
// set RBTree<K, K>
// map RBTree<K, pair<K, V>>
template <class K, class T, class KeyOfT>
class RBTree
{
	typedef RBTreeNode<T> Node;//T决定节点存储类型的数据
public:
	typedef __RBTreeIterator<T, T&, T*> iterator;//普通迭代器
	typedef __RBTreeIterator<T, const T&, const T*> const_iterator;//const迭代器
	//begin()
	iterator Begin()//找最左节点
	{
		Node* subLeft = _root;
		while (subLeft && subLeft->_left)
		{
			subLeft = subLeft->_left;
		}
		return iterator(subLeft);//调用迭代器的构造函数
	}
	//end()
	iterator End()//返回最右节点的下一个
	{
		return iterator(nullptr);
	}
	//const版本
	//begin() 
	const_iterator Begin() const//找最左节点
	{
		Node* subLeft = _root;
		while (subLeft && subLeft->_left)
		{
			subLeft = subLeft->_left;
		}
		return const_iterator(subLeft);//调用迭代器的构造函数
	}
	//end()
	const_iterator End() const//返回最右节点的下一个
	{
		return const_iterator(nullptr);
	}
	pair<iterator, bool> Insert(const T& data)
	{
	//1、一开始为空树，直接new新节点
		if (_root == nullptr)
		{
			_root = new Node(data);
			_root->_col = Black;//新插入的节点处理成黑色
			return make_pair(iterator(_root), true);
		}
	//2、寻找插入的合适位置
		KeyOfT kot;
		Node* cur = _root;
		Node* parent = nullptr;
		while (cur)
		{
			if (kot(cur->_data) < kot(data))
			{
				parent = cur;
				cur = cur->_right;//插入的值 > 节点的值，更新到右子树查找
			}
			else if (kot(cur->_data) > kot(data))
			{
				parent = cur;
				cur = cur->_left;//插入的值 < 节点的值，更新到左子树查找
			}
			else
			{
				return make_pair(iterator(cur), false);//插入的值 = 节点的值，数据冗余插入失败，返回false
			}
		}
	//3、找到了插入的位置，进行父亲与插入节点的链接
		cur = new Node(data);
		Node* newnode = cur;
		cur->_col = Red;//插入的节点处理成红色
		if (kot(parent->_data) < kot(data))
		{
			parent->_right = cur;//插入的值 > 父亲的值，链接在父亲的右边
		}
		else
		{
			parent->_left = cur;//插入的值 < 父亲的值，链接在父亲的左边
		}
		cur->_parent = parent;//三叉链，要双向链接

	//4、检测新节点插入后，红黑树的性质是否造到破坏
		while (parent && parent->_col == Red)//存在连续的红色节点
		{
			Node* grandfather = parent->_parent;
			assert(grandfather);
			//先确保叔叔的位置
			if (grandfather->_left == parent)
			{
				Node* uncle = grandfather->_right;
				//情况一：cur为红，p为红，g为黑，u存在且为红
				if (uncle && uncle->_col == Red)
				{
					//变色
					parent->_col = uncle->_col = Black;
					grandfather->_col = Red;
					//继续往上处理
					cur = grandfather;
					parent = cur->_parent;
				}
				//情况二+情况三：叔叔不存在，或者叔叔存在且为黑
				else
				{
					if (cur == parent->_left)//p为g的左，cur为p的左，则进行右单旋 + p变黑，g变红
					{
						//		  g
						//     p
						// cur
						RotateR(grandfather);
						parent->_col = Black;
						grandfather->_col = Red;
					}
					else//p是g的左，cur是p的右，则进行左右双旋 + cur变黑， g变红
					{
						//		  g
						//	 p
						//	     cur
						RotateLR(grandfather); 
						cur->_col = Black;
						grandfather->_col = Red;
					}
					break;
				}
			}
			else//grandfather->_right == parent
			{
				Node* uncle = grandfather->_left;
				//情况一：cur为红，p为红，g为黑，u存在且为红
				if (uncle && uncle->_col == Red)
				{
					//变色
					parent->_col = uncle->_col = Black;
					grandfather->_col = Red;
					//继续往上处理
					cur = grandfather;
					parent = cur->_parent;
				}
				//情况二+情况三：叔叔不存在，或者叔叔存在且为黑
				else
				{
					if (cur == parent->_right)//p为g的右，cur为p的右，则进行左单旋 + p变黑，g变红
					{
						//	g
						//	   p
						//	     cur
						RotateL(grandfather);
						parent->_col = Black;
						grandfather->_col = Red;
					}
					else//p是g的右，cur是p的左，则进行右左双旋 + cur变黑， g变红
					{
						//   g
						//	      p
						//	cur
						RotateRL(grandfather); 
						cur->_col = Black;
						grandfather->_col = Red;
					}
					break;
				}
			}
		}
		_root->_col = Black;//暴力处理把根变成黑色
		return make_pair(iterator(newnode), true);
	}
	//Find查找函数
	iterator Find(const K& key)
	{
		Node* cur = _root;
		KeyOfT kot;
		while (cur)
		{
			if (kot(cur->_data) < key)
			{
				cur = cur->_right;//查询的值 > 节点值，-》右子树
			}
			else if (kot(cur->_data) > key)
			{
				cur = cur->_left;//查询的值 < 节点值，-》左子树
			}
			else
			{
				return iterator(cur);
			}
		}
		return End();
	}
private:
	//1、左单旋
	void RotateL(Node* parent)
	{
		Node* subR = parent->_right;
		Node* subRL = subR->_left;
		Node* ppNode = parent->_parent;//提前保持parent的父亲
		//1、建立parent和subRL之间的关系
		parent->_right = subRL;
		if (subRL)//防止subRL为空
		{
			subRL->_parent = parent;
		}
		//2、建立subR和parent之间的关系
		subR->_left = parent;
		parent->_parent = subR;
		//3、建立ppNode和subR之间的关系
		if (parent == _root)
		{
			_root = subR;
			_root->_parent = nullptr;
		}
		else
		{
			if (parent == ppNode->_left)
			{
				ppNode->_left = subR;
			}
			else
			{
				ppNode->_right = subR;
			}
			subR->_parent = ppNode;//三叉链双向链接关系
		}
	}
	//2、右单旋
	void RotateR(Node* parent)
	{
		Node* subL = parent->_left;
		Node* subLR = subL->_right;
		Node* ppNode = parent->_parent;
		//1、建立parent和subLR之间的关系
		parent->_left = subLR;
		if (subLR)
		{
			subLR->_parent = parent;
		}
		//2、建立subL和parent之间的关系
		subL->_right = parent;
		parent->_parent = subL;
		//3、建立ppNode和subL的关系
		if (parent == _root)
		{
			_root = subL;
			_root->_parent = nullptr;
		}
		else
		{
			if (parent == ppNode->_left)
			{
				ppNode->_left = subL;
			}
			else
			{
				ppNode->_right = subL;
			}
			subL->_parent = ppNode;//三叉链双向关系
		}
	}
	//3、左右双旋
	void RotateLR(Node* parent)
	{
		RotateL(parent->_left);
		RotateR(parent);
	}
	//4、右左双旋
	void RotateRL(Node* parent)
	{
		RotateR(parent->_right);
		RotateL(parent);
	}
private:
	Node* _root = nullptr;
};