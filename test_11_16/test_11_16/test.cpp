#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;
//class StackOnly
//{
//public:
//	static StackOnly CreateObj()
//	{
//		return StackOnly();//传值返回 —— 拷贝构造
//	}
//	StackOnly(const StackOnly&) = delete;
//	void print()
//	{
//		cout << "Stack Only" << endl;
//	}
//private:
//	//构造函数私有
//	StackOnly()
//	{}
//};
//int main()
//{
//	StackOnly::CreateObj();
//	//StackOnly* h3 = new StackOnly(h1);不能使用new在堆区创建对象
//	return 0;
//}
//class StackOnly
//{
//public:
//	StackOnly()
//	{}
//private:
//	//C++98
//	void* operator new(size_t size);
//	void operator delete(void* p);
//	//C++11
//	//void* operator new(size_t size) = delete;
//	//void operator delete(void* p) = delete;
//};
//int main()
//{
//	StackOnly h1;
//	//StackOnly* h3 = new StackOnly(h1);不能使用new在堆区创建对象
//	return 0;
//}
//class StackOnly
//{
//private:
//	void* operator new(size_t size) = delete;
//	void operator delete(void* p) = delete;
//};
//StackOnly h1;//全局区
//int main()
//{
//	static StackOnly h2;//静态区
//	return 0;
//}

//class StackOnly
//{
//public:
//	static StackOnly CreateObj()
//	{
//		return StackOnly();
//	}
//	// 禁掉operator new可以把下面用new 调用拷贝构造申请对象给禁掉
//	void* operator new(size_t size) = delete;
//	StackOnly(const StackOnly&) = delete;
//	void operator delete(void* p) = delete;
//private:
//	StackOnly()
//		:_a(0)
//	{}
//private:
//	int _a;
//};
////StackOnly h1;
//int main()
//{
//	StackOnly obj = StackOnly::CreateObj();//栈区 -- 正确
//	//StackOnly* ptr = new StackOnly(obj);//调用new在堆区创建对象 -- 禁掉
//	//static StackOnly copy(obj);//调用拷贝构造在静态区创建对象 -- 禁掉
//	return 0;
//}


class NonInherit
{
public:
	static NonInherit GetInstance()
	{
		return NonInherit();
	}
private:
	NonInherit()
	{}
};
class B : public NonInherit
{

};
int main()
{
	B b;
	return 0;
}