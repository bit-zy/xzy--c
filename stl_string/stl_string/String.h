#pragma once
#include<iostream>
#include<string>
using namespace std;
#include<assert.h>
//namespace cpp
//{
//	class string
//	{
//	public:
//		//构造函数
//		string(const char* str)
//			:_str(new char[strlen(str) + 1])
//		{
//			strcpy(_str, str);
//		}
//
//		//拷贝构造函数
//		//不能用浅拷贝，原因如下：1、析构两次 2、一个对象修改会影响另外一个
//		//传统写法
//		//s2(s1);
//		string(const string& s)
//			:_str(new char[strlen(s._str) + 1])
//		{
//			strcpy(_str, s._str);
//		}
//
//		//赋值运算符重载 --> 深拷贝
//		//s1 = s3  s1.operator=(&s1, s3);
//		string& operator=(const string& s)
//		{
//			//防止自己给自己赋值
//			if (this != &s)
//			{
//			/*	
//				//法一：
//				//先删除原先s1的所有空间，防止赋值后s1过大导致空间浪费，s1过小导致空间不够
//				delete[] _str;
//				//给s1开辟与s3等价的空间大小，要多开一字节给'\0'
//				_str = new char[strlen(s._str) + 1];
//				strcpy(_str, s._str);
//			*/
//				//法二优化
//				char* tmp = new char[strlen(s._str) + 1];
//				strcpy(tmp, s._str);
//				delete[] _str;
//				_str = tmp;
//			}
//			return *this;
//		}
//
//		//析构函数
//		~string()
//		{
//			if (_str)
//			{
//				delete[] _str;
//			}
//		}
//
//		//c_str  获取c形式的字符串
//		const char* c_str() const //最好加上const，便于普通及const类型均可调用
//		{
//			return _str;
//		}
//
//		//[]运算符重载
//		char& operator[](size_t pos)//引用返回，便于后续修改返回的字符
//		{
//			assert(pos < strlen(_str));
//			return _str[pos]; //返回pos位置字符的引用
//		}
//
//		//返回字符串的长度
//		size_t size()
//		{
//			return strlen(_str);
//		}
//
//	private:
//		char* _str;
//	};
//}

// 接下来是完善的string增删查改等操作：
namespace cpp
{
	class string
	{
	public:
		

	/*无参的默认构造函数，不推荐，推荐使用下面的全缺省构造函数
		string()
			:_size(0)
			, _capacity(0)
		{
			_str = new char[1];
			_str[0] = '\0';
		}
	*/
//全缺省构造函数
		string(const char* str = "")
			//按声明的顺序进行初始化
			:_size(strlen(str))
			, _capacity(_size)
		{
			_str = new char[_capacity + 1];
			strcpy(_str, str);
		}

//拷贝构造函数
		//不能用浅拷贝，原因如下：1、析构两次 2、一个对象修改会影响另外一个
	/*传统写法
		//s2(s1);
		string(const string& s)
			:_size(strlen(s._str))
			,_capacity(_size)
		{
			_str = new char[_capacity + 1];
			strcpy(_str, s._str);
		}
	*/
	/*现代写法*/
		string(const string& s)
			:_str(nullptr)
			, _size(0)
			, _capacity(0)
		{
			string tmp(s._str);
			swap(tmp);
		}

//赋值运算符重载 --> 深拷贝
		//s1 = s3  s1.operator=(&s1, s3);
	/*传统写法
		string& operator=(const string& s)
		{
			//防止自己给自己赋值
			if (this != &s)
			{
				//法一：
				//先删除原先s1的所有空间，防止赋值后s1过大导致空间浪费，s1过小导致空间不够
				//delete[] _str;
				//给s1开辟与s3等价的空间大小，要多开一字节给'\0'
				//_str = new char[strlen(s._str) + 1];
				//strcpy(_str, s._str);
			
				//法二优化
				char* tmp = new char[s._capacity + 1];
				strcpy(tmp, s._str);
				delete[] _str;
				_str = tmp;
				_size = s._size;
				_capacity = s._capacity;
			}
			return *this;
		}
	*/
	//现代写法s1 = s3;
		/*
		法一：
		string& operator=(const string& s)
		{
			if (this != &s)
			{
				string tmp(s._str);
				swap(tmp);
			}
			return *this;
		}
		*/
		//法二：简洁版
		string& operator=(string s)//传值传参调用拷贝构造，s就是s3的深拷贝结果
		{
			swap(s);//交换这俩对象
			return *this;
		}

//析构函数
		~string()
		{
			if (_str)
			{
				delete[] _str;
				_str = nullptr;
				_size = _capacity = 0;
			}
		}
		//swap交换函数
		void swap(string& s)
		{
			std::swap(_str, s._str);
			std::swap(_size, s._size);
			std::swap(_capacity, s._capacity);
		}

		//c_str  获取c形式的字符串
		const char* c_str() const //最好加上const，便于普通及const类型均可调用
		{
			return _str;
		}
//字符串访问相关函数
	//1、[]运算符重载
		//版本1：
		char& operator[](size_t pos) //引用返回，便于后续修改返回的字符
		{
			assert(pos < _size);
			return _str[pos]; //返回pos位置字符的引用
		}
		//版本2：
		const char& operator[](size_t pos) const//引用返回，便于后续修改返回的字符
		{
			assert(pos < _size);
			return _str[pos]; //返回pos位置字符的引用
		}
	//2、迭代器
		//版本1：
		typedef char* iterator;
		iterator begin()
		{
			return _str;//返回第一个有效字符的指针
		}
		iterator end()
		{
			return _str + _size;//返回最后一个字符后一个位置的地址，即'\0'的地址
		}
		//版本2：
		typedef const char* const_iterator;
		const_iterator begin() const
		{
			return _str;//返回第一个有效字符的指针
		}
		const_iterator end() const
		{
			return _str + _size;//返回最后一个字符后一个位置的地址，即'\0'的地址
		}


		//返回字符串的长度
		size_t size() const //不改变内部成员，最好加上const
		{
			return _size;
		}

		//返回字符串容量
		size_t capacity() const //不改变内部成员，最好加上const
		{
			return _capacity;
		}



//增加：
		//reserve扩容
		void reserve(size_t n)
		{
			if (n > _capacity)
			{
				char* tmp = new char[n + 1];//每次开空间一定要多给一个字节
				strcpy(tmp, _str);
				delete[] _str;
				_str = tmp;
				_capacity = n;
			}
		}
		//resize调整大小
		void resize(size_t n, char ch = '\0')
		{
			//如果n < _size，就保留前n个字符即可，把下标n置为'\0'
			if (n < _size)
			{
				_size = n;
				_str[_size] = '\0';
			}
			else
			{
				//如果n > _capacity，就要扩容了
				if (n > _capacity)
				{
					reserve(n);
				}
				for (size_t i = _size; i < n; i++)
				{
					//把剩余的字符置为ch
					_str[i] = ch;
				}
				_size = n;
				_str[_size] = '\0';
			}
		}

		//push_back尾插字符
		void push_back(char ch)
		{
		/*法一：
			//先检查是否需要扩容
			if (_size == _capacity)
			{
				//复用reserve进行扩容，如果一开始容量为0，记得处理，否则容量*2依旧为0
				reserve(_capacity == 0 ? 4 : _capacity * 2);
			}
			_str[_size] = ch;
			_size++;
			_str[_size] = '\0';
		*/
			//法二：复用insert尾插入字符
			insert(_size, ch);
		}

		//append
		void append(const char* str)
		{
		/*
			法一：
			//统计追加字符串后的长度
			size_t len = _size + strlen(str);
			//判断是否需要扩容
			if (len > _capacity)
			{
				reserve(len);
			}
			//把字符串追加到末尾
			strcpy(_str + _size, str);
			_size = len;
		*/
			//法二：复用insert函数
			insert(_size, str);
		}

		//operator+=字符
		string& operator+=(char ch)
		{
			//复用push_back
			push_back(ch);
			return *this;
		}
		//operator+=字符串
		string& operator+=(const char* str)
		{
			//复用append
			append(str);
			return *this;
		}

		//insert插入字符
		string& insert(size_t pos, char ch)
		{
			assert(pos <= _size);
			if (_size == _capacity)
			{
				//复用reserve进行扩容，如果一开始容量为0，记得处理，否则容量*2依旧为0
				reserve(_capacity == 0 ? 4 : _capacity * 2);
			}
			size_t end = _size + 1; //最好把end放到_size + 1的位置，防止后续出现整型提升等问题
			while (end > pos)
			{
				_str[end] = _str[end - 1];
				end--;
			}
			//当end挪动到pos的位置时停止挪动，并把ch赋到pos的下标处
			_str[pos] = ch;
			_size += 1;
			return *this;
		}
		//insert插入字符串
		string& insert(size_t pos, const char* str)
		{
			assert(pos <= _size);
			size_t len = strlen(str);
			if (len == 0)
			{
				//如果传进来的字符串为空，直接返回即可
				return *this;
			}
			if (_size + len > _capacity)
			{
				//判断是否扩容
				reserve(_size + len);
			}
			size_t end = _size + len;
			//当end >= pos + len时都不结束循环
			while (end >= pos + len)
			{
				_str[end] = _str[end - len];
				end--;
			}
			//不能使用strcpy，因为会把\0也拷过去，就会出错
			strncpy(_str + pos, str, len);
			_size += len;
			return *this;
		}

//删除
		//erase删除
		void erase(size_t pos, size_t len = npos)
		{
			assert(pos < _size);
			if (len == npos || pos + len >= _size)
			{
				//这种情况是删除pos后的所有数据，直接把pos处设定为'\0'即可
				_str[pos] = '\0';
				_size = pos;
			}
			else
			{
				size_t begin = pos + len;
				while (begin <= _size)
				{
					_str[begin - len] = _str[begin];
					++begin;
				}
				_size -= len;
			}
		}
		//clear清除数据
		void clear()
		{
			_str[0] = '\0';
			_size = 0;
		}
		
//查找
		//find查找字符
		size_t find(char ch, size_t pos = 0)
		{
			for (; pos < _size; pos++)
			{
				if (_str[pos] == ch)
					return pos;
			}
			//没找到就返回npos，-1
			return npos; //-1
		}
		//find查找字符串
		size_t find(const char* str, size_t pos = 0)
		{
			//直接复用C语言库函数strstr即可，strstr函数返回的是地址
			const char* p = strstr(_str + pos, str);
			if (p == nullptr)
			{
				return npos;
			}
			else
			{
				//返回下标直接用p - str即可
				return p - _str;
			}
		}
		

	private:
		char* _str;
		size_t _size;	  //有效字符个数
		size_t _capacity; //实际存储有效字符的空间
		const static size_t npos;
	};
	const size_t string::npos = -1;

	/****	****	****	****	****	****	**非成员函数**	****	****	****	****	****	****	****/



//关系运算符重载
	//1、operator<
	bool operator<(const string& s1, const string& s2)
	{
		return strcmp(s1.c_str(), s2.c_str()) < 0;
	}	
	//2、operator==
	bool operator==(const string& s1, const string& s2)
	{
		return strcmp(s1.c_str(), s2.c_str()) == 0;
	}
	//3、operator<=
	bool operator<=(const string& s1, const string& s2)
	{
		return s1 < s2 || s1 == s2;
	}
	//4、operator>
	bool operator>(const string& s1, const string& s2)
	{
		return !(s1 <= s2);
	}
	//5、operator>=
	bool operator>=(const string& s1, const string& s2)
	{
		return !(s1 < s2);
	}
	//6、operator!=
	bool operator!=(const string& s1, const string& s2)
	{
		return !(s1 == s2);
	}
//<<流插入运算符重载
	ostream& operator<<(ostream& out, const string& s)
	{
		for (auto ch : s)
		{
			out << ch;
		}
		return out;
	}
//>>流提取运算符重载
	istream& operator>>(istream& in, string& s)
	{
	/*
		法一：
		s.clear();
		char ch;
		ch = in.get();//使用get()函数才能获取空格或者换行字符
		while (ch != ' ' && ch != '\n')
		{
			s += ch;
			ch = in.get();
		}
		return in;
	*/
		//法二：
		//要先把原字符串的所有数据给清空才可以输入新的数据，否则会累加到原数据后面，出错
		s.clear();
		char ch;
		ch = in.get();//使用get()函数才能获取空格或者换行字符
		char buff[128] = { '\0' };
		size_t i = 0;
		while (ch != ' ' && ch != '\n')
		{
			buff[i++] = ch;
			if (i == 127)
			{
				s += buff;
				memset(buff, '\0', 128);
				i = 0;
			}
			ch = in.get();
		}
		s += buff;
		return in;
	}
//getline函数
	istream& getline(istream& in, string& s)
	{
		s.clear();
		char ch;
		ch = in.get();
		//getline函数只有在遇到换行符才会停止
		while (ch != '\n')
		{
			s += ch;
			ch = in.get();
		}
		return in;
	}
}