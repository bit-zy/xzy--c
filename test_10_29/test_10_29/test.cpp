#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;
//double Division(int a, int b)
//{
//	// 当b == 0时抛出异常
//	if (b == 0)
//		throw "Division by zero condition!";
//	else
//		return ((double)a / (double)b);
//}
//void Func()
//{
//	try
//	{
//		int len, time;
//		cin >> len >> time;
//		cout << Division(len, time) << endl;
//	}
//	catch (const char* errmsg)
//	{
//		cout << errmsg << endl;
//	}
//	int len, time;
//	cin >> len >> time;
//	cout << Division(len, time) << endl;
//}
//int main()
//{
//	try 
//	{
//		Func();
//	}
//	catch (const char* errmsg)
//	{
//		cout << errmsg << endl;
//	}
//	return 0;
//}


//void func1()
//{
//	throw string("这是一个异常");
//}
//void func2()
//{
//	func1();
//}
//void func3()
//{
//	func2();
//}
//int main()
//{
//	try
//	{
//		func3();
//	}
//	catch (const string& s)
//	{
//		cout << "错误描述：" << s << endl;
//	}
//	catch (...)
//	{
//		cout << "未知异常" << endl;
//	}
//	return 0;
//}

#include<ctime>
#include<string>
double Division(int a, int b)
{
	// 当b == 0时抛出异常
	if (b == 0)
		throw "Division by zero condition!";
	else
		return ((double)a / (double)b);
}
size_t x = 0;//记录抛异常的次数
void Probability()
{
	int val = rand();
	if (val < RAND_MAX / 4)//概率是%25
	{
		string str("25%概率抛异常->");
		str += to_string(val);
		x++;
		throw str;
	}
	else
	{
		cout << val << endl;
	}
}
void Func()
{
	try
	{
		Probability();
		int len, time;
		len = rand();
		time = rand() % 10;
		cout << Division(len, time) << endl;
	}
	catch (const string& s)
	{
		cout << s << endl;
	}
}
int main()
{
	try
	{
		srand(time(0));
		size_t N = 1000;
		for (size_t i = 0; i < N; i++)
		{
			Func();
		}
		cout << "抛异常的次数" << x << endl;
		cout << "抛异常的概率" << (double)x / N << endl;
	}
	catch (...)//捕获没有匹配的任意类型异常
	{
		cout << "未知异常" << endl;
	}
	return 0;
}