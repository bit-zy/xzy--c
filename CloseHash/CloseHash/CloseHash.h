#pragma once
#include<iostream>
#include<string>
#include<vector>
using namespace std;

//利用仿函数将数据类型转换为整型
template<class K>
struct DefaultHash
{
	size_t operator()(const K& key)
	{
		return (size_t)key;
	}
};
//模板的特化
template<>
struct DefaultHash<string>
{
	size_t operator()(const string& key)
	{
		//BKDR哈希算法
		size_t hash = 0;
		for (auto ch : key)
		{
			hash = hash * 131 + ch;//把所有字符的ascii码值累计加起来
		}
		return hash;
	}
};

//闭散列哈希表的模拟实现
namespace CloseHash
{
	enum State
	{
		EMPTY,
		EXIST,
		DELETE
	};
	//哈希节点状态的类
	template<class K, class V>
	struct HashData
	{
		pair<K, V> _kv;
		State _state = EMPTY;//记录每个位置的状态，默认给空
	};


	//哈希表的类
	template<class K, class V, class HashFunc = DefaultHash<K>>//添加仿函数便于把其他类型的数据转换为整型数据
	class HashTable
	{
		typedef HashData<K, V> Data;
	public:
		//插入
		bool Insert(const pair<K, V>& kv)
		{
			//1、去除冗余
			if (Find(kv.first))
			{
				//说明此值已经有了，直接返回false
				return false;
			}
			//2、扩容
				//负载因子超过0.7，就扩容
			if (_tables.size() == 0 || _n * 10 / _tables.size() >= 7)
			{
				size_t newSize = _tables.size() == 0 ? 10 : _tables.size() * 2;
				//扩容以后，需要重新建立映射关系
				HashTable<K, V, HashFunc> newHT;
				newHT._tables.resize(newSize);
				//遍历旧表，把旧表每个存在的元素插入newHT
				for (auto& e : _tables)
				{
					if (e._state == EXIST)
					{
						newHT.Insert(e._kv);
					}
				}
				newHT._tables.swap(_tables);//建立映射关系后交换
			}
			//3、插入
			HashFunc hf;
			size_t starti = hf(kv.first);//取出键值对的key，并且避免了负数的情况，借用仿函数确保是整型数据
			starti %= _tables.size();
			size_t hashi = starti;
			size_t i = 1;
			//线性探测/二次探测
			while (_tables[hashi]._state == EXIST)
			{
				hashi = starti + i;//二次探测改为 +i^2
				++i;
				hashi %= _tables.size();//防止hashi超出数组
			}
			_tables[hashi]._kv = kv;
			_tables[hashi]._state = EXIST;
			_n++;
			return true;
		}
		//查找
		Data* Find(const K& key)
		{
			//判断表的size是否为0
			if (_tables.size() == 0)
			{
				return nullptr;
			}
			HashFunc hf;
			size_t starti = hf(key);//通过仿函数把其它类型数据转为整型数据
			starti %= _tables.size();
			size_t hashi = starti;
			size_t i = 1;
			//线性探测/二次探测
			while (_tables[hashi]._state != EMPTY)//不为空就继续
			{
				if (_tables[hashi]._state != DELETE && _tables[hashi]._kv.first == key)
				{
					return &_tables[hashi];//找到了就返回此对象的地址
				}
				hashi = starti + i;//二次探测改为 +i^2
				++i;
				hashi %= _tables.size();//防止hashi超出数组
			}
			return nullptr;
		}
		//删除
		bool Erase(const K& key)
		{
			//复用Find函数去帮助我们查找删除的值是否存在
			Data* ret = Find(key);
			if (ret)
			{
				//若存在，直接把此位置的状态置为DELETE即可
				ret->_state = DELETE;
				return true;
			}
			else
			{
				return false;
			}
		}
	private:
		vector<Data> _tables;
		size_t _n = 0;//记录存放的有效数据的个数
	};
}