#pragma once
#include<iostream>
#include<assert.h>
#include"Reverse_Iterator.h"
using namespace std;

namespace cpp
{
/*1、模拟实现结点类*/
	template<class T>
	struct list_node//因为成员函数都是公有，就不设计成class了，直接struct
	{
		list_node<T>* _next;//后继指针
		list_node<T>* _prev;//前驱指针
		T _data;//记录数据
		//构造函数
		list_node(const T& val = T())//给一个缺省值T()
			:_next(nullptr)
			, _prev(nullptr)
			, _data(val)
		{}
	};

/*2、模拟实现迭代器类*/
	template<class T, class Ref, class Ptr>
	struct __list_iterator//因为成员函数都是公有，就不设计成class了，直接struct
	{
		typedef list_node<T> Node;
		typedef __list_iterator<T, Ref, Ptr> self;//对其进行typedef，方便后续使用
		Node* _node;
	//基本成员函数
		//1、构造函数
		__list_iterator(Node* node)//通过结点指针构造一个迭代器
			:_node(node)
		{}
		/*
		  2、析构函数 -- 结点不属于迭代器，不需要迭代器释放
		  3、拷贝构造 -- 默认浅拷贝即可
		  4、赋值重载 -- 默认浅拷贝即可
		*/
		
		//*运算符重载
		Ref operator*()//结点出了作用域还在，用引用返回
		{
			return _node->_data;//返回结点指向的数据
		}
		////*运算符重载
		//T& operator*()//结点出了作用域还在，用引用返回
		//{
		//	return _node->_data;//返回结点指向的数据
		//}
		//->运算符重载
		Ptr operator->()
		{
			return &(operator*());
		}
		////->运算符重载
		//T* operator->()
		//{
		//	//return &(operator*());
		//	return &_node->_data;//返回结点指针所指结点的数据的地址
		//}

		//前置++
		self& operator++()//迭代器++的返回值还是迭代器
		{
			_node = _node->_next;//直接让自己指向下一个结点即可实现++
			return *this;//返回自增后的结点指针
		}
		//后置++
		self operator++(int)//加参数以便于区分前置++
		{
			self tmp(*this);//拷贝构造tmp
			_node = _node->_next;//直接让自己指向下一个结点即可实现++
			return tmp;//主要返回tmp，才是后置++
		}
		//前置--
		self& operator--()
		{
			_node = _node->_prev;//让_node指向上一个结点即可实现--
			return *this;
		}
		//后置--
		self operator--(int)//记得传缺省值以区分前置--
		{
			self tmp(*this);//拷贝构造tmp
			_node = _node->_prev;
			return tmp;
		}

		//!=运算符重载
		bool operator!=(const self& it)
		{
			return _node != it._node;//返回两个结点指针的位置是否不同即可
		}
		//==运算符重载
		bool operator==(const self& it)
		{
			return _node == it._node;//返回俩结点指针是否相同
		}

	};

/*3、模拟实现list的功能类*/
	template<class T>
	class list
	{
		typedef list_node<T> Node;
	public:
		//正向迭代器
		typedef __list_iterator<T, T&, T*> iterator;//普通迭代器
		typedef __list_iterator<T, const T&, const T*> const_iterator;//const迭代器

		//反向迭代器适配支持
		typedef Reverse_iterator<iterator, T&, T*>reverse_iterator;
	//默认成员函数
		//1、构造函数
		list()
		{
			_head = new Node();//申请一个头结点
			_head->_next = _head;//头结点的下一个结点指向自己构成循环
			_head->_prev = _head;//头结点的上一个结点指向自己构成循环
		}
		//传迭代器区间构造
		template <class InputIterator>
		list(InputIterator first, InputIterator last)
		{
			empty_init();
			while (first != last)
			{
				push_back(*first);
				first++;
			}
		}
		//2、析构函数
		~list()
		{
			clear();
			delete _head;//删去哨兵位头结点
			_head = nullptr;
		}
		//3、拷贝构造函数 lt2(lt1)
		/*传统写法
		list(const list<T>& lt)
		{
			//先初始化lt2
			empty_init();
			//遍历lt1，把lt1的元素push_back到lt2里头
			for (auto e : lt)
			{
				push_back(e);//自动开辟新空间，完成深拷贝
			}
		}
		*/
		//空初始化  对头结点进行初始化
		void empty_init()
		{
			_head = new Node();
			_head->_next = _head;
			_head->_prev = _head;
		}
		//swap交换函数
		void swap(list<T>& lt)
		{
			std::swap(_head, lt._head);//交换头指针
		}

		/*现代写法*/
		list(const list<T>& lt)
		{
			//初始化自己
			empty_init();
			list<T>tmp(lt.begin(), lt.end());//借用迭代器区间去构造tmp
			//交换头结点指针即可完成深拷贝现代写法
			swap(tmp);
		}

		//4、赋值运算符重载（现代写法）
		list<T>& operator=(list<T> lt)//套用传值传参去拷贝构造完成深拷贝
		{
			swap(lt);
			return *this;
		}

		//clear清除
		void clear()
		{
			//复用erase
			iterator it = begin();
			while (it != end())
			{
				it = erase(it);
			}
		}
		
		//尾插
		void push_back(const T& x)
		{
			Node* tail = _head->_prev;//找尾
			Node* newnode = new Node(x);//创建一个新的结点
			//_head  tail  newnode
			//使tail和newnode构成循环
			tail->_next = newnode;
			newnode->_prev = tail;
			//使newnode和头结点_head构成循环
			newnode->_next = _head;
			_head->_prev = newnode;

			/*
			* 法二：复用insert
			* insert(end(), x);
			*/
		}
		//头插
		void push_front(const T& x)
		{
			insert(begin(), x);
		}
		//insert，插入pos位置之前
		iterator insert(iterator pos, const T& x)
		{
			Node* newnode = new Node(x);//创建新的结点
			Node* cur = pos._node; //迭代器pos处的结点指针
			Node* prev = cur->_prev;
			//prev newnode cur
			//链接prev和newnode
			prev->_next = newnode;
			newnode->_prev = prev;
			//链接newnode和cur
			newnode->_next = cur;
			cur->_prev = newnode;
			//返回新插入元素的迭代器位置
			return iterator(newnode);
		}
		//尾删
		void pop_back()
		{
			erase(--end());
		}
		//头删
		void pop_front()
		{
			erase(begin());
		}
		//erase
		iterator erase(iterator pos)
		{
			assert(pos != end());
			Node* cur = pos._node;
			Node* prev = cur->_prev;
			Node* next = cur->_next;
			//prev cur next
			//链接prev和next
			prev->_next = next;
			next->_prev = prev;
			//delete要删去的结点
			delete cur;
			//返回删除元素后一个元素的迭代器位置
			//return next;
			return iterator(next);
		}

	//迭代器相关函数
		//begin
		iterator begin()//begin返回的就是第一个有效数据，即头结点的下一个结点
		{
			return iterator(_head->_next);//构造了一个匿名对象，通过调用构造函数利用头结点指向的第一个结点作为参数，来返回头结点
			//return _head->_next;  也可以这样写
		}
		const_iterator begin() const
		{
			return const_iterator(_head->_next);
			//return _head->_next; 
		}
		//end
		iterator end()
		{
			return iterator(_head);//end返回的是最后一个结点的下一个结点，就是头结点_head
			//return _head;  也可以这样写
		}
		const_iterator end() const
		{
			return const_iterator(_head);
			//return _head;  也可以这样写
		}

		//反向迭代器
		reverse_iterator rbegin()
		{
			reverse_iterator(end());
		}
		reverse_iterator rend()
		{
			reverse_iterator(begin());
		}

	private:
		Node* _head;
	};
}