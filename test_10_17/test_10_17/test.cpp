#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;
//int main()
//{
//    int num = 0;
//    while (cin >> num)
//    {
//        int n = num;
//        int tmp = 0;
//        while (1)
//        {
//            tmp += n % 10;
//            n /= 10;
//            if (tmp >= 10)
//            {
//                n = tmp;
//                tmp = 0;
//            }
//            if (n == 0 && tmp < 10)
//            {
//                break;
//            }
//        }
//        cout << tmp << endl;
//    }
//    return 0;
//}

// write your code here cpp
//#include<iostream>
//#include<string>
//using namespace std;
//int main()
//{
//    string str;
//    while (cin >> str)
//    {
//        int num = 0;
//        for (int i = 0; i < str.size(); i++)
//        {
//            num += str[i] - '0';
//        }
//        int tmp = 0;
//        while (num)
//        {
//            tmp += num % 10;
//            num /= 10;
//            if (num == 0 && tmp / 10 != 0)
//            {
//                num = tmp;
//                tmp = 0;
//            }
//        }
//        cout << tmp << endl;
//    }
//    return 0;
//}

//#include<iostream>
//using namespace std;
//int fib(int n)
//{
//    if (n == 1)
//        return 1;
//    else if (n == 2)
//        return 2;
//    else
//        return fib(n - 1) + fib(n - 2) + 1;
//}
//int main()
//{
//    int number = 0;
//    cin >> number;
//    int sum = 0;
//    for (int i = 1; i < number; i++)
//    {
//        int ret = fib(i);
//        sum += ret;
//        //sum += 1;
//    }
//    cout << (sum + 1) << endl;
//    return 0;
//}

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
class Solution {
public:
    vector<int> largestValues(TreeNode* root) {
        queue<TreeNode*> que;
        vector<int> result;

        if (root)
            que.push(root);
        while (!que.empty())
        {
            int size = que.size();
            int maxval = INT_MIN;
            double ret = 1.1;
            for (int i = 0; i < size; i++)
            {
                TreeNode* node = que.front();
                que.pop();
                /*
                if (ret == 1.1)
                {
                    maxval = node->val;
                    ret = 2.0;
                }
                else
                {
                    maxval = maxval > node->val ? maxval : node->val;
                }*/
                maxval = max(maxval, node->val);
                if (node->right)
                    que.push(node->right);
                if (node->left)
                    que.push(node->left);
            }
            result.push_back(maxval);
        }
        return result;
    }
};