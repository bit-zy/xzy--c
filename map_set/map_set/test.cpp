#define _CRT_SECURE_NO_WARNINGS 1
#include<string>
#include"map.h"
#include"set.h"
void test_set1()
{
	cpp::set<int> s;
	s.insert(8);
	s.insert(6);
	s.insert(11);
	s.insert(5);
	s.insert(6);
	s.insert(7);
	s.insert(10);
	s.insert(13);
	s.insert(12);
	s.insert(15);
	cpp::set<int>::iterator it = s.begin();
	while (it != s.end())
	{
		cout << *it << " ";
		++it;
	}
	cout << endl;
	for (auto e : s)
	{
		cout << e << " ";
	}
	cout << endl;
}
void test_map1()
{
	cpp::map<string, int> m;
	m.insert(make_pair("111", 1));
	m.insert(make_pair("555", 5));
	m.insert(make_pair("333", 3));
	m.insert(make_pair("222", 2));
	cpp::map<string, int>::iterator it = m.begin();
	while (it != m.end())
	{
		cout << it->first << ":" << it->second << endl;
		it++;
	}
	cout << endl;
	for (auto& kv : m)
	{
		cout << kv.first << ":" << kv.second << endl;
	}
	cout << endl;
}
void test_map2()
{
	string arr[] = { "ƻ��", "����", "ƻ��", "����", "ƻ��", "ƻ��", "����", "ƻ��", "�㽶", "ƻ��", "�㽶" };
	cpp::map<string, int> countMap;
	for (auto& str : arr)
	{
		countMap[str]++;
	}

	for (const auto& kv : countMap)
		cout << kv.first << ":" << kv.second << endl;
}
void test_map3()
{
	cpp::map<string, string> dict;
	dict["insert"];
	dict["insert"] = "����";
	dict["left"] = "���";
}
int main()
{
	//test_set1();
	//test_map1();
	//test_map2();
	test_map3();
	return 0;
}

