#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;
//class Person
//{
//public:
//	void Print()
//	{
//		cout << "name:" << _name << endl;
//		cout << "age:" << _age << endl;
//	}
//private:
//	string _name = "peter"; // 姓名
//	int _age = 18; // 年龄
//};
//// 继承后父类的Person的成员（成员函数+成员变量）都会变成子类的一部分。这里体现出了 \
//Student和Teacher复用了Person的成员。下面我们使用监视窗口查看Student和Teacher对象，可 \
//以看到变量的复用。调用Print可以看到成员函数的复用。
//class Student : public Person
//{
//public:
//	void func()
//	{
//		Print();//基类公有Print在子类可以访问
//		//_age = 10; err 基类私有_age不能在子类访问
//	}
//protected:
//	int _stuid; // 学号
//};
//class Teacher : public Person
//{
//protected:
//	int _jobid; // 工号
//};
//int main()
//{
//	Student s;
//	Teacher t;
//	Person p;
//	//s._age = 0; err 基类私有_age不能在类外访问
//	/*s.Print();
//	t.Print();*/
//	return 0;
//}

////父类
//class Person
//{
//protected:
//	string _name; // 姓名
//	string _sex; // 性别
//	int _age; // 年龄
//};
////子类
//class Student : public Person
//{
//public:
//	int _No; // 学号
//};
//int main()
//{
//	//Student s;
//	//Person p = s;	  //派生类对象赋值给基类对象
//	//Person& rp = s;   //派生类对象赋值给基类引用
//	//Person* ptrp = &s;//派生类对象指针赋值给基类指针
//
//	Student s;
//	// 1.子类对象可以赋值给父类对象/指针/引用
//	Person p = s;
//	Person* rp = &s;
//	Person& ptrp = s;
//
//	//2.基类对象不能赋值给派生类对象
//	//s = p; err
//	
//	// 3.基类的指针可以通过强制类型转换赋值给派生类的指针
//	rp = &s;
//	Student* ps1 = (Student*)rp; // 这种情况转换时可以的。
//	ps1->_No = 10;
//	rp = &p;
//	Student* ps2 = (Student*)rp; // 这种情况转换时虽然可以，但是会存在越界访问的问题
//	ps2->_No = 10;
//}


//// Student的_num和Person的_num构成隐藏关系，可以看出这样代码虽然能跑，但是非常容易混淆
//class Person
//{
//protected:
//	string _name = "小李子"; // 姓名
//	int _num = 111; // 身份证号
//};
//class Student : public Person
//{
//public:
//	void Print()
//	{
//		cout << " 姓名:" << _name << endl;
//		cout << " 学号:" << _num << endl;
//		cout << " 身份证号:" << Person::_num << endl;
//	}
//protected:
//	int _num = 999; // 学号
//};
//int main()
//{
//	Student s1;
//	s1.Print();
//};


//// B中的fun和A中的fun不是构成重载，因为不是在同一作用域
//// B中的fun和A中的fun构成隐藏，成员函数满足函数名相同就构成隐藏。
//class A
//{
//public:
//	void fun()
//	{
//		cout << "A::fun()" << endl;
//	}
//};
//class B : public A
//{
//public:
//	void fun(int i)
//	{
//		cout << "B::fun(int i)->" << i << endl;
//	}
//};
//int main()
//{
//	B b;
//	b.fun(10);//屏蔽父类的，调用子类的 B::fun(int i)->10
//	b.A::fun();//指定调用的父类的   A::fun()
//};


//class Person
//{
//public:
//	Person(const char* name)
//		: _name(name)
//	{
//		cout << "Person()" << endl;
//	}
//	Person(const Person& p)
//		: _name(p._name)
//	{
//		cout << "Person(const Person& p)" << endl;
//	}
//	Person& operator=(const Person& p)
//	{
//		cout << "Person operator=(const Person& p)" << endl;
//		if (this != &p)
//			_name = p._name;
//		return *this;
//	}
//	~Person()
//	{
//		cout << "~Person()" << endl;
//	}
//protected:
//	string _name; // 姓名
//};
////子类
//class Student : public Person
//{
//	//子类构造函数原则：a、调用父类构造函数初始化继承父类的成员。b、自己再初始化自己的成员
//	//析构、拷贝构造、赋值重载也是类似
//public:
///*1、构造函数*/
//	Student(const char* name = "", int num = 10)
//		:Person(name)//调用基类的构造函数初始化基类的那一部分成员
//		, _num(num)//初始化派生类成员
//	{
//		cout << "Student(const char* name = "", int num = 10)" << endl;
//	}
///*2、拷贝构造函数*/
//	Student(const Student& s)
//		:Person(s)//调用基类的拷贝构造函数完成基类成员的拷贝构造
//		,_num(s._num)//拷贝构造派生类成员
//	{
//		cout << "Student(const Student& s)" << endl;
//	}
///*3、赋值运算符重载函数*/
//	Student& operator=(const Student& s)
//	{
//		if (this != &s)
//		{
//			//子类operator=和父类operator=构成隐藏，为了避免无线递归，要加上指定父类作用域
//			Person::operator=(s);//调用基类的operator=完成基类成员的赋值
//			_num = s._num;//完成派生类的赋值
//		}
//		cout << "Student& operator=(const Student& s)" << endl;
//		return *this;
//	}
///*4、析构函数*/
//	//父子类的析构函数构成隐藏关系 -- ps:后面多态的需要，析构函数名统一会被处理成destructor()，因此构成隐藏
//	//为了保证析构顺序（先子后父）。子类的析构函数完成后会自动调用父类的析构函数，所以不需要我们显示调用
//	~Student()
//	{
//		cout << "~Student()" << endl;
//	}//自动调用父类的析构函数
//protected:
//	int _num; //学号
//};
//int main()
//{
//	Student s1("李四", 1);
//	Student s2(s1);
//	Student s3("王五", 2);
//	s1 = s3;
//}



//class Student;
//class Person
//{
//public:
//	friend void Display(const Person& p, const Student& s);
//protected:
//	string _name; // 姓名
//};
//class Student : public Person
//{
//protected:
//	int _stuNum; // 学号
//};
//
//void Display(const Person& p, const Student& s)
//{
//	//Display是父类的友元，不是子类的友元
//	cout << p._name << endl;//可以访问父类
//	// cout << s._stuNum << endl; error 不能访问子类
//}
//int main()
//{
//	Person p;
//	Student s;
//	Display(p, s);
//}

class Person
{
public:
	Person() { ++_count; }
protected:
	string _name; // 姓名
public:
	static int _count; // 统计人的个数。
};
int Person::_count = 0;
class Student : public Person
{
protected:
	int _stuNum; // 学号
};
class Graduate : public Student
{
protected:
	string _seminarCourse; // 研究科目
};
int main()
{
	Student s1;
	Student s2;
	Student s3;
	Graduate s4;
	cout << " 人数 :" << Person::_count << endl;// 人数 :4
	cout << " 人数 :" << Student::_count << endl;// 人数 :4
	cout << " 人数 :" << s4._count << endl;// 人数 :4
	Student::_count = 0;
	cout << " 人数 :" << Person::_count << endl;// 人数 :0
}