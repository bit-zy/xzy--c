#pragma once
#include<iostream>
#include<vector>
using namespace std;
namespace cpp
{
	//N个比特位的位图
	template<size_t N>
	class bitset
	{
	public:
		//构造函数
		bitset()
		{
			//+1保证足够比特位，最多浪费8个比特位
			_bits.resize(N / 8 + 1, 0);
		}
		//把x映射的位标记成1
		void set(size_t x)
		{
			//x映射的比特位在第几个char对象
			size_t i = x / 8;
			//x在char第几个比特位
			size_t j = x % 8;
			//利用按位或|把第j位标记成1
			_bits[i] |= (1 << j);
		}
		//把x映射的位标记成0
		void reset(size_t x)
		{
			//x映射的比特位在第几个char对象
			size_t i = x / 8;
			//x在char第几个比特位
			size_t j = x % 8;
			//将1左移 j 位再整体反转后与第 i 个char进行与运算
			_bits[i] &= (~(1 << j));
		}
		//判断指定比特位x的状态是否为1
		bool test(size_t x)
		{
			//x映射的比特位在第几个char对象
			size_t i = x / 8;
			//x在char第几个比特位
			size_t j = x % 8;
			//将1左移 j 位后与第 i 个char类型进行与运算得出结果
			return _bits[i] & (1 << j);
		}
		//翻转指定位x
		void flip(size_t x)
		{
			//x映射的比特位在第几个char对象
			size_t i = x / 8;
			//x在char第几个比特位
			size_t j = x % 8;
			//将1左移 j 位后与第 i 个char进行按位异或运算^即可。
			_bits[i] ^= (1 << j);
		}
		//获取位图中可以容纳位N的个数
		size_t size()
		{
			return N;
		}
		
		//统计set中1的位数
		size_t count()
		{
			size_t count = 0;
			for (auto e : _bits)
			{
				int n = e;
				while (n)
				{
					int m = n - 1;
					n = n & m;
					count++;
				}
			}
			return count;
		}
		//判断所有比特位若无置为1，返回true
		bool none()
		{
			//遍历每个char
			for (auto e : _bits)
			{
				if (e != 0)//说明有位被置为1，返回false
					return false;
			}
			return true;//说明全为0，返回true
		}
		//判断位图中是否有位被置为1，若有则返回true
		bool any()
		{
			return !none();
		}
		//全部NUM个bit位被set返回true
		bool all()
		{
			size_t size = _bits.size();
			//先检查前N-1个char
			for (size_t i = 0; i < size - 1; i++)
			{
				if (~_bits[i] != 0)//取反应该为0，否则取反之前不全为1,返回false
					return false;
			}
			//再检查最后一个char的前 N%8 个位
			for (size_t j = 0; j < N % 8; j++)
			{
				if ((_bits[size - 1] & (1 << j)) == 0)//和test的原理一致
					return false;
			}
			return true;
		}
	private:
		vector<char> _bits;//位图
	};
}