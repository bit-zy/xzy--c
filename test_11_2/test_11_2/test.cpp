#define _CRT_SECURE_NO_WARNINGS 1
double Division(int a, int b)
{
	// 当b == 0时抛出异常
	if (b == 0)
	{
		throw "Division by zero condition!";
	}
	return (double)a / (double)b;
}
void Func()
{
	int* array = nullptr;
	array = new int[1024 * 1024 * 10];
	try
	{
		int len, time;
		//len = rand();
		//time = rand() % 5;//增大time = 0抛异常的概率
		cin >> len >> time;
		cout << Division(len, time) << endl;
	}
	catch (...)
	{
		cout << "1::delete []" << array << endl;
		delete[] array;
		throw;//捕获什么抛什么
	}
	cout << "2::delete []" << array << endl;
	delete[] array;
}
int main()
{
	srand(time(0));
	while (1)
	{
		try
		{
			Func();
		}
		catch (const char* errmsg)
		{
			cout << errmsg << endl;
		}
		catch (const exception& e)
		{
			cout << e.what() << endl;
		}
	}
	return 0;
}


// 这里可以看到如果发生除0错误抛出异常，另外下面的array没有得到释放。
// 所以这里捕获异常后并不处理异常，异常还是交给外面处理，这里捕获了再
// 重新抛出去。

