#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;
//class Base1 {
//public:
//	virtual void func1() { cout << "Base1::func1" << endl; }
//	virtual void func2() { cout << "Base1::func2" << endl; }
//private:
//	int b1;
//};
//class Base2 {
//public:
//	virtual void func1() { cout << "Base2::func1" << endl; }
//	virtual void func2() { cout << "Base2::func2" << endl; }
//private:
//	int b2;
//};
//class Derive : public Base1, public Base2 {
//public:
//	virtual void func1() { cout << "Derive::func1" << endl; }
//	virtual void func3() { cout << "Derive::func3" << endl; }
//private:
//	int d1;
//};
//typedef void(*VFPTR) ();
//void PrintVTable(VFPTR vTable[])
//{
//	cout << " 虚表地址>" << vTable << endl;
//	for (int i = 0; vTable[i] != nullptr; ++i)
//	{
//		printf(" 第%d个虚函数地址 :0X%x,->", i, vTable[i]);
//		VFPTR f = vTable[i];
//		f();
//	}
//	cout << endl;
//}
//int main()
//{
//	printf(" 0X%x\n", &Derive::func1);
//	Derive d;
//	PrintVTable((VFPTR*)(*(int*)&d));
//	PrintVTable((VFPTR*)(*(int*)((char*)&d + sizeof(Base1))));
//
//	//Base1* ptr1 = &d;
//	//Base2* ptr2 = &d;
//	//Derive* ptr3 = &d;
//	//cout << ptr1 << endl;//004FFA1C
//	//cout << ptr2 << endl;//004FFA24
//	//cout << ptr3 << endl;//004FFA1C
//}

//
//class A
//{
//public:
//	int _a;
//};
//// class B : public A
//class B : virtual public A
//{
//public:
//	int _b;
//};
//// class C : public A
//class C : virtual public A
//{
//public:
//	int _c;
//};
//class D : public B, public C
//{
//public:
//	int _d;
//};
//int main()
//{
//	D d;
//	d.B::_a = 1;
//	d.C::_a = 2;
//	d._b = 3;
//	d._c = 4;
//	d._d = 5;
//	return 0;
//}



//#include <iostream>
//using namespace std;
//class A
//{
//public:
//	A(char* s) { cout << s << endl; }
//	~A() {};
//};
//class B : virtual public A
//{
//public:
//	B(char* s1, char* s2)
//		:A(s1)
//	{
//		cout << s2 << endl;
//	}
//};
//class C : virtual public A
//{
//public:
//	C(char* s1, char* s2)
//		:A(s1)
//	{
//		cout << s2 << endl;
//	}
//};
//class D : public B, public C
//{
//public:
//	D(char* s1, char* s2, char* s3, char* s4)
//		:B(s1, s2)
//		, C(s1, s3)
//		, A(s1)
//	{
//		cout << s4 << endl;
//	}
//};

class A
{
public:
	A()
	{
		_a = 1;
	}
	virtual inline void f1()
	{
		cout << "A::f1()" << endl;
	}
	virtual void f2();
private:
	int _a;
};
class B : public A
{
	virtual inline void f1()
	{
		cout << "B::f1()" << endl;
	}
	virtual void f2()
	{
		cout << "B::f2()" << endl;
	}
};
void A::f2()
{
	cout << "A::f2()" << endl;
}

void Func(A* ptr)
{
	ptr->f1();
	ptr->f2();
}
int main()
{
	/*A aa;
	aa.f1();
	aa.f2();*/
	A aa;
	B bb;
	Func(&aa);
	Func(&bb);
	return 0;
}
