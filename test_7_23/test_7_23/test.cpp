#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include"Date.h"
using namespace std;
//#define N 100
//静态栈
//template<class T, size_t N = 20>
//class Stack
//{
//public:
//	void f()
//	{
//		N = 10;
//	}
//private:
//	T _a[N];
//	int _top;
//};
//int main()
//{
//	Stack<int, 100> st1;//定义一个大小为100的静态数组
//  st1.f();
//  Stack<double, 500> st2;//定义一个大小为500的静态数组
//	return 0;
//}

//template<class T>
//bool Less(T left, T right)
//{
//	return left < right;
//}

//// 函数模板 -- 参数匹配
//template<class T>
//bool Less(T left, T right)
//{
//	return left < right;
//}
//// 对Less函数模板进行特化
//template<>
//bool Less<Date*>(Date* left, Date* right)
//{
//	return *left < *right;
//}
//
//int main()
//{
//	cout << Less(1, 2) << endl; //1 可以比较，结果正确
//	Date d1(2022, 7, 7);
//	Date d2(2022, 7, 8);
//	cout << Less(d1, d2) << endl; //1 可以比较，结果正确
//
//	Date* p1 = new Date(2022, 7, 16);
//	Date* p2 = new Date(2022, 7, 15);
//	cout << Less(p1, p2) << endl; // 0/1 可以比较，结果错误
//}
#include<string>


template<class T1, class T2>
class Data
{
public:
	Data() { cout << "Data<T1, T2>" << endl; }
};


//全特化：
template<>
class Data<int, double>
{
public:
	Data() { cout << "Data<int, double>" << endl; }
};

//半特化/偏特化
template <class T1>
class Data<T1, char>// 将第二个参数特化为char
{
public:
	Data() { cout << "Data<T1, char>" << endl; }
};

//两个参数偏特化为指针类型
template <class T1, class T2>
class Data <T1*, T2*>//只要你T1和T2是指针，就走这里
{
public:
	Data() { cout << "Data<T1*, T2*>" << endl; };
};
//两个参数偏特化为引用类型
template <class T1, class T2>
class Data <T1&, T2&>//只要你T1和T2是指针，就走这里
{
public:
	Data() { cout << "Data<T1&, T2&>" << endl; };
};

int main()
{
	Data<int, int> d1;//Data<T1, T2>
	Data<int, double> d2;//Data<int, double>
	//Data<char, double> d3;//Data<T1, T2>
	//Data<char, int> d4;
	//只要第二个参数为char都会匹配半特化（偏特化）
	Data<int, char> d3;//Data<T1, char>
	Data<char, char> d4;//Data<T1, char>
	Data<int*, int*> d5;//Data<T1*, T2*>
	Data<int*, string*> d6;//Data<T1*, T2*>
	Data<int*, int> d7;//Data<T1, T2>
	Data<int*, char> d8;//Data<T1, char>
	Data<int&, char&> d10;//Data<T1&, T2&>
	Data<int, char&> d11;//Data<T1, T2>
	//Data<int*, char&> d12;//Data<T1, T2>
	Data<char, int> d12;//Data<T1, T2>
}


#include<vector>
#include <algorithm>
template<class T>
struct Less
{
	bool operator()(const T& x, const T& y) const
	{
		return x < y;
	}
};
// 对Less类模板按照指针方式特化
//全特化
template<>
struct Less<Date*>
{
	bool operator()(Date* x, Date* y) const
	{
		return *x < *y;
	}
};
//偏特化
template<class T>
struct Less<T*>//只要你是指针，都走我
{
	bool operator()(T* x, T* y) const
	{
		return *x < *y;
	}
};
//int main()
//{
//	Date d1(2022, 7, 7);
//	Date d2(2022, 7, 6);
//	Date d3(2022, 7, 8);
//	vector<Date> v1;
//	v1.push_back(d1);
//	v1.push_back(d2);
//	v1.push_back(d3);
//	// 可以直接排序，结果是日期升序
//	sort(v1.begin(), v1.end(), Less<Date>());
//	vector<Date*> v2;
//	v2.push_back(&d1);
//	v2.push_back(&d2);
//	v2.push_back(&d3);
//	// 可以直接排序，结果错误日期还不是升序，而v2中放的地址是升序
//	// 此处需要在排序过程中，让sort比较v2中存放地址指向的日期对象
//	// 但是走Less模板，sort在排序时实际比较的是v2中指针的地址，因此无法达到预期
//	sort(v2.begin(), v2.end(), Less<Date*>());
//
//	vector<int*> v3;
//	v3.push_back(new int(3));
//	v3.push_back(new int(1));
//	v3.push_back(new int(2));
//	sort(v3.begin(), v3.end(), Less<int*>());
//	return 0;
//}