#define _CRT_SECURE_NO_WARNINGS 1
//#include<iostream>
//using namespace std;
//int main()
//{
//	cout << "hello" << endl;
//	cerr << "hello" << endl;
//	clog << "hello" << endl;
//	return 0;
//}

//#include<iostream>//包含iostream文件
//using namespace std;//引入std标准命名空间
//int main()
//{
//	int a;
//	cin >> a;
//	cout << a << endl;
//	std::cin >> a;//使用时指定所属命名空间
//	std::cout << a << std::endl;//使用时指定所属命名空间
//	return 0;
//}

//#include<iostream>
//using namespace std;
//int main()
//{
//	int a, b, c;
//	cin >> a;//输入10 20 30
//	cout << a << endl;//10
//	cin >> b;//直接从缓冲区提取数据
//	cout << b << endl;//20
//	cin >> c;//直接从缓冲区提取数据
//	cout << c << endl;//30
//	return 0;
//}

//#include<iostream>
//using namespace std;
//int main()
//{
//	int a = 10;
//	cin >> a;
//	cout << a << endl;
//	return 0;
//}

//#include<iostream>
//#include<string>
//using namespace std;
//int main()
//{
//	string str;
//	getline(cin, str);//输入"hello world"
//	cout << str << endl;//输出"hello world"
//	return 0;
//}

//#include<iostream>
//using namespace std;
//class Date
//{
//	//友元函数
//	friend ostream& operator<<(ostream& out, const Date& d);//流插入 <<
//	friend istream& operator>>(istream& in, Date& d);//流提取 >>
//
//public:
//	Date(int year = 1, int month = 1, int day = 1)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
////流插入 <<
//ostream& operator<<(ostream& out, const Date& d)
//{
//	out << d._year << "-" << d._month << "-" << d._day << endl;
//	return out;
//}
////流提取 >>
//istream& operator>>(istream& in, Date& d)
//{
//	in >> d._year >> d._month >> d._day;
//	return in;
//}
//int main()
//{
//	Date d;
//	cin >> d;//2022 11 20
//	cout << d << endl;//2022-11-20
//}

//#include<iostream>
//using namespace std;
//int main()
//{
//	char buff[128];
//	while (scanf("%s", buff) != EOF)
//	{
//		//……
//		printf("%s\n", buff);
//	}
//	return 0;
//}

//#include<iostream>
//#include<string>
//using namespace std;
//int main()
//{
//	string str;
//	while (cin >> str)
//	{
//		cout << str << endl;
//	}
//	return 0;
//}


#include<iostream>
using namespace std;
class Date
{
	friend ostream& operator << (ostream& out, const Date& d);
	friend istream& operator >> (istream& in, Date& d);
public:
	Date(int year = 1, int month = 1, int day = 1)
		:_year(year)
		, _month(month)
		, _day(day)
	{}
	//支持Date类型转换为bool类型
	operator bool()
	{
		// 这里是随意写的，假设输入_year为0，则结束
		if (_year == 0)
			return false;
		else
			return true;
	}
	//支持Date类型转换为int类型
	operator int()
	{
		return _year + _month + _day;
	}
private:
	int _year;
	int _month;
	int _day;
};
istream& operator >> (istream& in, Date& d)
{
	in >> d._year >> d._month >> d._day;
	return in;
}
ostream& operator << (ostream& out, const Date& d)
{
	out << d._year << " " << d._month << " " << d._day;
	return out;
}
int main()
{
	//内置类型转自定义类型（隐式类型转换）
	Date d1 = 0;
	Date d2 = { 2022, 11, 20 };
	//自定义类型转换为内置类型(bool)
	bool ret1 = d1;
	bool ret2 = d2;
	cout << ret1 << endl;//0
	cout << ret2 << endl;//1
	if (d2)
	{
		cout << "operator ()" << endl;
	}
	//自定义类型转换为内置类型(int)
	int i = d2;
	cout << i << endl;//2053
	return 0;
}
