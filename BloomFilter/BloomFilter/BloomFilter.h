#pragma once
#include<iostream>
using namespace std;
#include<bitset>
#include<string>
namespace cpp
{
	struct BKDRHash
	{
		size_t operator()(const string& s)
		{
			size_t value = 0;
			for (auto ch : s)
			{
				value *= 31;
				value += ch;
			}
			return value;
		}
	};
	struct APHash
	{
		size_t operator()(const string& s)
		{
			size_t hash = 0;
			for (long i = 0; i < s.size(); i++)
			{
				if ((i & 1) == 0)
				{
					hash ^= ((hash << 7) ^ s[i] ^ (hash >> 3));
				}
				else
				{
					hash ^= (~((hash << 11) ^ s[i] ^ (hash >> 5)));
				}
			}
			return hash;
		}
	};
	struct DJBHash
	{
		size_t operator()(const string& s)
		{
			size_t hash = 5381;
			for (auto ch : s)
			{
				hash += (hash << 5) + ch;
			}
			return hash;
		}
	};

	template<size_t M,
		class K = string,
		class HashFunc1 = BKDRHash,
		class HashFunc2 = APHash,
		class HashFunc3 = DJBHash>
	class BloomFilter
	{
	public:
		//set插入
		void Set(const K& key)
		{
			//把插入的元素通过哈希函数获取对应的三个映射位置
			size_t hash1 = HashFunc1()(key) % M;
			size_t hash2 = HashFunc2()(key) % M;
			size_t hash3 = HashFunc3()(key) % M;
 			//把映射的三个位置设置为1
			_bs.set(hash1);
			_bs.set(hash2);
			_bs.set(hash3);
		}
		//test查找
		bool Test(const K& key)
		{
			size_t hash1 = HashFunc1()(key) % M;
			//依次判断key通过哈希函数映射的三个位置是否都被设置
			if (_bs.test(hash1) == false)
			{
				return false;//key必定不在
			}
			size_t hash2 = HashFunc2()(key) % M;
			if (_bs.test(hash2) == false)
			{
				return false;//key必定不在
			}
			size_t hash3 = HashFunc3()(key) % M;
			if (_bs.test(hash3) == false)
			{
				return false;//key必定不在
			}
			//三个位置都在，才能返回true，但是会存在误判
			return true;
		}
	private:
		bitset<M> _bs;
	};
}