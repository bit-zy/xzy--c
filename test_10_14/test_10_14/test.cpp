#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
#include<queue>
using namespace std;
struct TreeNode {
    int val;
    TreeNode* left;
    TreeNode* right;
    TreeNode() : val(0), left(nullptr), right(nullptr) {}
    TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
    TreeNode(int x, TreeNode* left, TreeNode* right) : val(x), left(left), right(right) {}
};
 
class Solution {
public:
    vector<int> rightSideView(TreeNode* root) {
        queue<TreeNode*> que;
        vector<int> result;
        if (root)
            que.push(root);
        while (!que.empty())
        {
            TreeNode* node = que.front();
            que.pop();
            result.push_back(node->val);
            if (node->right)
                que.push(node->right);
        }
        return result;
    }
};
int main()
{
   // TreeNode s(1, 2);
    return 0;
}