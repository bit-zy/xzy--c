#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include"SmartPtr.h"
using namespace std;
//int div()
//{
//	int a, b;
//	cin >> a >> b;
//	if (b == 0)
//		throw invalid_argument("除0错误");
//	return a / b;
//}
//void Func()
//{
	//int* p1 = new int[10];
	//int* p2 = new int[10];
	//int* p3 = new int[10];
	//int* p4 = new int[10];
//	try
//	{
//		cout << div() << endl;
//	}
//	catch (...)
//	{
//		delete[] p1;
//		delete[] p2;
//		delete[] p3;
//		delete[] p4;
//		throw;
//	}
//	delete[] p1;
//	delete[] p2;
//	delete[] p3;
//	delete[] p4;
//}
//int main()
//{
//	try
//	{
//		Func();
//	}
//	catch (exception& e)
//	{
//		cout << e.what() << endl;
//	}
//	return 0;
//}
//double div()
//{
//	double a, b;
//	cin >> a >> b;
//	if (b == 0)
//		throw invalid_argument("除0错误");
//	return a / b;
//}
//void Func()
//{
//	cpp::SmartPtr<int> sp1(new int);
//	cpp::SmartPtr<int> sp2(new int);
//	cpp::SmartPtr<int> sp3(new int);
//	cpp::SmartPtr<int> sp4(new int);
//	cout << div() << endl;
//}
//int main()
//{
//	try
//	{
//		Func();
//	}
//	catch (exception& e)
//	{
//		cout << e.what() << endl;
//	}
//	return 0;
//}

//double div()
//{
//	double a, b;
//	cin >> a >> b;
//	if (b == 0)
//		throw invalid_argument("除0错误");
//	return a / b;
//}
//void Func()
//{
//	cpp::SmartPtr<int> sp1(new int);
//	cpp::SmartPtr<pair<string, int>> sp2(new pair<string, int>("sort", 1));
//	*sp1 = 0;
//	sp2->second = 10;
//	sp2->first = "ten";
//}
//int main()
//{
//	try
//	{
//		Func();
//	}
//	catch (exception& e)
//	{
//		cout << e.what() << endl;
//	}
//	return 0;
//}

//int main()
//{
//	cpp::SmartPtr<int> sp1(new int);
//	cpp::SmartPtr<int> sp2(sp1);//拷贝构造
//
//	cpp::SmartPtr<int> sp3(new int);
//	cpp::SmartPtr<int> sp4(new int);
//	sp3 = sp4;//拷贝赋值
//	return 0;
//}
//int main()
//{
//	cpp::auto_ptr<int> ap1(new int(1));
//	cpp::auto_ptr<int> ap2(ap1);
//	cpp::auto_ptr<int> ap3 = ap2;
//	return 0;
//}

//#include<memory>
//int main()
//{
//	std::unique_ptr<int> up1(new int);
//	std::unique_ptr<int> up2(up1);//err，不能拷贝
//	return 0;
//}

//int main()
//{
//	cpp::shared_ptr<int> sp1(new int);
//	cpp::shared_ptr<int> sp2(sp1);
//	cpp::shared_ptr<int> sp3(new int);
//	sp1 = sp3;
//	sp2 = sp3;
//	std::shared_ptr<int> sp3 = sp2;
//	return 0;
//}
//int main()
//{
//	cpp::shared_ptr<int> sp1(new int);
//	cpp::shared_ptr<int> sp2(sp1);
//	cpp::shared_ptr<int> sp3(new int);
//	sp1 = sp3;
//	sp2 = sp3;
//	return 0;
//}
//
//struct ListNode
//{
//	ListNode* _next = nullptr;
//	ListNode* _prev = nullptr;
//	int _val;
//	~ListNode()
//	{
//		cout << "~ListNode()" << endl;
//	}
//};
//int main()
//{
//	ListNode* p1 = new ListNode;
//	ListNode* p2 = new ListNode;
//	p1->_next = p2;
//	p2->_prev = p1;
//	delete p1;
//	delete p2;
//	return 0;
//}
//struct ListNode
//{
//	std::shared_ptr<ListNode> _next = nullptr;
//	std::shared_ptr<ListNode> _prev = nullptr;
//	int _val = 0;
//	~ListNode()
//	{
//		cout << "~ListNode()" << endl;
//	}
//};
//int main()
//{
//	std::shared_ptr<ListNode> p1(new ListNode);
//	std::shared_ptr<ListNode> p2(new ListNode);
//	p1->_next = p2;
//	//p2->_prev = p1;
//	return 0;
//}

//struct ListNode
//{
//	std::weak_ptr<ListNode> _next;
//	std::weak_ptr<ListNode> _prev;
//	int _val = 0;
//	~ListNode()
//	{
//		cout << "~ListNode()" << endl;
//	}
//};
//int main()
//{
//	std::shared_ptr<ListNode> p1(new ListNode);
//	std::shared_ptr<ListNode> p2(new ListNode);
//	cout << p1.use_count() << endl;
//	cout << p2.use_count() << endl;
//	p1->_next = p2;
//	p2->_prev = p1;
//	cout << p1.use_count() << endl;
//	cout << p2.use_count() << endl;
//	return 0;
//}
// struct ListNode
//{
//	 cpp::weak_ptr<ListNode> _next;
//	 cpp::weak_ptr<ListNode> _prev;
//	int _val = 0;
//	~ListNode()
//	{
//		cout << "~ListNode()" << endl;
//	}
//};
//int main()
//{
//	cpp::shared_ptr<ListNode> p1(new ListNode);
//	cpp::shared_ptr<ListNode> p2(new ListNode);
//	cout << p1.use_count() << endl;
//	cout << p2.use_count() << endl;
//	p1->_next = p2;
//	p2->_prev = p1;
//	cout << p1.use_count() << endl;
//	cout << p2.use_count() << endl;
//	return 0;
//}

class Date
{
public:
	~Date()
	{
		cout << "~Date()" << endl;
	}
private:
	int _year = 1;
	int _month = 1;
	int _day = 1;
};
////针对new[]的释放
//template<class T>
//struct DeleteArray
//{
//	void operator()(T* ptr)
//	{
//		cout << "delete[]" << ptr << endl;
//		delete[] ptr;
//	}
//};
////针对malloc的释放
//template<class T>
//struct Free
//{
//	void operator()(T* ptr)
//	{
//		cout << "free" << ptr << endl;
//		free(ptr);
//	}
//};
////针对fopen的释放
//struct Fclose
//{
//	void operator()(FILE* ptr)
//	{
//		cout << "fclose" << ptr << endl;
//		fclose(ptr);
//	}
////};
//int main()
//{
//	cpp::unique_ptr<Date> up1(new Date);//默认new的释放
//	cpp::unique_ptr<Date, cpp::DeleteArray<Date>> up2(new Date[10]);//针对new[]的释放
//	cpp::unique_ptr<Date, cpp::Free<Date>> up3((Date*)malloc(sizeof(Date) * 10));//针对malloc的释放
//	cpp::unique_ptr<FILE, cpp::Fclose> up4((FILE*)fopen("Test.cpp", "r"));//针对fopen的释放
//	return 0;
//}


//针对new[]的释放
template<class T>
struct DeleteArray
{
	void operator()(T* ptr)
	{
		cout << "delete[]" << ptr << endl;
		delete[] ptr;
	}
};
//针对malloc的释放
template<class T>
struct Free
{
	void operator()(T* ptr)
	{
		cout << "free" << ptr << endl;
		free(ptr);
	}
};
//针对fopen的释放
struct Fclose
{
	void operator()(FILE* ptr)
	{
		cout << "fclose" << ptr << endl;
		fclose(ptr);
	}
};
int main()
{
	//针对new的释放
	cpp::shared_ptr<Date> sp1(new Date);//默认new的释放
	//针对new[]的释放
	std::shared_ptr<Date> sp2(new Date[10], DeleteArray<Date>());//传仿函数的匿名对象释放
	std::shared_ptr<Date> sp3(new Date[10], [](Date* ptr) {delete[] ptr; });//传lambda表达式释放
	//针对malloc的释放
	std::shared_ptr<Date> sp4((Date*)malloc(sizeof(Date) * 10), Free<Date>());//传仿函数的匿名对象释放
	//针对fopen的释放
	std::shared_ptr<FILE> sp6((FILE*)fopen("Test.cpp", "r"), Fclose());//传仿函数的匿名对象释放
	std::shared_ptr<FILE> sp5((FILE*)fopen("Test.cpp", "r"), [](FILE* ptr) {
		cout << "fclose: " << ptr << endl;
		fclose(ptr); });////传lambda表达式释放
	return 0;
}