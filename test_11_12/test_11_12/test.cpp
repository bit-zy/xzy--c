#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;

//class myString
//{
//public:
//	void insert(size_t pos, char ch)
//	{
//		//扩容……
//		//挪动数据
//		int end = _size;
//		while (end >= pos)
//		{
//			//_str[end + 1] = _str[end];
//			cout << end << endl;
//			--end; 
//		}
//		//放入插入数据……
//	}
//private:
//	char* _str;
//	size_t _size;
//	size_t _capacity;
//};
//int main()
//{
//	myString().insert(0, 'a');
//	return 0;
//}

//int main()
//{
//	int i = 1;
//	//隐式类型转换 -- 相近类型（意义相近）
//	double d = i;
//	printf("%d, %.2f\n", i, d);
//	int* p = &i;
//	//显示的强制类型转换 -- 不相似类型
//	int address = (int)p;
//	printf("%x, %d\n", p, address);
//	return 0;
//}
//int main()
//{
//	double d = 12.34;
//	int a = d;
//	cout << a << endl;
//	cout << sizeof(a) << endl;
//	cout << sizeof(d) << endl;
//	return 0;
//}
//

//int main()
//{
//	int d = 12.34;
//	double a = static_cast<int>(d);
//	cout << a << endl;//12
//	return 0;
//}
//static_cast 相近类型之间的转换
//int main()
//{
//	double d = 12.34;
//	int a = static_cast<int>(d);
//	cout << a << endl;//12
//	/*int* p = &a;
//	int x = static_cast<int>(p);不是相近类型，不支持转换*/
//	return 0;
//}

//reinterpret_cast 不相近类型之间的转换
//int main()
//{
//	double d = 12.34;
//	int a = static_cast<int>(d);
//	cout << a << endl;
//	//int *p = static_cast<int*>(a);这里使用static_cast会报错，应该使用reinterpret_cast
//	int* p = reinterpret_cast<int*>(&a);
//	cout << *p << endl;
//	int* t = &a;
//	int t2 = reinterpret_cast<int>(t);
//	cout << t2 << endl;
//	return 0;
//}



//typedef void (*FUNC)();
//int DoSomething(int i)
//{
//	cout << "DoSomething" << endl;
//	return 0;
//}
//int main()
//{
//	FUNC f = reinterpret_cast<FUNC>(DoSomething);
//	f();//DoSomething
//	return 0;
//}



//int main()
//{
//	const int a = 2;
//	int* p = const_cast<int*>(&a);//取消变量a的const属性
//	*p = 3;
//	cout << a << endl; //2
//	cout << *p << endl;//3
//	return 0;
//}
//int main()
//{
//	volatile const int a = 2;
//	int* p = const_cast<int*>(&a);//取消变量a的const属性
//	*p = 3;
//	cout << a << endl; //3
//	cout << *p << endl;//3
//	return 0;
//}

//int main()
//{
//	volatile const int a = 2;
//	//int* p = const_cast<int*>(&a);//C++取消变量a的const属性
//	int* p = (int*)&a;//C语言强转
//	*p = 3;
//	cout << a << endl; //3
//	cout << *p << endl;//3
//	return 0;
//}

//class A
//{
//public:
//	virtual void f() {}
//};
//class B : public A
//{};
//void fun(A* pa)
//{
//	// dynamic_cast会先检查是否能转换成功，能成功则转换，不能则返回空
//	//B* ptr = dynamic_cast<B*>(pa);
//	B* ptr = (B*)pa;//不安全
//	//B* ptr = reinterpret_cast<B*>(pa);也可使用reinterpret_cast
//	if (ptr)
//	{
//		cout << "转换成功" << ptr << endl;
//	}
//	else
//	{
//		cout << "转换失败" << ptr << endl;
//	}
//}
//int main()
//{
//	A a;
//	B b;
//	fun(&a);
//	fun(&b);
//	return 0;
//}


class A
{
public:
	virtual void f()
	{}
};
class B : public A
{};
void func(A* pa)
{
	B* pb1 = (B*)pa;               //不安全
	B* pb2 = dynamic_cast<B*>(pa); //安全

	cout << "pb1: " << pb1 << endl;
	cout << "pb2: " << pb2 << endl;
}
int main()
{
	A a;
	B b;
	func(&a);
	func(&b);
	return 0;
}
