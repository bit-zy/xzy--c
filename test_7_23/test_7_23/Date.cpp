#define _CRT_SECURE_NO_WARNINGS 1
#include"Date.h"
//获取某月天数
int Date::GetMonthDay(int year, int month)
{
	assert(year >= 0 && month > 0 && month < 13);
	static int monthDayArray[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
	if (month == 2 && isLeapYear(year))
		return 29;
	else
		return monthDayArray[month];
}
//构造函数
Date::Date(int year, int month, int day)
{
	if (year >= 1 && month <= 12 && month >= 1 && day <= GetMonthDay(year, month))
	{
		_year = year;
		_month = month;
		_day = day;
	}
	else
		cout << "日期非法" << endl;
}
// <运算符重载
bool Date::operator<(const Date& d) const //类外访问成员函数需要设定类域
{
	if (_year < d._year ||
		_year == d._year && _month < d._month ||
		_year == d._year && _month == d._month && _day < d._day)
		return true;
	else
		return false;
}
// ==运算符重载
bool Date::operator==(const Date& d) const
{
	return _year == d._year &&
		_month == d._month &&
		_day == d._day;
}

//日期 + 天数   d1 + 100  -->  d1.operator+(day)
Date Date::operator+(int day) const
{
	//法一：
	/*
	Date ret(*this); //拷贝构造，拿d1去初始化ret
	ret._day += day;
	while (ret._day > GetMonthDay(ret._year, ret._month))
	{
		ret._day -= GetMonthDay(ret._year, ret._month);
		ret._month++;
		if (ret._month == 13)
		{
			ret._year++;
			ret._month = 1;
		}
	}
	return ret; //出了作用域，对象ret不在，它是一个局部对象，我们这里不能用引用，用了的话，返回的就是ret的别名，但是ret又已经销毁了，访问野指针了
	*/
	//法二：复用日期 += 天数
	Date ret(*this);
	ret += day;
	return ret;
}

//日期 += 天数
Date& Date::operator+=(int day)
{
	//法一：
	//如果day小于0，要单独处理
	if (day < 0)
	{
		return *this -= -day;
	}
	_day += day;
	while (_day > GetMonthDay(_year, _month))
	{
		_day -= GetMonthDay(_year, _month);
		_month++;
		if (_month == 13)
		{
			_year++;
			_month = 1;
		}
	}
	return *this;


	//法二：复用
	//* this = *this + day; //让d1+过天数后再返回给自己从而实现+=
	//return *this;
}

//日期 -=天数  d1-=100
Date& Date::operator-=(int day)
{
	//如果减去的天数是负数，要单独处理，直接调用+=
	if (day < 0)
	{
		return *this += -day;
	}
	_day -= day;
	while (_day <= 0)
	{
		--_month;
		if (_month == 0)
		{
			_month = 12;
			--_year;
		}
		_day += GetMonthDay(_year, _month);
	}
	return *this;
}

//日期 - 天数
Date Date::operator-(int day) const
{
	Date ret(*this);
	ret -= day;
	return ret;
}

//前置++
Date& Date::operator++() //无参的为前置
{
	*this += 1; //直接复用+=
	return *this;
}

//后置++
Date Date::operator++(int i) //有参数的为后置
{
	Date tmp(*this);
	*this += 1;
	return tmp;
}

//前置--
Date& Date::operator--() //无参的为前置
{
	*this -= 1; //直接复用+=
	return *this;
}

//后置--
Date Date::operator--(int i) //有参数的为后置
{
	Date tmp(*this);
	*this -= 1;
	return tmp;
}

//日期 - 日期
int Date::operator-(const Date& d) const
{
	int flag = 1;
	Date max = *this;
	Date min = d;
	if (*this < d)
	{
		min = *this;
		max = d;
		flag = -1;
	} //确保max是大的，min是小的
	int n = 0;
	while (min != max)
	{
		min++;
		n++;
	}//算出min和max之间绝对值差距
	return n * flag; //如果d1大，结果为正，d2大结果为负
}