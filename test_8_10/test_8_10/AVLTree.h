#pragma once
#include<iostream>
using namespace std;
//节点类
template<class K, class V>
struct AVLTreeNode
{
	//存储的键值对
	pair<K, V> _kv;
	//三叉连结构
	AVLTreeNode<K, V>* _left;//左孩子
	AVLTreeNode<K, V>* _right;//右孩子
	AVLTreeNode<K, V>* _parent;//父亲
	//平衡因子_bf
	int _bf;//右子树 - 左子树的高度差
	//构造函数
	AVLTreeNode(const pair<K, V>& kv)
		:_kv(kv)
		, _left(nullptr)
		, _right(nullptr)
		, _parent(nullptr)
		, _bf(0)
	{}
};
//AVL树的类
template<class K, class V>
class AVLTree
{
	typedef AVLTreeNode<K, V> Node;
public:
	//1、按照搜索树的规则插入
	//2、是否违反平衡规则，如果违反就需要处理：旋转
	bool Insert(const pair<K, V>& kv)
	{	
		//1、一开始为空树，直接new新节点
		if (_root == nullptr)
		{
			//如果_root一开始为空树，直接new一个kv的节点，更新_root和_bf
			_root = new Node(kv);
			_root->_bf = 0;
			return true;
		}
		//2、寻找插入的合适位置
		Node* cur = _root;//记录插入的位置
		Node* parent = nullptr;//保存parent为cur的父亲
		while (cur)
		{
			if (cur->_kv.first < kv.first)
			{
				//插入的值 > 节点的值
				parent = cur;
				cur = cur->_right;//更新到右子树查找
			}
			else if (cur->_kv.first > kv.first)
			{
				//插入的值 < 节点的值
				parent = cur;
				cur = cur->_left;//更新到左子树查找
			}
			else
			{
				//插入的值 = 节点的值，数据冗余插入失败，返回false
				return false;
			}
		}
		//3、找到了插入的位置，进行父亲与插入节点的链接
		cur = new Node(kv);
		if (parent->_kv.first < kv.first)
		{
			//插入的值 > 父亲的值，链接在父亲的右边
			parent->_right = cur;
		}
		else
		{
			//插入的值 < 父亲的值，链接在父亲的左边
			parent->_left = cur;
		}
		//因为是三叉连，插入后记得双向链接（孩子链接父亲）
		cur->_parent = parent;
		//4、更新新插入节点的祖先的平衡因子
		while (parent)//最远要更新到根
		{
			if (cur == parent->_right)
			{
				parent->_bf++;//新增结点在parent的右边，parent的平衡因子++
			}
			else
			{
				parent->_bf--;//新增结点在parent的左边，parent的平衡因子 --
			}
			//判断是否继续更新？
			if (parent->_bf == 0)// 1 or -1 -> 0 填上了矮的那一方
			{
				//1 or -1 -》 0 填上了矮的那一方，此时正好，无需更新
				break;
			}
			else if (parent->_bf == 1 || parent->_bf == -1)
			{
				//0 -》 1或-1  此时说明插入节点导致一边变高了，继续更新祖先
				cur = cur->_parent;
				parent = parent->_parent;
			}
			else if (parent->_bf == 1 || parent->_bf == -1)
			{
				//1 or -1 -》2或-2 插入节点导致本来高的一边又变更高了
				//此时子树不平衡，需要进行旋转
				//……
			}
			else
			{
				//插入之前AVL树就存在不平衡树，|平衡因子| >= 2的节点
				//实际上根据前面的判断不可能走到这一步，不过这里其实是为了检测先前的插入是否存在问题
				assert(false);
			}
		}
		return true;
	}
private:
	Node* _root = nullptr;
};



