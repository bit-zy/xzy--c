#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<fstream>
using namespace std;
//int main()
//{
//	ifstream ifs("test.cpp");
//	//法一：
//	while (ifs)
//	{
//		char ch = ifs.get();
//		cout << ch;
//	}
//	/*法二
//	char ch;
//	while (ifs >> ch)
//	{
//		cout << ch;这里读出的结果不换行，过滤掉了空格和换行
//	}*/
//	return 0;
//}

int main()
{
	ifstream ifs("test.cpp");
	ofstream ofs("copy.cpp");
	char ch = ifs.get();
	while (~ch)
	{
		ofs << ch;
		ch = ifs.get();
	}
	return 0;
}