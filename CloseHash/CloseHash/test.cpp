#define _CRT_SECURE_NO_WARNINGS 1
#include"CloseHash.h"
void TestHT1()
{
	int a[] = { 20, 5, 8, 99999, 10, 30, 50 };
	//HashTable<int, int, DefaultHash<int>> ht;
	CloseHash::HashTable<int, int> ht;
	if (ht.Find(10))
	{
		cout << "找到了10" << endl;
	}
	for (auto e : a)
	{
		ht.Insert(make_pair(e, e));
	}
	//测试扩容
	ht.Insert(make_pair(15, 15));
	ht.Insert(make_pair(5, 5));
	ht.Insert(make_pair(15, 15));
	if (ht.Find(50))
	{
		cout << "找到了50" << endl;
	}
	if (ht.Find(10))
	{
		cout << "找到了10" << endl;
	}
	ht.Erase(10);

	if (ht.Find(50))
	{
		cout << "找到了50" << endl;
	}
	if (ht.Find(10))
	{
		cout << "找到了10" << endl;
	}
}
void TestHT2()
{
	string arr[] = { "苹果", "西瓜", "苹果", "西瓜", "苹果", "苹果", "西瓜", "苹果", "香蕉", "苹果", "香蕉" };
	/*string s1("苹果");
	string s2("果苹");
	string s3("果果");
	string s4("萍果");

	string s5("abcd");
	string s6("bcad");
	string s7("aadd");
	DefaultHash hf;
	cout << hf(s1) << endl;
	cout << hf(s2) << endl;
	cout << hf(s3) << endl;
	cout << hf(s4) << endl << endl;
	cout << hf(s5) << endl;
	cout << hf(s6) << endl;
	cout << hf(s7) << endl;*/
	CloseHash::HashTable<string, int> countHT;
	for (auto& str : arr)
	{
		auto ret = countHT.Find(str);
		if (ret)
		{
			ret->_kv.second++;
		}
		else
		{
			countHT.Insert(make_pair(str, 1));
		}
	}
}


int main()
{
	//TestHT1();
	TestHT2();
	return 0;
}