#define _CRT_SECURE_NO_WARNINGS 1
#include"List.h"
//#include"Reverse_Iterator.h"

void test1()
{
	cpp::list<int> lt;
	lt.push_back(1);
	lt.push_back(2);
	lt.push_back(3);
	lt.push_back(4);
	cpp::list<int>::iterator it = lt.begin();
	while (it != lt.end())
	{
		cout << *it << " ";//1 2 3 4
		++it;
	}
}

struct AA
{
	AA(int a1 = 0, int a2 = 0)
		:_a1(a1)
		, _a2(a2)
	{}
	int _a1;
	int _a2;
};

void test2()
{
	cpp::list<AA> lt;
	lt.push_back(AA(1, 1));
	lt.push_back(AA(2, 2));
	lt.push_back(AA(3, 3));
	lt.push_back(AA(4, 4));
	cpp::list<AA>::iterator it = lt.begin();
	while (it != lt.end())
	{
		//cout << (*it)._a1 << "-" << (*it)._a2 << " ";//1-1 2-2 3-3 4-4
		cout << it->_a1 << "-" << it->_a2 << " ";//1-1 2-2 3-3 4-4
		it++;
	}
}

void print_list(const cpp::list<int>& lt)
{
	cpp::list<int>::const_iterator it = lt.begin();
	while (it != lt.end())
	{
		//*it = 10;
		cout << *it << " ";//1 2 3 4
		++it;
	}
}
void print_list2(cpp::list<int>& lt)
{
	cpp::list<int>::iterator it = lt.begin();
	while (it != lt.end())
	{
		(*it);
		cout << *it << " ";//2 3 4 5
		++it;
	}
}
void test4()
{
	cpp::list<int> lt;
	lt.push_back(1);
	lt.push_back(2);
	lt.push_back(3);
	lt.push_back(4);

	lt.push_front(1);
	lt.push_front(2);
	lt.push_front(3);
	lt.push_front(4);
	for (auto e : lt)
		cout << e << " ";
	cout << endl;
	lt.pop_front();
	lt.pop_front();

	lt.pop_back();
	lt.pop_back();
	for (auto e : lt)
		cout << e << " ";
}


//测试迭代器失效
/*
* list的insert不存在迭代器失效
* 不存在野指针问题，也不存在意义变了的问题
*/
void test5()
{
	cpp::list<int> lt;
	lt.push_back(1);
	lt.push_back(2);
	lt.push_back(3);
	lt.push_back(4);
	lt.push_back(5);
	lt.push_back(6);

	//要求在偶数的前面插入这个偶数*10
	auto it1 = lt.begin();
	while (it1 != lt.end())
	{
		if (*it1 % 2 == 0)
		{
			lt.insert(it1, *it1 * 10);
		}
		it1++;
	}
	for (auto e : lt)
		cout << e << " ";
	cout << endl;
}
/*
* list的erase以后迭代器是失效的，经典野指针失效，因为这个迭代器指向结点已经被释放了
*/
void test6()
{
	cpp::list<int> lt;
	lt.push_back(1);
	lt.push_back(2);
	lt.push_back(3);
	lt.push_back(4);
	lt.push_back(5);
	lt.push_back(6);

	//删除所有的偶数
	auto it1 = lt.begin();
	while (it1 != lt.end())
	{
		if (*it1 % 2 == 0)
		{
			it1 = lt.erase(it1);
		}
		it1++;
	}
	for (auto e : lt)
		cout << e << " ";
	cout << endl;
	lt.clear();
	for (auto e : lt)
		cout << e << " ";
	cout << endl;
	lt.push_back(3);
	lt.push_back(4);
	lt.push_back(5);
	lt.push_back(6);
	for (auto e : lt)
		cout << e << " ";
}

void test7()
{
	cpp::list<int> lt1;
	lt1.push_back(1);
	lt1.push_back(2);
	lt1.push_back(3);
	lt1.push_back(4);
	lt1.push_back(5);
	lt1.push_back(6);

	cpp::list<int> lt2(lt1);
	for (auto e : lt2)
	{
		cout << e << " ";
	}
	cout << endl;

	cpp::list<int> lt3 = lt2;
	for (auto e : lt2)
	{
		cout << e << " ";
	}
	cout << endl;
}

void test8()
{
	cpp::list<int> lt;
	lt.push_back(1);
	lt.push_back(2);
	lt.push_back(3);
	lt.push_back(4);
	lt.push_back(5);
	lt.push_back(6);
	cpp::list<int>::reverse_iterator rit = lt.rbegin();
	while (rit != lt.rend())
	{
		cout << *rit << " ";
		++rit;
	}
}
int main()
{
	//test1();
	//test2();
	cpp::list<int> lt;
	lt.push_back(1);
	lt.push_back(2);
	lt.push_back(3);
	lt.push_back(4);
	//print_list(lt);
	//print_list2(lt);
	//test4();
	//test5();
	//test6();
	//test7();
	test8();
}