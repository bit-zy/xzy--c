#define _CRT_SECURE_NO_WARNINGS 1
#include"HashTable.h"
void TestHT1()
{
	int a[] = { 20, 5, 8, 99999, 10, 30, 50 };
	//HashTable<int, int, DefaultHash<int>> ht;
	CloseHash::HashTable<int, int> ht;
	if (ht.Find(10))
	{
		cout << "�ҵ���10" << endl;
	}
	for (auto e : a)
	{
		ht.Insert(make_pair(e, e));
	}
	//��������
	ht.Insert(make_pair(15, 15));
	ht.Insert(make_pair(5, 5));
	ht.Insert(make_pair(15, 15));
	if (ht.Find(50))
	{  
		cout << "�ҵ���50" << endl;
	}
	if (ht.Find(10))
	{
		cout << "�ҵ���10" << endl;
	}
	ht.Erase(10);
	
	if (ht.Find(50))
	{
		cout << "�ҵ���50" << endl;
	}
	if (ht.Find(10))
	{
		cout << "�ҵ���10" << endl;
	}
}
void TestHT2()
{
	string arr[] = { "ƻ��", "����", "ƻ��", "����", "ƻ��", "ƻ��", "����", "ƻ��", "�㽶", "ƻ��", "�㽶" };
	/*string s1("ƻ��");
	string s2("��ƻ");
	string s3("����");
	string s4("Ƽ��");

	string s5("abcd");
	string s6("bcad");
	string s7("aadd");
	DefaultHash hf;
	cout << hf(s1) << endl;
	cout << hf(s2) << endl;
	cout << hf(s3) << endl;
	cout << hf(s4) << endl << endl;
	cout << hf(s5) << endl;
	cout << hf(s6) << endl;
	cout << hf(s7) << endl;*/
	CloseHash::HashTable<string, int> countHT;
	for (auto& str : arr)
	{
		auto ret = countHT.Find(str);
		if (ret)
		{
			ret->_kv.second++;
		}
		else
		{
			countHT.Insert(make_pair(str, 1));
		}
	}
}

void TestHT3()
{
	int a[] = { 20, 5, 8, 99999, 10, 30, 50 };
	//HashTable<int, int, DefaultHash<int>> ht;
	Bucket::HashTable<int, int> ht;
	if (ht.Find(10))
	{
		cout << "�ҵ���10" << endl;
	}
	for (auto e : a)
	{
		ht.Insert(make_pair(e, e));
	}
	//��������
	ht.Insert(make_pair(15, 15));
	ht.Insert(make_pair(5, 5));
	ht.Insert(make_pair(15, 15));
	ht.Insert(make_pair(25, 15));
	ht.Insert(make_pair(35, 15));
	ht.Insert(make_pair(45, 15));
}
void TestHT4()
{
	int a[] = { 20, 5, 8, 99999, 10, 30, 50 };
	//HashTable<int, int, DefaultHash<int>> ht;
	Bucket::HashTable<int, int> ht;
	if (ht.Find(10))
	{
		cout << "�ҵ���10" << endl;
	}
	for (auto e : a)
	{
		ht.Insert(make_pair(e, e));
	}
	ht.Erase(20);
	ht.Erase(10);
	ht.Erase(30);
	ht.Erase(50);
	//��������
	ht.Insert(make_pair(15, 15));
	ht.Insert(make_pair(5, 5));
	ht.Insert(make_pair(15, 15));
	ht.Insert(make_pair(25, 15));
	ht.Insert(make_pair(35, 15));
	ht.Insert(make_pair(45, 15));
}


void TestHT5()
{
	string arr[] = { "ƻ��", "����", "ƻ��", "����", "ƻ��", "ƻ��", "����", "ƻ��", "�㽶", "ƻ��", "�㽶" };
	Bucket::HashTable<string, int> countHT;
	for (auto& str : arr)
	{
		auto ret = countHT.Find(str);
		if (ret)
		{
			ret->_kv.second++;
		}
		else
		{
			countHT.Insert(make_pair(str, 1));
		}
	}
}
int main()
{
	//TestHT1();
	//TestHT2();
	//TestHT3();
	TestHT4();
	//TestHT5();
	return 0;
}

