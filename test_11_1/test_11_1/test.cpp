#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
#include<string>
#include<math.h>
using namespace std;
bool IsPrime(int n)
{
    for (int i = 2; i < sqrt(n); i++)
    {
        if (n % i == 0)
            return false;
    }
    return true;
}
int main()
{
    int n = 0;
    while (cin >> n)
    {
        vector<int> v;
        string str = to_string(n);
        str += " = ";
        int i = 2;
        while (n != 1)
        {
            if (IsPrime(i) && n % i == 0)
            {
                str += to_string(i);

                if (n / i != 1)
                    str += " * ";
                n /= i;
                continue;
            }
            else
            {
                i++;
            }
        }
        cout << str << " ";
    }
    return 0;
}