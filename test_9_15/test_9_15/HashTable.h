#pragma once
#include<iostream>
#include<vector>
using namespace std;
enum States
{
	EMPTY,
	EXIST,
	DELETE
};
//哈希节点状态的类
template<class K, class V>
struct HashData
{
	pair<K, V> _kv;
	State _state;//记录每个位置的状态
};
//哈希表的类
template<class K, class V>
class HashTable
{
private:
	vector<HashData> _tables;
};