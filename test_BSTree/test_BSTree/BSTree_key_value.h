#pragma once
#include<iostream>
#include<string>
using namespace std;
namespace key_value
{
	template<class K, class V>
	struct BSTreeNode
	{
		BSTreeNode<K, V>* _left; //左指针
		BSTreeNode<K, V>* _right;//右指针
		const K _key;//节点值，假设const修饰防止后续修改key导致破坏二叉搜索树的结构
		V _value;
		BSTreeNode(const K& key, const V& value)//构造函数
			:_left(nullptr)
			, _right(nullptr)
			, _key(key)
			, _value(value)
		{}
	};

	template<class K, class V>
	class BSTree
	{
		typedef BSTreeNode<K, V> Node;
	public:
		//中序遍历 -- 递归
		void InOrder()
		{
			_InOrder(_root);
			cout << endl;
		}

		///////////////////////////////////////递归版的插入、删除、查找/////////////////////////////////////////////
			//查找
		Node* FindR(const K& key)
		{
			return _FindR(_root, key);
		}
		//插入
		bool InsertR(const K& key, const V& value)
		{
			return _InsertR(_root, key, value);
		}
		//删除
		bool EraseR(const K& key)
		{
			return _EraseR(_root, key);
		}

	private:
		//查找的子树
		Node* _FindR(Node* root, const K& key)//查找的时候要返回结点的指针，为了方便后续达到可以修改value而不能修改key的效果
		{
			if (root == nullptr)
				return nullptr;
			if (root->_key < key)
			{
				//如果比key小，转换到右子树去找
				return _FindR(root->_right, key);
			}
			else if (root->_key > key)
			{
				//如果比key大，转换到左子树去找
				return _FindR(root->_left, key);
			}
			else
			{
				//找到了
				return root;
			}
		}
		//插入的子树
		bool _InsertR(Node*& root, const K& key, const V& value)//Node*&为指针的引用
		{
			if (root == nullptr)
			{
				root = new Node(key, value);//当root为空，把自己创建成新结点
				return true;
			}
			if (root->_key < key)
				return _InsertR(root->_right, key, value);//如果比key小，转换到右子树去插入
			else if (root->_key > key)
				return _InsertR(root->_left, key, value);//如果比key大，转换到左子树去插入
			else
				return false;//如果相等，就返回false
		}
		//删除的子树
		bool _EraseR(Node*& root, const K& key)
		{
			//1、递归查找删除的位置
			if (root == nullptr)
			{
				//如果是空就返回false
				return false;
			}
			if (root->_key < key)
			{
				return _EraseR(root->_right, key);//如果比key小，转换到右子树去插入
			}
			else if (root->_key > key)
			{
				return _EraseR(root->_left, key);//如果比key大，转换到左子树去插入
			}
			//2、确认链接关系
			else
			{
				Node* del = root;//提前保存root结点的位置
				//开始删除
				if (root->_left == nullptr)
				{
					//如果左为空
					root = root->_right;
				}
				else if (root->_right == nullptr)
				{
					//如果右为空
					root = root->_left;
				}
				else
				{
					Node* minRight = root->_right;//minRight用于找到右子树的最小值
					while (minRight->_left)
					{
						minRight = minRight->_left;
					}
					swap(root->_key, minRight->_key);
					return _EraseR(root->_right, key);
				}
				delete del;
				return true;
			}
		}

		//中序遍历的子树
		void _InOrder(Node* root)
		{
			if (root == nullptr)
				return;
			_InOrder(root->_left);//递归到左子树
			cout << root->_key << ":" << root->_value << endl;;//访问根结点
			_InOrder(root->_right);//递归到右子树
		}
	private:
		Node* _root = nullptr;
	};
}