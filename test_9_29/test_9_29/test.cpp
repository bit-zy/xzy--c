#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;
//class Base
//{
//public:
//	virtual int foo(int x)
//	{
//		return x * 10;
//	}
//	int foo(char x[14])
//	{
//		char y[10] = { 0 };
//		cout << sizeof(y) << endl;
//		cout << sizeof(x) << endl;
//		return sizeof(x) + 10;
//	}
//};
//class Derived : public Base
//{
//	int foo(int x)
//	{
//		return x * 20;
//	}
//	virtual int foo(char x[10])
//	{
//		return sizeof(x) + 20;
//	}
//};
//int main()
//{
//	Derived stDerived;
//	Base* pstBase = &stDerived;
//	char x[10];
//	//cout << sizeof(x) << endl;
//	printf("%d\n", pstBase->foo(100) + pstBase->foo(x));
//	return 0;
//}


//int foo(char x[14])//(char* x[14])
//{
//	cout << sizeof(x) << endl;
//	//����
//	return sizeof(x) + 10;
//}
//int main()
//{
//	char x[10] = { 0 };
//	cout << foo(x) << endl;
//	return 0;
//}

//class A
//{
//public:
//	void FuncA()
//	{
//		printf("FuncA called\n");
//	}
//	virtual void FuncB()
//	{
//		printf("FuncB called\n");
//	}
//};
//class B : public A
//{
//public:
//	void FuncA()
//	{
//		A::FuncA();
//		printf("FuncAB called\n");
//	}
//	virtual void FuncB()
//	{
//		printf("FuncBB called\n");
//	}
//};
//int main()
//{	
//	B b;
//	A* pa;
//	pa = &b;
//	A* pa2 = new A;
//	pa->FuncA();
//	pa->FuncB();
//	pa2->FuncA();
//	pa2->FuncB();
//	delete pa2;
//	return 0;
//}
//class Base
//{
//public:
//	int Bar(char x)
//	{
//		return (int)(x);
//	}
//	virtual int Bar(int x)
//	{
//		return (2 * x);
//	}
//};
//class Derived : public Base
//{
//public:
//	int Bar(char x)
//	{
//		return (int)(-x);
//	}
//	int Bar(int x)
//	{
//		return (x / 2);
//	}
//};
//int main()
//{
//	Derived Obj;
//	Base* pObj = &Obj;
//	printf("%d,", pObj->Bar((char)(100)));
//	printf("%d,", pObj->Bar(100));
//
//	return 0;
//}

//int main()
//{
//	int a = 10;
//	int* p = &a;
//	*(p) = 20;
//	cout << a;
//	return 0;
//}

//class Test
//{
//public:
//	int a;
//	int b;
//	virtual void fun(){}
//	Test(int tmp1 = 0, int tmp2 = 0)
//	{
//		a = tmp1;
//		b = tmp2;
//	}
//	int getA()
//	{
//		return a;
//	}
//	int getB()
//	{
//		return b;
//	}
//};
//int main()
//{
//	Test obj(5, 10);
//	int* pInt = (int*)&obj;
//	*(pInt + 0) = 100;
//	*(pInt + 1) = 200;
//	cout << "a = " << obj.getA() << endl;
//	cout << "b = " << obj.getB() << endl;
//	return 0;
//}


#include<iostream>
using namespace std;
bool IsPerNum(int num)
{
    int sum = 0;
    for (int i = 1; i <= num / 2; i++)
    {
        if (num % i == 0)
            sum += i;
    }
    if (sum == num)
        return true;
    else
        return false;
}
int main()
{
    int n = 0;
    int count = 0;
    while (n--)
    {
        if (IsPerNum(n))
        {
            count++;
        }
    }
    cout << count << endl;
    return 0;
}