﻿#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;
#include<vector>
#include <algorithm>
#include <functional>
//int main()
//{
//	int array[] = { 4,1,8,5,3,7,0,9,2,6 };
//	// 默认按照小于比较，排出来结果是升序
//	std::sort(array, array + sizeof(array) / sizeof(array[0]));
//	// 如果需要降序，需要改变元素的比较规则
//	std::sort(array, array + sizeof(array) / sizeof(array[0]), greater<int>());
//	return 0;
//}
//int main()
//{
//	
//	return 0;
//}

//struct Goods
//{
//	string _name; // 名字
//	double _price; // 价格
//	int _evaluate; // 评价
//	Goods(const char* str, double price, int evaluate)
//		:_name(str)
//		, _price(price)
//		, _evaluate(evaluate)
//	{}
//};
//struct ComparePriceLess
//{
//	bool operator()(const Goods& gl, const Goods& gr)
//	{
//		return gl._price < gr._price;
//	}
//};
//struct ComparePriceGreater
//{
//	bool operator()(const Goods& gl, const Goods& gr)
//	{
//		return gl._price > gr._price;
//	}
//};
//struct CompareNumLess
//{
//	bool operator()(const Goods& g1, const Goods& g2)
//	{
//		return g1._num < g2._num;
//	}
//};
//struct CompareNumGreater
//{
//	bool operator()(const Goods& g1, const Goods& g2)
//	{
//		return g1._num > g2._num;
//	}
//};
//int main()
//{
//	vector<Goods> v = { { "苹果", 2.1, 5 }, { "香蕉", 3, 4 }, { "橙子", 2.2,3 }, { "菠萝", 1.5, 4 } };
//	sort(v.begin(), v.end(), ComparePriceLess());
//	sort(v.begin(), v.end(), ComparePriceGreater());
//}

////  捕捉列表		  参数列表    取消常量性	 返回值类型     函数体
//[capture - list](parameters) mutable -> return-type{ statement }


//int main()
//{
//	int a = 2, b = 5;
//	auto Add1 = [](int x, int y)->int { return x + y; };
//	cout << Add1(a, b) << endl;//7
//	//返回值类型明确的情况下，返回值类型可以省略掉，由编译器自动推导
//	auto Add2 = [](int x, int y) { return x + y; };
//	cout << Add2(a, b) << endl;//7
//	return 0;
//}
//int main()
//{
//	int a = 0, b = 200;
//	auto Add1 = [](int x, int y)->double { return (x + y) / 3.0; };
//	auto Add2 = [](int x, int y)->int { return (x + y) / 3.0; };
//	cout << Add1(a, b) << endl;//66.6667
//	cout << Add2(a, b) << endl;//66
//	return 0;
//}


//int main()
//{
//	int a = 0, b = 200;
//	auto Add1 = [](int x, int y)->double { return (x + y) / 3.0; };
//	auto Add2 = [](int x, int y)->int { return (x + y) / 3.0; };
//	//传值捕捉
//	auto Add3 = [a, b] { return (a + b) / 3.0; };
//	cout << Add1(a, b) << endl;//66.6667
//	cout << Add2(a, b) << endl;//66
//	cout << Add3() << endl;//66.6667
//	return 0;
//}
//int main()
//{
//	int a = 0, b = 200;
//	auto swap1 = [](int& x, int& y) {
//		int tmp = x;
//		x = y;
//		y = tmp;
//	};
//	swap1(a, b);
//	cout << a << " " << b << endl;//200 0
//	return 0;
//}
//int main()
//{
//	int a = 0, b = 200;
//	auto swap1 = [](int& x, int& y) {
//		int tmp = x;
//		x = y;
//		y = tmp;
//	};
//	auto swap2 = [a, b]{
//		int tmp = a;
//		a = b;
//		b = tmp;
//	};
//	swap2();
//	cout << a << " " << b << endl;//200 0
//	return 0;
//}



//int main()
//{
//	int a = 0, b = 200;
//	//mutable只是让传值捕捉变量const属性去掉
//	auto swap2 = [a, b]()mutable {
//		int tmp = a;
//		a = b;
//		b = tmp;
//	};
//	swap2();
//	cout << a << " " << b << endl;
//	return 0;
//}
//int main()
//{
//	int a = 0, b = 200;
//	auto swap2 = [&a, &b] {
//		int tmp = a;
//		a = b;
//		b = tmp;
//	};
//	swap2();
//	cout << a << " " << b << endl;//200 0
//	return 0;
//}


//mutable只是让传值捕捉变量const属性去掉

//int main()
//{
//	int c = 2, d = 3, e = 4, f = 5, g = 6, ret;
//	//传值捕获全部变量
//	auto Func1 = [=] {
//		return c + d * e / f + g - 1;
//	};
//	cout << Func1() << endl;//9
//	//传引用捕获全部变量
//	auto Func2 = [&] {
//		ret = c + d * e / f + g - 2;
//		return ret;
//	};
//	cout << Func2() << endl;//8
//	//混着捕捉：c,d传值。ret传引用
//	auto Func3 = [c, d, &ret] {
//		ret = c + d;
//		return ret;
//	};
//	cout << Func3() << endl;//5
//	//混着捕捉：ret传引用捕捉，其它全部传值捕捉
//	auto Func4 = [=, &ret] {
//		ret = c + d * e / f + g - 3;
//		return ret;
//	};
//	cout << Func4() << endl;//7
//	return 0;
//}


//struct Goods
//{
//	string _name; // 名字
//	double _price; // 价格
//	int _evaluate; // 评价
//};
//int main()
//{
//	vector<Goods> v = { { "苹果", 2.1, 5 }, { "香蕉", 3, 4 }, { "橙子", 2.2,3 }, { "菠萝", 1.5, 4 } };
//	sort(v.begin(), v.end(), [](const Goods& g1, const Goods& g2) {
//		return g1._price < g2._price; });//价格升序
//	sort(v.begin(), v.end(), [](const Goods& g1, const Goods& g2) {
//		return g1._price > g2._price; });//价格降序
//	sort(v.begin(), v.end(), [](const Goods& g1, const Goods& g2) {
//		return g1._evaluate < g2._evaluate; });//评分升序
//	sort(v.begin(), v.end(), [](const Goods& g1, const Goods& g2) {
//		return g1._evaluate > g2._evaluate; });//评分降序
//}

//unordered_map<string, string, 

//void (*PF)();
//int main()
//{
//	auto f1 = [] {cout << "hello world" << endl; };
//	auto f2 = [] {cout << "hello world" << endl; };
//	// 此处先不解释原因，等lambda表达式底层实现原理看完后，大家就清楚了
//	//f1 = f2; // 编译失败--->提示找不到operator=()
//	// 允许使用一个lambda表达式拷贝构造一个新的副本
//	auto f3(f2);
//	f3();
//	// 可以将lambda表达式赋值给相同类型的函数指针
//	PF = f2;
//	PF();
//	return 0;
//}


//class Rate
//{
//public:
//	Rate(double rate) : _rate(rate)
//	{}
//	double operator()(double money, int year)
//	{
//		return money * _rate * year;
//	}
//private:
//	double _rate;
//};
//int main()
//{
//	// 函数对象
//	double rate = 0.49;
//	Rate r1(rate);
//	r1(10000, 2);
//	// lambda
//	auto r2 = [=](double monty, int year)->double {return monty * rate * year; };
//	r2(10000, 2);
//	return 0;
//}


//template<class F, class T>
//T useF(F f, T x)
//{
//	static int count = 0;
//	cout << "count:" << ++count << endl;
//	cout << "count:" << &count << endl;
//	return f(x);
//}
//double f(double i)
//{
//	return i / 2;
//}
//struct Functor
//{
//	double operator()(double d)
//	{
//		return d / 3;
//	}
//};
//int main()
//{
//	// 函数名
//	cout << useF(f, 11.11) << endl;
//	// 函数对象
//	cout << useF(Functor(), 11.11) << endl;
//	// lambda表达式
//	cout << useF([](double d)->double { return d / 4; }, 11.11) << endl;
//	return 0;
//}
//



#include <functional>
int f(int a, int b)
{
	return a + b;
}
struct Functor
{
public:
	int operator() (int a, int b)
	{
		return a + b;
	}
};
class Plus
{
public:
	static int plusi(int a, int b)
	{
		return a + b;
	}
	double plusd(double a, double b)
	{
		return a + b;
	}
};
int main()
{
	// 函数名(函数指针)
	std::function<int(int, int)> func1 = f;
	cout << func1(1, 2) << endl;
	// 仿函数
	std::function<int(int, int)> func2 = Functor();
	cout << func2(10, 20) << endl;
	// lambda表达式
	std::function<int(int, int)> func3 = [](int a, int b) {return a + b; };
	cout << func3(15, 25) << endl;
	//类的静态成员函数
	std::function<int(int, int)> func4 = Plus::plusi;//非静态成员函数必须加&，静态可不加
	cout << func4(100, 200) << endl;
	//类的非静态成员函数
	std::function<double(Plus, double, double)> func5 = &Plus::plusd;//非静态成员函数必须加&，静态可不加
	cout << func5(Plus(), 100.11, 200.11) << endl;
	return 0;
}


//int main()
//{
//	// 函数名(函数指针)
//	std::function<int(int, int)> func1 = f;
//	cout << func1(1, 2) << endl;
//	// 函数对象
//	std::function<int(int, int)> func2 = Functor();
//	cout << func2(1, 2) << endl;
//	// lamber表达式
//	std::function<int(int, int)> func3 = [](const int a, const int b)
//	{return a + b; };
//	cout << func3(1, 2) << endl;
//	// 类的成员函数
//	std::function<int(int, int)> func4 = &Plus::plusi;
//	cout << func4(1, 2) << endl;
//	std::function<double(Plus, double, double)> func5 = &Plus::plusd;
//	cout << func5(Plus(), 1.1, 2.2) << endl;
//	return 0;
//}