#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<string>
#include<map>
using namespace std;

void test_map1()
{
	//����
	map<string, string> dict;
	dict.insert(pair<string, string>("sort", "����"));
	pair<string, string> kv("byte", "�ֽ�");
	dict.insert(kv);
	dict.insert(make_pair("left", "���"));
	dict.insert({ "right", "�ұ�" });
	//����
	map<string, string>::iterator it = dict.begin();
	while (it != dict.end())
	{
		cout << (*it).first << ":" << (*it).second << endl;
		it++;
	}
}
void test()
{
	map<string, string> dict;
	auto ret1 = dict.insert(make_pair("left", "���"));
	auto ret2 = dict.insert(make_pair("left", "ʣ��"));
}
void test_map2()
{
	//����
	map<string, string> dict;
	dict.insert(make_pair("left", "��"));
	dict.insert(make_pair("right", "��"));
	dict.insert(make_pair("front", "ǰ"));
	dict.insert(make_pair("back", "��"));
	//����
	map<string, string>::iterator it = dict.begin();
	while (it != dict.end())
	{
		//cout << (*it).first << ":" << (*it).second << endl;
		cout << it->first << ":" << it->second << endl;
		it++;
	}
	cout << endl;
	//��Χfor
	for (const auto& e : dict)
		cout << e.first << ":" << e.second << endl;
}

void test_map3()
{
	string arr[] = { "ƻ��", "����", "ƻ��", "����", "ƻ��", "ƻ��", "����", "ƻ��", "�㽶", "ƻ��", "�㽶" };
	/*map<string, int> countMap;
	for (auto& str : arr)
	{
		map<string, int>::iterator it = countMap.find(str);
		if (it != countMap.end())
		{
			it->second++;
		}
		else
		{
			countMap.insert(make_pair(str, 1));
		}
	}*/

	map<string, int> countMap;
	for (auto& str : arr)
	{
		//pair<map<string, int>::iterator, bool> ret = countMap.insert(make_pair(str, 1));
		auto ret = countMap.insert(make_pair(str, 1));
		if (ret.second == false)
		{
			ret.first->second++;
		}
	}
	for (const auto& kv : countMap)
		cout << kv.first << ":" << kv.second << endl;
}
int main() 
{
	map<int, string> m1;
	map<int, string> m2(m1);
	map<int, string> m3(m2.begin(), m2.end());
	map<int, string, greater<int>> m4;
	//test_map1();
	//test_map2();
	test_map3();
	//test();
	return 0;
}
