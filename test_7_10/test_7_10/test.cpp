#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<vector>
using namespace std;
//第一题：买一把铲子
using namespace std;
int main()
{
    int k, r;
    int num = 1;
    cin >> k >> r;
    int sum = k;
    if (k % 10 == 0 || k % 10 == r)
        cout << num << endl;
    else
    {
        while (sum % 10 != 0 && sum % 10 != r)
        {
            sum += k;
            num++;
        }
        cout << num << endl;
    }
    return 0;
}
//第二题：区域和检索——数组不可变
class NumArray {
public:
    vector<int> sums;
    NumArray(vector<int>& nums) {
        int len = nums.size();
        sums.resize(len + 1);
        for (int i = 0; i < len; i++)
        {
            sums[i + 1] = sums[i] + nums[i];
        }
    }
    int sumRange(int left, int right) {
        return sums[right + 1] - sums[left];
    }
};
//第三题：所有奇数长度子数组的和
class Solution {
public:
    vector<int> nums;
    int sumOddLengthSubarrays(vector<int>& arr) {
        int len = arr.size();
        nums.resize(len + 1);
        for (int i = 0; i < len; i++)
        {
            nums[i + 1] = nums[i] + arr[i];
        }
        int sum = 0;

        for (int n = 0; n < arr.size(); n++)
        {
            for (int i = n + 1; i <= arr.size(); i += 2)
            {
                sum += nums[i] - nums[n];
            }
        }
        return sum;
    }
};