#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<algorithm>
#include<functional>
#include<string>
#include<vector>
using namespace std;
void test1()
{
	vector<int> v1;//存储int类型数据
	v1.push_back(1);
	vector<double> v2;
	v2.push_back(1.1);//存储double类型数据
	vector<string> v3;
	v3.push_back("hello world");//存储string类型数据
	vector<int> v4(10, 5);//用10个5来初始化v1
	vector<int> v5(v4);//用v1去拷贝构造v2
	vector<int> v6(v1.begin(), v1.end());//使用迭代器拷贝构造v2的数据
	string s = "hello world";
	vector<char> v(s.begin(), s.end());
}
void test2()
{
	vector<int> v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	////1、下标 + []
	////for (size_t i = 0; i < v.size(); i++)
	////{
	////	v[i] += 1;
	////	cout << v[i] << " "; // 2 3 4 5
	////}
	////2、迭代器
	//vector<int>::iterator it = v.begin();
	//while (it != v.end())
	//{
	//	*it -= 2;
	//	cout << *it << " "; // -1 0 1 2
	//	it++;
	//}
	//反向迭代器
	/*vector<int> v(10, 5);
	vector<int>::reverse_iterator rit = v.rbegin();
	while (rit != v.rend())
	{
		cout << *rit << " ";
		rit++;
	}*/
	//3、范围for
	for (auto e : v)
	{
		cout << e << " "; //1 2 3 4
	}
}

void test3()
{
	vector<int> v(7, 5);
	cout << v.size() << endl;//7
	cout << v.capacity() << endl;//5
}
void test4()
{
	vector<int> v1;
	cout << v1.max_size() << endl;//1073741823
	vector<char> v2;
	cout << v2.max_size() << endl;//2147483647
}
void test5()
{
	size_t sz;
	std::vector<int> foo;
	sz = foo.capacity();
	std::cout << "making foo grow:\n";
	for (int i = 0; i < 100; ++i) 
	{ 
		foo.push_back(i);
		if (sz != foo.capacity()) 
		{
			sz = foo.capacity();
			std::cout << "capacity changed: " << sz << '\n';
		}
	}
}

void test6()
{
	vector<int> v(10, 5);
	cout << v.capacity() << endl;//10
	//如果n > 当前容量大小，更新容量至n
	v.reserve(100);
	cout << v.capacity() << endl;//100
	//如果n < 当前容量大小，不做出任何改动
	v.reserve(20);
	cout << v.capacity() << endl;//100
}
void test7()
{
	vector<int> v(10, 5);
	cout << v.size() << endl;//10
	cout << v.capacity() << endl;//10
	//如果n的大小 > size和capacity，更新到n。超出的部分用1初始化
	v.resize(100, 1);
	cout << v.size() << endl;//100
	cout << v.capacity() << endl;//100
	//如果n的大小 < size，更新size到n，容量capacity不变
	v.resize(50);
	cout << v.size() << endl;//50
	cout << v.capacity() << endl;//100
	//如果n的大小 > size，且 < capacity，更新size到n，容量capacity不变
	v.resize(70);
	cout << v.size() << endl;//50
	cout << v.capacity() << endl;//100
}
void test8()
{
	vector<int> v;
	v.push_back(1);
	v.push_back(10);
	v.pop_back();
	v.pop_back();
}
void test9()
{
	vector<int> v;
	v.push_back(1);
	v.push_back(10);
	v.insert(v.begin(), 0); //在下标为0的位置插入0
	v.insert(v.begin(), 2, -1);//在下标为0的位置往后插入两个-1
	for (auto e : v)
		cout << e << " "; //-1 -1 0 1 10
	cout << endl;
	v.insert(v.begin() + 3, 2);//在下标为3的位置插入2
	for (auto e : v)
		cout << e << " "; //-1 -1 0 2 1 10
	cout << endl;
	v.erase(v.begin()); //头删
	for (auto e : v)
		cout << e << " "; //-1 0 2 1 10
	cout << endl;
	v.erase(v.begin() + 3); //删除下标为3的值
	for (auto e : v)
		cout << e << " "; //-1 0 2 10
	cout << endl;
	//删除在该迭代器区间内的元素（左闭右开）
	v.erase(v.begin(), v.begin() + 3);//删除下标[0, 3)左闭右开的值
	for (auto e : v)
		cout << e << " ";//10
}
void test10()
{
	vector<int> v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	//vector<int>::iterator pos = find(v.begin(), v.end(), 3);
	auto pos = find(v.begin(), v.end(), 3);//调用find在左闭右开的区间内寻找val
	if (pos != v.end())
	{
		cout << "找到了" << endl;
		v.erase(pos);//找到后，把该值删掉
	}
	else
	{
		cout << "没有找到" << endl;
	}
	for (auto e : v)
		cout << e << " "; //1 2 4
}
void test11()
{
	vector<int> v;
	v.push_back(1);
	v.push_back(-23);
	v.push_back(30);
	v.push_back(9);
	v.push_back(0);
	v.push_back(-90);
	//默认sort是升序
	sort(v.begin(), v.end());
	for (auto e : v)
		cout << e << " "; //-90 -23 0 1 9 30
	cout << endl;
//要排降序，就要用到仿函数,具体是啥后续详谈
	sort(v.begin(), v.end(), greater<int>());
	for (auto e : v)
		cout << e << " ";
}
int main()
{
	test11();
	
	return 0;
}