#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;
//class Person
//{
//public:
//	string _name; // 姓名
//	int a[10000];
//};
//class Student : public Person
//{
//protected:
//	int _num; //学号
//};
//class Teacher : public Person
//{
//protected:
//	int _id; // 职工编号
//};
//class Assistant : public Student, public Teacher
//{
//protected:
//	string _majorCourse; // 主修课程
//};
//int main()
//{
//	// 这样会有二义性无法明确知道访问的是哪一个
//	Assistant a;
////	a._name = "peter"; error 存在二义性的问题
//	// 需要显示指定访问哪个父类的成员可以解决二义性问题，但是数据冗余问题无法解决
//	a.Student::_name = "xxx";
//	a.Teacher::_name = "yyy";
//	//存在数据冗余问题
//	cout << sizeof(a) << endl; //80136
//}
//class Person
//{
//public:
//	string _name; // 姓名
//	int a[10000];
//};
//class Student : virtual public Person
//{
//protected:
//	int _num; //学号
//};
//class Teacher : virtual public Person
//{
//protected:
//	int _id; // 职工编号
//};
//class Assistant : public Student, public Teacher
//{
//protected:
//	string _majorCourse; // 主修课程
//};
//int main()
//{
//	Assistant a;
//	//不存在二义性问题
//    a._name = "peter"; 
//	//也不存在数据冗余问题
//	cout << sizeof(a) << endl;//40112
//	a.Student::_name = "xxx";
//	a.Teacher::_name = "yyy";
//}



class A
{
public:
	int _a;
};
//class B : public A
class B : virtual public A
{
public:
	int _b;
};
//class C : public A
class C : virtual public A
{
public:
	int _c;
};
class D : public B, public C
{
public:
	int _d;
};;



int main()
{
	D d;
	d._a = 0;
	d.B::_a = 1;
	d.C::_a = 2;
	d._b = 3;
	d._c = 4;
	d._d = 5;

	B b;
	b._a = 10;
	b._b = 20;
	B* ptr1 = &d;
	B* ptr2 = &b;

	cout << ptr1->_a << endl; //0
	cout << ptr2->_a << endl; //10
	cout << ptr1->_b << endl; //3
	cout << ptr2->_b << endl; //20
	return 0;
}