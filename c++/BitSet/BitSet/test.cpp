#define _CRT_SECURE_NO_WARNINGS 1
#include"BitSet.h"
void test_bit_set1()
{
	cpp::bitset<100> bs;
	cout << bs.none() << endl;
	bs.set(18);
	bs.set(19);
	cout << bs.any() << endl;
	cout << bs.none() << endl;

	/*bs.set(26);
	bs.set(1);
	bs.set(99);*/
	cout << bs.count() << endl;
	cout << bs.all() << endl;
	/*cout << bs.test(18) << endl;
	cout << bs.test(19) << endl;
	cout << bs.test(20) << endl;*/
	/*bs.reset(19);
	bs.reset(18);*/
}
void test_bit_set2()
{
	//cpp::bit_set<INT_MAX> bigBS;
	//cpp::bit_set<-1> bigBS;
	cpp::bitset<0xFFFFFFFF> bigBS;
}
void test_two_bitset()
{
	int a[] = { 4,3,2,4,5,2,2,4,7,8,9,2,1,3,7 };
	cpp::two_bitset<10> tbs;
	for (auto e : a)
	{
		tbs.set(e);
	}
	for (size_t i = 0; i < 10; i++)
	{
		if (tbs.is_once(i))
		{
			cout << i << endl;
		}
	}
}
int main()
{
	//test_bit_set1();
	//test_bit_set2();
	test_two_bitset();
	return 0;
}

