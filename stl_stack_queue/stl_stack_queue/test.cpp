#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<stack>
#include<list>
#include<functional>
#include<queue>
#include"Stack.h"
#include"Queue.h"
using namespace std;
void test_stack()
{
	cpp::stack<int> s;
	s.push(1);
	s.push(2);
	s.push(3);
	s.push(4);
	cout << s.size() << endl;//4
	while (!s.empty())
	{
		cout << s.top() << " ";//4 3 2 1
		s.pop();
	}
}queue<int, list<int>> q3;
void test_queue()
{
	cpp::queue<int, list<int>> q;
	q.push(1);
	q.push(2);
	q.push(3);
	q.push(40);
	cout << q.size() << endl;//4
	cout << q.back() << endl;//40
	while (!q.empty())
	{
		cout << q.front() << " ";//1 2 3 40
		q.pop();
	}
}

void test_priority_queue()
{
	//����less
	//cpp::less<int> LessCom;
	//cout << LessCom(1, 2) << endl;//1
	////����greater
	//cpp::greater<int> GreaterCom;
	//cout << GreaterCom(1, 5) << endl;//0
	//cpp::priority_queue<int> pq;
	cpp::priority_queue<int, vector<int>, greater<int>> pq;
	pq.push(9);
	pq.push(4);
	pq.push(7);
	pq.push(0);
	pq.push(2);
	cout << pq.size() << endl;//5
	while (!pq.empty())
	{
		cout << pq.top() << " ";//9 7 4 2 0 
		pq.pop();
	}
}
int main()
{
	stack<int> s;
	stack<int, vector<int>> s1;
	stack<int, list<int>> s2;
	//stack<int, string> s3;//���ڽض����ݶ�ʧ�ķ���
	//test_stack();
	
	//test_queue();
	test_priority_queue();
}

