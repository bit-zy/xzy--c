#define _CRT_SECURE_NO_WARNINGS 1
//class Solution {
//public:
//    bool CheckPermutation(string s1, string s2) {
//        sort(s1.begin(), s1.end());
//        sort(s2.begin(), s2.end());
//        if (s1 == s2)
//            return true;
//        else
//            return false;
//    }
//};
//
//class Solution {
//public:
//    bool isUnique(string astr) {
//        unordered_map<char, int> mp;
//        //利用哈希表建立映射关系
//        for (int i = 0; i < astr.length(); i++)
//        {
//            mp[astr[i]]++;
//        }
//        //统计次数
//        for (int i = 0; i < astr.length(); i++)
//        {
//            if (mp[astr[i]] > 1)
//                return false;
//        }
//        return true;
//    }
//};
//
//
//vector<int> FindNumsAppearOnce(vector<int>& array) {
//    // write code here
//    unordered_map<int, int> mp;
//    for (int i = 0; i < array.size(); i++)
//    {
//        mp[array[i]]++;
//    }
//    vector<int> v;
//    for (int i = 0; i < array.size(); i++)
//    {
//        if (mp[array[i]] == 1)
//        {
//            v.push_back(array[i]);
//        }
//    }
//    sort(v.begin(), v.end());
//    return v;
//}
//};


//#include<iostream>
//using namespace std;
//void fun(int& x, int& y)
//{
//	x = x ^ y;
//	y = y ^ x;
//	x = y ^ x;
//}
//int main()
//{
//	int a, b;
//	cin >> a >> b;
//	cout << "交换前：a=" << a << " : " << "b=" << b << endl;
//	fun(a, b);
//	cout << "交换后：a=" << a << " : " << "b=" << b << endl;
//	return 0;
//}


//#include<iostream>
//#include<vector>
//using namespace std;
//int main()
//{
//	vector<int> v = { 1, 1, 2, 2, 3, 3, 5, 5, 7, 7, 11 };
//	int k = 0;
//	for (auto e : v)
//	{
//		k ^= e;
//	}
//	cout << k << endl;
//	return 0;
//}
//
////获取一个二进制数字的最右的1
//   a = 01101110010000
//  ~a = 10010001101111
//~a+1 = 10010001110000
//a&(~a+1) = 00000000010000 = a&(-a)
////总结：a&(~a+1) = a&(-a)

//
//#include<iostream>
//#include<vector>
//using namespace std;
//int main()
//{
//	vector<int> v = { 6, 10, 6, 6, 4, 4, 12, 12, 12, 12, 3, 3 };
//	int ans = 0;
//	for (auto e : v)
//		ans ^= e;//结果为a ^ b
//	//提取最右的1
//	int rightOne = ans & (-ans);
//	int a = 0;
//	for (int i = 0; i < v.size(); i++)
//	{
//		if ((v[i] & rightOne) != 0)
//		{
//			a ^= v[i];
//		}
//	}
//	int b = a ^ ans;
//	cout << a << endl << b << endl;
//	return 0;
//}

//0000 1100 0010 1011
//0000 0000 0000 0100


#include<iostream>
#include<vector>
using namespace std;
int onlyKTimes(vector<int>& arr, int k, int m)
{
	int t[32] = { 0 };
	//t[0] 0位置的1出现了几个
	//t[i] i位置的1出现了几个
	for (auto num : arr)
	{
		for (int i = 0; i <= 31; i++)
		{
			if (((num >> i) & 1) != 0)//说明第i位不为0
			{
				t[i]++;
			}
		}
	}
	int ans = 0;
	for (int i = 0; i < 32; i++)
	{
		if (t[i] % m != 0)//说明第i位上是1
		{
			ans |= (1 << i);
		}
	}
	return ans;
}
int main()
{
	vector<int> v = {3, 3, 3, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5};
	int ret = onlyKTimes(v, 3, 5);
	cout << ret << endl;
	return 0;
}

