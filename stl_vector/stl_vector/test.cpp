#define _CRT_SECURE_NO_WARNINGS 1
#include<vector>
#include"vector.h"
void test()
{
	int i = 0;
	int j = int();
	int k = int(1);
	cout << i << endl;//0
	cout << j << endl;//0
	cout << k << endl;//1
}
void test_vector1()
{
	cpp::vector<int> v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	v.push_back(5);
	cpp::vector<int>::iterator it = v.begin();
	while (it != v.end())
	{
		cout << *it << " ";
		it++;
	}
	cout << endl;
	v.pop_back();
	v.pop_back();
	/*for (auto e : v)
	{
		cout << e << " ";
	}*/
	for (size_t i = 0; i < v.size(); i++)
	{
		cout << v[i] << " ";
	}
}

void test_vector2()
{
	cpp::vector<int> v;
	v.resize(10, -2);
	for (auto e : v)
	{
		cout << e << " ";
	}
}

void test_vector3()
{
	//在所有的偶数前面插入2
	cpp::vector<int> v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	v.push_back(5);
	v.push_back(6);
	cpp::vector<int>::iterator it = v.begin();
	while (it != v.end())
	{
		if (*it % 2 == 0)
		{
			//it = v.insert(it, 20);
			it++;
		}
		it++;
	}
	for (auto e : v)
	{
		cout << e << " ";
	}

}
//在所有的偶数前面插入2
	/*v.push_back(5);
	v.push_back(6);*/
	
void test_vector4()
{
	
	cpp::vector<int> v;
	//v.reserve(10);
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	cout << v.size() << ":" << v.capacity() << endl;
	auto pos = find(v.begin(), v.end(), 2);
	if (pos != v.end())
	{
		v.erase(pos);
	}
	cout << *pos << endl;
	*pos = 10;
	cout << *pos << endl << endl;
	cout << v.size() << ":" << v.capacity() << endl;
	for (auto e : v)
	{
		cout << e << " ";
	}
	
}

namespace std
{
	void test_vector5()
	{

		vector<int> v;
		//v.reserve(10);
		v.push_back(1);
		v.push_back(2);
		v.push_back(3);
		v.push_back(4);
		cout << v.size() << ":" << v.capacity() << endl;
		auto pos = find(v.begin(), v.end(), 4);
		if (pos != v.end())
		{
			v.erase(pos);
		}
		cout << *pos << endl;
		*pos = 10;
		cout << *pos << endl << endl;
		cout << v.size() << ":" << v.capacity() << endl;
		for (auto e : v)
		{
			cout << e << " ";
		}

	}
	void test_vector6()
	{
		//删除所有的偶数
		vector<int> v;
		//v.reserve(10);
		v.push_back(1);
		v.push_back(2);
		v.push_back(2);
		v.push_back(2);
		v.push_back(2);
		v.push_back(3);
		v.push_back(4);
		v.push_back(5);
		auto it = v.begin();
		while (it != v.end())
		{
			if (*it % 2 == 0)
			{
				it = v.erase(it);
			}
			else
			{
				it++;
			}
		}
		for (auto e : v)
		{
			cout << e << " ";
		}
	}
}
void test_vector7()
{
	//在所有的偶数前面插入2
	cpp::vector<int> v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	v.push_back(5);
	v.push_back(6);
	cpp::vector<int> v2(v.begin(), v.end());
	for (auto e : v2)
	{
		cout << e << " ";//1 2 3 4 5 6
	}
	cout << endl;
	string s("hello world");
	cpp::vector<char> v3(s.begin(), s.end());
	for (auto e : v3)
	{
		cout << e << " "; //h e l l o   w o r l d
	}
	cout << endl;
	cpp::vector<int> v4(v2);
	for (auto e : v4)
	{
		cout << e << " ";//1 2 3 4 5 6
	}
	cout << endl;
	v4.pop_back();
	v4.pop_back();
	v4.pop_back();
	cpp::vector<int> v5;
	v5 = v4;
	for (auto e : v5)
	{
		cout << e << " ";//1 2 3 4 5 6
	}
}

void test_vector8()
{
	cpp::vector<int> v(10, 4);
	for (auto e : v)
	{
		cout << e << " ";
	}
}


void test_vector9()
{
	cpp::vector<int> v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	//v.push_back(5);
	v.insert(v.begin(), 0); 
	for (auto e : v)
	{
		cout << e << " ";
	}
}

void test_vector10()
{
	//在所有的偶数前面插入2
	cpp::vector<int> v;
	//v.reserve(10);
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	v.push_back(5);
	v.push_back(6);
	cpp::vector<int>::iterator it = v.begin();
	while (it != v.end())
	{
		if (*it % 2 == 0)
		{
			it = v.insert(it, 20);
			it++;
		}
		it++;
	}
	for (auto e : v)
	{
		cout << e << " ";
	}
}
void test1()
{
	vector<int> v;
	v.reserve(10);
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	cout << v.size() << ":" << v.capacity() << endl;
	auto pos = find(v.begin(), v.end(), 2);
	if (pos != v.end())
	{
		v.insert(pos, 20);
	}
	cout << *pos << endl;
}
void test2()
{
	cpp::vector<int> v;
	//v.reserve(10);
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	cout << v.size() << ":" << v.capacity() << endl;
	auto pos = find(v.begin(), v.end(), 4);
	if (pos != v.end())
	{
		v.erase(pos);
	}
	cout << *pos << endl;
	*pos = 10;
	cout << *pos << endl << endl;
	cout << v.size() << ":" << v.capacity() << endl;
	for (auto e : v)
	{
		cout << e << " ";
	}
}
void test3()
{
	std::vector<int> v;
	//v.reserve(10);
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	cout << v.size() << ":" << v.capacity() << endl;
	auto pos = find(v.begin(), v.end(), 4);
	if (pos != v.end())
	{
		v.erase(pos);
	}
	cout << *pos << endl;
	*pos = 10;
	cout << *pos << endl << endl;
	cout << v.size() << ":" << v.capacity() << endl;
	for (auto e : v)
	{
		cout << e << " ";
	}
}
void test4()
{
	//删除所有的偶数
	std::vector<int> v;
	//v.reserve(10);
	v.push_back(1);
	v.push_back(2);
	v.push_back(2);
	v.push_back(2);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	v.push_back(5);
	auto it = v.begin();
	while (it != v.end())
	{
		if (*it % 2 == 0)
		{
			it = v.erase(it);
		}
		else
		{
			it++;
		}
	}
	for (auto e : v)
	{
		cout << e << " ";
	}
}
namespace cpp
{
	class Solution {
	public:
		// 核心思想：找出杨辉三角的规律，发现每一行头尾都是1，中间第[j]个数等于上一行[j-1]+[j]
		vector<vector<int>> generate(int numRows) {
			vector<vector<int>> vv;
			// 先开辟杨辉三角的空间
			vv.resize(numRows);
			for (size_t i = 1; i <= numRows; ++i)
			{
				vv[i - 1].resize(i, 0);
				// 每一行的第一个和最后一个都是1
				vv[i - 1][0] = 1;
				vv[i - 1][i - 1] = 1;
			}
			for (size_t i = 0; i < vv.size(); ++i)
			{
				for (size_t j = 0; j < vv[i].size(); ++j)
				{
					if (vv[i][j] == 0)
					{
						vv[i][j] = vv[i - 1][j - 1] + vv[i - 1][j];
					}
				}
			}
			return vv;
		}
	};

	void test7()
	{
		vector<vector<int>> vv = Solution().generate(5);
		for (size_t i = 0; i < vv.size(); ++i)
		{
			for (size_t j = 0; j < vv[i].size(); ++j)
			{
				cout << vv[i][j] << " ";
			}
			cout << endl;
		}
	}
}
int main()
{
	/*cpp::vector<int> v;
	size_t size = v.size();
	size_t capacity = v.capacity();
	cout << "size:" << size << endl;
	cout << "capacity:" << capacity << endl;

	v.push_back(1);
	cout << "size:" << size << endl;
	cout << "capacity:" << capacity << endl;
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);*/

	//test_vector1();
	//test();
	//test_vector2();
	//test_vector3();
	//std::test_vector5();
	//std::test_vector6();
	//test_vector8();
	//test_vector10();
	//test2();
	//test3();
	//test4();
	cpp::test7();
}