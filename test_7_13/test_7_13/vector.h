#pragma once
#include<assert.h>
#include<iostream>
using namespace std;

namespace cpp
{
	template<class T>
	class vector
	{
	public:
		typedef T* iterator;
		typedef const T* const_iterator;
		/*构造函数*/
			//无参构造
		vector()
			:_start(nullptr)
			, _finish(nullptr)
			, _endofstoage(nullptr)
		{}
		//带参构造
		template <class InputIterator>
		vector(InputIterator first, InputIterator last)
			: _start(nullptr)
			, _finish(nullptr)
			, _endofstoage(nullptr)
		{
			//将迭代器区间在[first, last)的数据一个个尾插到容器当中
			while (first != last)
			{
				push_back(*first);
				first++;
			}
		}
		//用n个val来构造vector
		vector(size_t n, const T& val = T())
			: _start(nullptr)
			, _finish(nullptr)
			, _endofstoage(nullptr)
		{
			reserve(n);
			for (size_t i = 0; i < n; i++)
			{
				push_back(val);
			}
		}
		vector(int n, const T& val = T())
			: _start(nullptr)
			, _finish(nullptr)
			, _endofstoage(nullptr)
		{
			reserve(n);
			for (int i = 0; i < n; i++)
			{
				push_back(val);
			}
		}


		//交换函数
		void swap(vector<T>& v)
		{
			std::swap(_start, v._start);
			std::swap(_finish, v._finish);
			std::swap(_endofstoage, v._endofstoage);
		}
		/*拷贝构造函数*/
				//vector(const vector& v) 也可以这样写，但不推荐
		vector(const vector<T>& v)
			:_start(nullptr)
			, _finish(nullptr)
			, _endofstoage(nullptr)
		{
			vector<T> tmp(v.begin(), v.end());
			swap(tmp);
		}

		/*赋值运算符重载*/
				//vector& operator=(vector v) 也可以这样写，但不推荐
		vector<T>& operator=(vector<T> v)
		{
			this->swap(v);
			return *this;
		}

		/*析构函数*/
		~vector()
		{
			if (_start)//避免释放空指针
			{
				delete[] _start;//释放容器所指向的空间
				_start = _finish = _endofstoage = nullptr;//置空
			}
		}


		/*容器访问相关函数*/
			//迭代器
				//begin
		iterator begin()
		{
			return _start;
		}
		//end
		iterator end()
		{
			return _finish;
		}
		//const版本迭代器
		const_iterator begin() const
		{
			return _start;
		}
		//end
		const_iterator end() const
		{
			return _finish;
		}

		//operator[]运算符重载
		T& operator[](size_t pos)
		{
			assert(pos < size());//检测pos的合法性
			return _start[pos];
		}
		//const版本的[]运算符重载
		const T& operator[](size_t pos) const
		{
			assert(pos < size());//检测pos的合法性
			return _start[pos];
		}

		/*空间增长问题*/
			//size
		size_t size() const //最好加上const，普通对象和const对象均可调用
		{
			return _finish - _start; //指针相减就能得到size的个数
		}
		//capacity
		size_t capacity() const
		{
			return _endofstoage - _start;
		}

		//reserve扩容
		void reserve(size_t n)
		{
			size_t sz = size();//提前算出size()的大小，方便后续更新_finish
			if (n > capacity())
			{
				T* tmp = new T[n];
				if (_start)//判断旧空间是否有数据
				{
					memcpy(tmp, _start, sizeof(T) * size());
					delete[] _start;//释放旧空间
				}
				_start = tmp;//指向新空间
			}
			//更新_finish和_endofstoage
			_finish = _start + sz;
			_endofstoage = _start + n;
		}
		//resize
			//void resize(size_t n, T val = T())
		void resize(size_t n, const T& val = T()) //利用T()调用默认构造函数的值进行初始化，这样写说明C++的内置类型也有自己的构造函数
		{
			//如果 n > capacity()容量，就需要扩容
			if (n > capacity())
			{
				reserve(n);
			}
			//如果 n > size()，就需要把有效数据_finish到_start + n之间的数据置为缺省值val
			if (n > size())
			{
				while (_finish < _start + n)
				{
					*_finish = val;
					_finish++;
				}
			}
			//如果 n < size()，更新有效数据到_start + n
			else
			{
				_finish = _start + n;
			}
		}


		/*增加*/
			//push_back
		void push_back(const T& x)
		{
			//检测是否需要扩容
			if (_finish == _endofstoage)
			{
				size_t newcapcacity = capacity() == 0 ? 4 : capacity() * 2;
				reserve(newcapcacity);
			}
			*_finish = x;
			_finish++;
			/*
				法二：复用insert
				insert(end(), x); //当insert中的参数pos为end()时，就是尾插
			*/
		}
		//insert
			//iterator insert(iterator pos, const T& x)
			//{
			//	//检测参数合法性
			//	assert(pos >= _start && pos <= _finish);
			//	//检测是否需要扩容
			//	/*扩容以后pos就失效了，需要更新一下*/
			//	if (_finish == _endofstoage)
			//	{
			//		size_t n = pos - _start;//计算pos和start的相对距离
			//		size_t newcapcacity = capacity() == 0 ? 4 : capacity() * 2;
			//		reserve(newcapcacity);
			//		pos = _start + n;//防止迭代器失效，要让pos始终指向与_start间距n的位置
			//	}
			//	//挪动数据
			//	iterator end = _finish - 1;
			//	while (end >= pos)
			//	{
			//		*(end + 1) = *(end);
			//		end--;
			//	}
			//	//把值插进去
			//	*pos = x;
			//	_finish++;
			//	return pos;
			//}

		void insert(iterator pos, const T& x)
		{
			//检测参数合法性
			assert(pos >= _start && pos <= _finish);
			/*扩容以后pos就失效了，需要更新一下*/
			if (_finish == _endofstoage)
			{
				size_t n = pos - _start;//计算pos和start的相对距离
				size_t newcapcacity = capacity() == 0 ? 4 : capacity() * 2;
				reserve(newcapcacity);
				pos = _start + n;//防止迭代器失效，要让pos始终指向与_start间距n的位置
			}
			//挪动数据
			iterator end = _finish - 1;
			while (end >= pos)
			{
				*(end + 1) = *(end);
				end--;
			}
			//把值插进去
			*pos = x;
			_finish++;
		}


		/*删除*/
			//pop_back
		void pop_back()
		{
			if (_finish > _start)
			{
				_finish--;
			}
			/*
				法二：复用erase
				erase(end() - 1);
				//不能用end()--，因为end()是传值返回，返回的是临时对象，临时对象具有常性，不能自身++或--，因此要用end() - 1
			*/
		}
		//erase
		iterator erase(iterator pos)
		{
			//检查合法性
			assert(pos >= _start && pos < _finish);
			//从pos + 1的位置开始往前覆盖，即可完成删除pos位置的值
			iterator it = pos + 1;
			while (it < _finish)
			{
				*(it - 1) = *it;
				it++;
			}
			_finish--;
			return pos;
		}
		//clear清空数据
		void clear()
		{
			_finish = _start;
		}

	private:
		iterator _start;	  //指向容器的头
		iterator _finish;	  //指向有效数据的尾
		iterator _endofstoage;//指向容器的尾
	};
}