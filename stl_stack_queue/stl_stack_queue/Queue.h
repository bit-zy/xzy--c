#pragma once
#include<iostream>
#include<vector>
#include<assert.h>
#include<deque>
using namespace std;
namespace cpp
{
	template<class T, class Container = deque<T>>
	class queue
	{
	public:
		//入队列尾插
		void push(const T& x)
		{
			_con.push_back(x);
		}
		//出队列头删
		void pop()
		{
			_con.pop_front();
		}
		//取队列顶部数据（取队头数据）
		const T& front()
		{
			return _con.front();//返回队头数据
		}
		//取队列尾部数据（取队尾数据）
		const T& back()
		{
			return _con.back();//返回队尾数据
		}
		//获取有效数据个数
		size_t size()
		{
			return _con.size();
		}
		//判空
		bool empty()
		{
			return _con.empty();
		}
	private:
		Container _con;
	};



	//仿函数/函数对象 --- 对象可以像调用函数一样去使用
	template<class T>
	struct less
	{
		//()运算符重载--用于比较大小
		bool operator()(const T& x, const T& y) const
		{
			return x < y;
		}
	};
	template<class T>
	struct greater
	{
		//()运算符重载--用于比较大小
		bool operator()(const T& x, const T& y) const
		{
			return x > y;
		}
	};

	//优先级队列
	template<class T, class Container = vector<T>, class Compare = less<T>>
	class priority_queue
	{
	public:
		//向上调整算法
		void AdjustUp(int child)
		{
			Compare comFunc;//仿函数
			int parent = (child - 1) / 2;
			while (child > 0)
			{
				//利用仿函数建大堆或小堆
				if (comFunc(_con[parent], _con[child]))
				{
					//如果为真，交换
					swap(_con[parent], _con[child]);
					//更新child和parent
					child = parent;
					parent = (child - 1) / 2;
				}
				else
				{
					//此时不需要调整，直接break
					break;
				}
			}
		}

		//向下调整算法
		void AdjustDown(int parent)
		{
			Compare comFunc;//仿函数
			int child = parent * 2 + 1;
			while (child < _con.size())
			{
				if (child + 1 < _con.size() && comFunc(_con[child], _con[child + 1]))
				{
					//如果为真，++child
					child++;
				}
				//利用仿函数建堆
				if (comFunc(_con[parent], _con[child]))
				{
					//如果为真，交换
					swap(_con[parent], _con[child]);
					//更新child和parent
					parent = child;
					child = parent * 2 + 1;
				}
				else
				{
					//此时不需要调整，直接break
					break;
				}
			}
		}
		//插入数据
		void push(const T& x)
		{
			_con.push_back(x);
			//每插入一个数字，都要向上调整键堆
			AdjustUp(_con.size() - 1);
		}
		//删除数据
		void pop()
		{
			//先断言不为空
			assert(!_con.empty());
			//交换头尾两头数据
			swap(_con[0], _con[_con.size() - 1]);
			//删除最后一个数据
			_con.pop_back();
			//删除后从根部向下调整建堆
			AdjustDown(0);
		}
		//取对顶数据
		const T& top()
		{
			return _con[0];
		}
		//获取size有效数据个数
		size_t size()
		{
			return _con.size();
		}
		//判空
		bool empty()
		{
			return _con.empty();
		}
	private:
		Container _con;
	};
}